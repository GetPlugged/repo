﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class SCR_SWORD_Line : MonoBehaviour, IOnEventCallback
{
    public LineRenderer lineRenderer;
    public MeshCollider meshCollider;
    public CapsuleCollider capsuleCollider;
    public PhotonView view;
    public Player owner;
    public SCR_SWORD_RIG gameRig;
    public Color color;
    public SCR_GLOBAL_RenderPlayerModel renderModel;
    public MeshRenderer meshRenderer;
    public float maxDissolve = .15f;
    public bool isSet;
    private float lerp;
    private float dissolveLerp;
    public float duration = 7.5f;
    // Start is called before the first frame update
    void Start()
    {
        //SetLineMesh();
        Debug.LogError(owner);
        //lineRenderer.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (isSet)
        {
            StartTimer();
        }
    }
    public void DestroyLine()
    {
        // Destroy effect goes here
        // Particle effects?
        lineRenderer.enabled = false;
    }
    public void SetLine(Vector3 startPos, Vector3 endPos)
    {
        lineRenderer.enabled = true;

        lineRenderer.SetPosition(0, startPos);
        lineRenderer.SetPosition(1, endPos);

        // Set Gameobject world position to match line
        transform.position = (startPos + endPos) / 2;
        Vector3 dir = (endPos - startPos).normalized;
        transform.rotation = Quaternion.LookRotation(dir);

        // Set collider props
        capsuleCollider.radius = .05f;
        capsuleCollider.height = Vector3.Distance(startPos, endPos);

        isSet = true;
    }
    private void RemoveLine()
    {
        isSet = false;
        lerp = 0;
        // Change color or effects here
        // Remove line display somehow
        lineRenderer.enabled = false;
    }

    private void StartTimer()
    {
        lerp += Time.deltaTime / duration;
        dissolveLerp = Mathf.Lerp(maxDissolve, 0, lerp);
        lineRenderer.material.SetFloat("_DissolveAmount", dissolveLerp);
        if (dissolveLerp <= 0)
        {
            gameRig.health--;
            RemoveLine();
        }
    }


    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;

        object[] data = (object[])photonEvent.CustomData;
        //Debug.Log(data[2].ToString());
        int ID = (int)data[2];
        if (ID != view.ViewID) return;

        switch (eventCode)
        {
            case 31:
                isSet = true;
                break;
            case 32:
                RemoveLine();
                break;
            default:
                break;
        }
    }
}
