﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_DEBUG_DebugPrefs : MonoBehaviour
{
    public int playerFace;
    public int playerBody;
    private Color playerColor;
    public float intensity;
    public int playerHead;
    void Awake()
    {
        //Don't keep this \/ just for funsies
        playerColor = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f);

        int hPref = PlayerPrefs.GetInt("0-GLOBAL_3-HEAD");
        int h = hPref;
        while (h == hPref)
        {
            h = Random.Range(0, 4);
        }
        playerHead = h;

        hPref = PlayerPrefs.GetInt("0-GLOBAL_2-BODY");
        h = hPref;
        while (h == hPref)
        {
            h = Random.Range(0, 4);
        }
        playerBody = h;

        PlayerPrefs.SetInt("0-GLOBAL_4-FACE", 0);
        PlayerPrefs.SetInt("0-GLOBAL_2-BODY", 0);
        PlayerPrefs.SetInt("0-GLOBAL_3-HEAD", 4);
        PlayerPrefs.SetInt("5-BIKES_0-BIKE", 0);

        PlayerPrefs.SetString("PLAYER_COLOR", "#"+ColorUtility.ToHtmlStringRGBA(playerColor));
        PlayerPrefs.SetFloat("INTENSITY", intensity);
    }
}
