﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_DEBUG_Body : MonoBehaviour
{
    public GameObject pivot;
    public GameObject cam;
    public GameObject neck;


    public float smooth;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(neck.transform.position.x, cam.transform.position.y, neck.transform.position.z), smooth);
        //transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(transform.eulerAngles.x, pivot.transform.eulerAngles.y, transform.eulerAngles.z), smooth);

    }
}
