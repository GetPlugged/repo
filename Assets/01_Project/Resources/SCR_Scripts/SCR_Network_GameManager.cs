﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

/// <summary>
/// Object required to be a networked scene(s). Only ONE should exist as a single instance
/// <para>Gamemanager is a self contained object, should only be used to grab properties, never assign.</para>
/// <para>Proper use: var = SCR_Network_GameManager.instance.variable</para>
/// </summary>
[AddComponentMenu("Plugged/GameManager")]
public class SCR_Network_GameManager : MonoBehaviourPunCallbacks
{
    #region Variables
    /// <summary>
    /// Reference to singleton object. Only one will exist in a scene
    /// </summary>

    //Global references
    public static SCR_Network_GameManager instance;
    [HideInInspector]
    public SCR_GLOBAL_Rig playerRig;
    public GameObject gameRig;
    [HideInInspector]
    public SCR_GLOBAL_WorldManager worldManager;

    public Color newColor;
    public Color worldColor;
    public Color worldColorSecondary;

    [Header("Rigs")]
    [Space(10)]
    public GameObject debugRig;
    public GameObject swordsRig;
    public GameObject virusRig;
    public GameObject bikesRig;
    public GameObject mechRig;

    [Header("Game Scenes")]
    public string launcherScene;
    public string homeScene;
    public string swordsScene;
    public string virusScene;
    public string mechScene;
    public string bikesScene;

    [Space(10)]
    /// <summary>
    /// While in play, select desired gamemode and hit 'P' to load.
    /// Gamemode needs to be set before hand in scene on manager, or else rig will not load properly
    /// </summary>
    public GameMode gamemode;
    public enum GameMode
    {
        none,
        debug,
        home,
        swords,
        virus,
        mech,
        bikes,
    };

    // Game Mode specific variables
    public int tLimit;
    public bool isSpawned;
    public bool didLeave = false;
    #endregion

    #region Callbacks


    public override void OnLeftRoom()
    {
        //SceneManager.LoadScene("SCN_Network_Launcher");
        //PhotonNetwork.LoadLevel("SCN_Network_Launcher"); //using this one
    }

    public override void OnConnectedToMaster()
    {
        if (didLeave == true)
        {
            Debug.Log("GameManager: Connected to Master");
            PhotonNetwork.JoinRandomRoom(); // to simulate connecting to other players
            Debug.Log("GameManager: Join Random Room");
            didLeave = false;
        }
        

    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        PhotonNetwork.CreateRoom(System.DateTime.Now.ToString(), null, null, null); // Create random string for room name
        Debug.Log("GameManager: No random room, creating solo room");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        //base.OnPlayerEnteredRoom(newPlayer);
        if (PhotonNetwork.IsMasterClient)
        {
            Debug.Log("OnPhotonPlayerConnected() isMasterClient " + PhotonNetwork.IsMasterClient);
            //LoadArena();
        }
        Debug.Log(PhotonNetwork.PlayerList);
        Debug.Log("Player Entered Room");
        
        
        //photonView.RPC("Colored", RpcTarget.AllBuffered, null);
    }

    public override void OnConnected()
    {
        Debug.Log("Gamemanager: Established Raw Connection");
        
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        //base.OnPlayerLeftRoom(otherPlayer);
        if (PhotonNetwork.IsMasterClient)
        {
            Debug.Log("OnPhotonPlayerDisconnected() isMasterClient " + PhotonNetwork.IsMasterClient);
            //LoadArena();
        }
    }

    public override void OnJoinedRoom()
    {
        Instantiate();
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        //////////////////////////////////////Change worldColor to leading player.
        //Color sColor;
        //ColorUtility.TryParseHtmlString((string)targetPlayer.CustomProperties["color"], out sColor);
        //worldColor = sColor;
        //if (targetPlayer.IsMasterClient)
        //{
        //    worldColor = (Color)changedProps["color"];
        //}
        //else
        //{
        //    worldColor = (Color)PhotonNetwork.PlayerList[0].CustomProperties["color"];
        //}
    }
    //public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    //{
    //    PhotonView playerView = PhotonView.Get(playerPrefab);
    //    Debug.Log(playerView);
    //    playerView.RPC("NetworkVanity", RpcTarget.AllBuffered, changedProps);
    //    //foreach (Player play in PhotonNetwork.PlayerList)
    //    //{
    //    //    if(play == targetPlayer)
    //    //    {
    //    //        changedProps["color"]
    //    //        Color color = (Color)play.CustomProperties["color"];
    //    //    }
    //    //}

    //    //foreach (Player listPlayer in PhotonNetwork.PlayerList)
    //    //{
    //    //    if (listPlayer == targetPlayer)
    //    //    {
    //    //        Debug.Log(targetPlayer);
    //    //        playerView.RPC("DebugMethod", RpcTarget.AllBuffered, changedProps);
    //    //    }
    //    //}


    //    //if (changedProps.ContainsKey("color"))
    //    //{

    //    //    PhotonView view = PhotonView.Get(playerPrefab);
    //    //    view.RPC("DebugMethod", RpcTarget.AllBuffered, null);
    //    //}
    //    //Color color = (Color)PhotonNetwork.LocalPlayer.CustomProperties["color"];

    //}

    /// <summary>
    /// 0 = Launcher, 1 = Home, 2 = Debug, 3 = Swords, 4 = Virus, 5 = Bikes, 6 = Mech
    /// </summary>
    /// <param name="level"></param>

    #endregion

    #region Public Methods 
    public void LeaveRoom()
    {
        didLeave = true;
        PhotonNetwork.LeaveRoom();
    }

    #endregion

    #region Private Methods

    void Awake()
    {
        // Create singleton, make sure this is the only one in the scene at all times
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        } else if (instance != this)
        {
            DestroyImmediate(this.gameObject);
        }
        
        isSpawned = false;
        //worldColor = Random.ColorHSV();      
        //list = PhotonNetwork.PlayerList list = new PhotonNetwork.PlayerList[]
        
    }
    private void Start()
    {
        CheckConnection();
        // Called when the scene begins
        //Instantiate();
    }

    /// <summary>
    /// DO NOT KEEP THIS IN FINAL BUILD
    /// Launcher scene NEEDS to be index 0!
    /// </summary>
    private void CheckConnection()
    {
        if (SceneManager.GetActiveScene().name != launcherScene)
        {
            
        }
        if (!PhotonNetwork.IsConnectedAndReady)
        {
            //DestroyImmediate(this.gameObject);
            SceneManager.LoadScene("SCN_Network_Launcher");
            return;
        }
    }

    public void LoadGamemodeInt(int num)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            // Need to switch enum when scene is loaded, to spawn mode specific rig
            switch (num)
            {
                case 1:
                    PhotonNetwork.LoadLevel("SCN_DEBUG_StandardRig");
                    worldColor = Color.white;
                    gamemode = GameMode.debug;
                    break;
                case 2:
                    //PhotonNetwork.LoadLevel("SCN_MODE_Home");
                    PhotonNetwork.LoadLevel(homeScene);
                    gamemode = GameMode.home;
                    worldColor = Color.black;
                    // No rig
                    break;
                case 3:
                    PhotonNetwork.LoadLevel(swordsScene);
                    gamemode = GameMode.swords;
                    worldColor = Color.white;
                    break;
                case 4:
                    PhotonNetwork.LoadLevel(virusScene);
                    gamemode = GameMode.virus;
                    worldColor = Color.green;
                    break;
                case 5:
                    PhotonNetwork.LoadLevel(mechScene);
                    gamemode = GameMode.mech;
                    worldColor = Color.white;
                    break;
                case 6:
                    PhotonNetwork.LoadLevel(bikesScene);
                    gamemode = GameMode.bikes;
                    worldColor = Color.white;
                    break;
                default:
                    break;
            }
        }
    }


    /// <summary>
    /// Loads local player upon connecting to a room
    /// </summary>
    private void Instantiate()
    {
        
        Vector3 spawn = new Vector3(0, 0, 0);
        Vector3 sRandom = new Vector3(Random.Range(-1, 1), .5f, Random.Range(-1, 1));
        Vector3 spawnVive = new Vector3(0, 0, 0);
        worldColor = Color.black;
        // Spawn player rig over network
        playerRig = PhotonNetwork.Instantiate("PRE_Prefabs/PRE_GLOBAL_Rig", spawn, Quaternion.identity).GetComponent<SCR_GLOBAL_Rig>(); ;
    }

    private void OnLevelWasLoaded(int level)
    {
        // Get client specific rig on joining specific map. Will have to manage this with photon onjoinedroom, to list what gamemode the current users are in. Set room specific properties!!
        Vector3 spawn = new Vector3(0, 0, 0);


        Debug.LogWarningFormat("Scene loaded: {1}, Build Scene Index: {0}", SceneManagerHelper.ActiveSceneBuildIndex, SceneManagerHelper.ActiveSceneName); // use when re-structuring
        string scene = SceneManagerHelper.ActiveSceneName;
        string rig = "";
        if (scene == homeScene)
        {
            gamemode = GameMode.home;
            // No game mode rig
        }
        else if(scene == swordsScene)
        {
            gamemode = GameMode.swords;
            rig = "PRE_Prefabs/" + swordsRig.name;
        }
        else if (scene == virusScene)
        {
            gamemode = GameMode.virus;
            rig = "PRE_Prefabs/" + virusRig.name;
        }
        else if (scene == mechScene)
        {
            gamemode = GameMode.mech;
            rig = "PRE_Prefabs/" + mechRig.name;
        }
        else if (scene == bikesScene)
        {
            gamemode = GameMode.bikes;
            rig = "PRE_Prefabs/" + bikesRig.name;
        }
        else if(scene == launcherScene){
            // No gamemode
            // No rig
        }
        else
        {
            Debug.LogError("No rig initalized! Check [GameManager Prefab] to see if correct scene was loaded");
            //rig = "PRE_Prefabs/" + debugRig.name;
        }

        if (rig != "") gameRig = PhotonNetwork.Instantiate(rig, spawn, Quaternion.identity);
        //gameRig = PhotonNetwork.Instantiate(rig, spawn, Quaternion.identity).GetComponent<SCR_GLOBAL_GameRigInit>();
      
    }
    #endregion
    public void SetColor(Color color)
    {
        //newColor = color;
    }



}
