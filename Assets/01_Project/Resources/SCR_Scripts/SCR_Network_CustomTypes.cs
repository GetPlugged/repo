﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using ExitGames.Client.Photon;

public class SCR_Network_CustomTypes
{
    public static void Register()
    {
        // Register with photon your custom data type to be serialized and deserialized.
        PhotonPeer.RegisterType(typeof(Color), (byte) 'C', SerializeColor, DeserializeColor);
    }

    public static readonly byte[] memColor = new byte[4 * 4];

    private static short SerializeColor(StreamBuffer outStream, object customobject)
    {
        Color c = (Color)customobject;
        int index = 0;
        lock (memColor)
        {
            byte[] bytes = memColor;
            Protocol.Serialize(c.r, bytes, ref index);
            Protocol.Serialize(c.g, bytes, ref index);
            Protocol.Serialize(c.b, bytes, ref index);
            Protocol.Serialize(c.a, bytes, ref index);
            outStream.Write(bytes, 0, 4 * 4);
        }
        return 4 * 4;

    }

    private static object DeserializeColor(StreamBuffer inStream, short length)
    {
        Color c = new Color();
        lock (memColor)
        {
            inStream.Read(memColor, 0, 4 * 4);
            int index = 0;
            Protocol.Deserialize(out c.r, memColor, ref index);
            Protocol.Deserialize(out c.g, memColor, ref index);
            Protocol.Deserialize(out c.b, memColor, ref index);
        }

        return c;
    }

}
