﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_BIKES_Needle : MonoBehaviour, IPunObservable
{
    public float speed;
    public int damage;
    public Transform target;
    public Transform driver;
    public float trackSpeed;
    public bool heatSeeking;
    public GameObject collider;
    private PhotonView view;
    private RaycastHit hit;
    public LayerMask mask;
    private Transform t;
    public int weaponIndex;
    // Start is called before the first frame update
    void Start()
    {
        view = gameObject.GetComponent<PhotonView>();
        if(view.IsMine)
        {
            GameObject c = Instantiate(collider, transform.position, transform.rotation);
            c.GetComponent<SCR_BIKES_NeedleCollider>().needle = gameObject;
            SCR_Network_GameManager.instance.gameRig.GetComponent<SCR_BIKES_Rig>().ammo[weaponIndex]--;

        }

    }

    // Update is called once per frame
    void Update()
    {
        if(view.IsMine)
        {
            if(target)
            {
                Vector3 d = (target.position - transform.position).normalized;
                Quaternion t = Quaternion.LookRotation(d);
                transform.rotation = Quaternion.Slerp(transform.rotation, t, trackSpeed * Time.deltaTime);
                //trackSpeed += trackSpeed;
            }
            transform.position += transform.forward * speed * Time.deltaTime;
        }
        else
        {

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (view.IsMine && other.gameObject.layer == 22 && heatSeeking)
        {
            if (!target)
            {

                if (!Physics.Linecast(transform.position, other.transform.position, out hit, mask))
                {
                    target = other.transform;
                }

            }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new System.NotImplementedException();
    }
}