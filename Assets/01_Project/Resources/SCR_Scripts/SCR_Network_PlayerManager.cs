﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using System;
using Photon.Pun;
using Photon.Realtime;

/// <summary>
/// Handles input
/// </summary>
public class PlayerManager : MonoBehaviourPunCallbacks, IPunObservable    //Gets observed by Photon View
{
    #region General Properties


    public SteamVR_Behaviour_Pose pose1;
    public SteamVR_Behaviour_Pose pose2;
    public SteamVR_Behaviour behaviour;
    public GameObject cameraRig;



    [SerializeField]
    public CharacterController characterController;
    //public PlayerInfo playerInfo;
    public Camera cameraVR;
    public ChaperoneInfo chaperoneInfo;

    public GameObject cube;

    #endregion

    #region Input Properties
    //public SteamVR_Action_Vector2 touchPad;
    public SteamVR_Input_Sources touchPadSource;
    public SteamVR_Action_Boolean trigger;
    public SteamVR_Input_Sources sourceTrigger;
    public SteamVR_Action_Boolean menuButton;
    public SteamVR_Input_Sources menuSource;

    // Stupid Alternative but actually works //
        public SteamVR_Action_Boolean menuAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Menu");
        //public SteamVR_Action_Vector2 touchPad2 = SteamVR_Input.GetAction<SteamVR_Action_Vector2>("TouchPadTouch");
    #endregion

    //public int health = 10;
    //public bool IsFiring;

    public static GameObject localPlayerInstance;
    public static PlayerManager instance;
    public GameObject lControllerPrefab;
    public GameObject[] leftHandBones;
    public GameObject[] rightHandBones;
    public GameObject RControllerPrefab;
    public GameObject faceMask;


    [Serializable]
    public class Controller
    {
        public SteamVR_Behaviour_Pose controller;
        public Transform pivot;
    }
    [Header("Player Specific")]
    public Controller leftHand;
    public Controller rightHand;


   
    #region Private Methods
    void Awake()
    {
        // used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized
        if (photonView.IsMine)
        {
            PlayerManager.localPlayerInstance = this.gameObject;
            cameraRig.SetActive(true);
        }

        // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
        //DontDestroyOnLoad(this.gameObject);       

    }
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        //characterController = GetComponent<CharacterController>();


        //bounds = GetComponent<SteamVR_PlayArea>();

    }

    // Update is called once per frame
    void Update()
    {
        if (!photonView.IsMine)
        {
            return;
        }

        //if(health <= 0)
        //{
        //    GameManager.instance.LeaveRoom();   // Contacts the game manager object to call the LeaveRoom method for the local player
        //}

        if (menuAction.GetState(touchPadSource))
        {
            Debug.Log("Menu Button Pressed");
            SCR_Network_GameManager.instance.LeaveRoom();
        }

        if (lControllerPrefab)
        {
            //lControllerPrefab.transform.position = leftHand.controller.transform.position;
            //lControllerPrefab.transform.rotation = leftHand.controller.transform.rotation;
        }
        if (RControllerPrefab)
        {
            //RControllerPrefab.transform.position = rightHand.controller.transform.position;
            //RControllerPrefab.transform.rotation = rightHand.controller.transform.rotation;
        }
        if(faceMask)
        {
            faceMask.transform.position = cameraVR.transform.position;
            faceMask.transform.rotation = cameraVR.transform.rotation;
        }
    }


    #endregion
    #region Public Methods


    #endregion
    //void OnTriggerEnter(Collider other)
    //{
    //    if(!photonView.isMine)
    //    {
    //        return;
    //    }
    //    if(!other.name.Contains("Beam"))
    //    {
    //        return;
    //    }
    //    health -= 1;
    //}

    //private void OnTriggerStay(Collider other)
    //{
    //    if (!photonView.isMine)
    //    {
    //        return;
    //    }
    //    if (!other.name.Contains("Beam"))
    //    {
    //        return;
    //    }
    //    health -= 1* Mathf.CeilToInt(Time.deltaTime);

    //}


    public void Move()
    {
        //if (!characterController.isGrounded)
        //{
        //    characterController.SimpleMove(new Vector3(0, -.5f, 0));
        //}
        //else
        //{
        //    var axis = touchPad2.GetAxis(touchPadSource);
        //    Vector3 targetdirection = new Vector3(axis.x, 0f, axis.y);
        //    targetdirection = cameraVR.transform.TransformDirection(targetdirection);
        //    targetdirection.y = -.5f;
        //    //debug.log(axis);
        //    characterController.SimpleMove(targetdirection);
        //}

    }

    public virtual void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        cameraVR.enabled = photonView.IsMine;
        //pose1.enabled = photonView.isMine;
        //pose2.enabled = photonView.isMine;
        //behaviour.enabled = photonView.isMine;
    }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
    //    if(stream.isWriting)
    //    {
    //        // We own the player, send the others our data
    //        stream.SendNext(IsFiring);
    //        stream.SendNext(health);
    //    }
    //    else
    //    {
    //        // Network player, receive data
    //        this.IsFiring = (bool)stream.ReceiveNext();
    //        this.health = (int)stream.ReceiveNext();
    //    }
    }
    private void OnLevelWasLoaded(int level)
    {
        //Used for when levels are loaded
    }
}
