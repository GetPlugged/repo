﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Place on network specific gameobject that needs to be persistent across loading. (Global player models)
/// Helps prevent player duplication bug!
/// </summary>
public class SCR_Network_DontDestroy : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

}
