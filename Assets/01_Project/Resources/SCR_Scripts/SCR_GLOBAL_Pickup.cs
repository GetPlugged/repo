﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Valve.VR;

public class SCR_GLOBAL_Pickup : MonoBehaviourPunCallbacks, IPunObservable
{
    public int pickupIndex;
    public int pickupType;
    private bool picked;
    private PhotonView view;

    public Renderer[] renderers;
    public Renderer[] multiRenderers;
    public float maxDissolve = 1;
    public Vector2[] intensityEffectors;
    public float spawnTime;

    public float colorLerp;

    public float dissolveLerp;

    public Color color;

    public float intensity;
    private float curSpawnTime;

    private float dissolve;
    private string netColor;
    private float netDissolve;
    private Material[] materials;
    private Color cRef;
    private SCR_Network_GameManager gm;
    public bool home;
    public int modifier;

    // Start is called before the first frame update
    void Start()
    {
        view = GetComponent<PhotonView>();

        if (GetComponent<PhotonView>())
            view = GetComponent<PhotonView>();

        materials = new Material[renderers.Length];

        for (int i = 0; i < materials.Length; i++)
        {
            materials[i] = renderers[i].material;
        }

        dissolve = 0;

        gm = SCR_Network_GameManager.instance;

        if (!home)
            curSpawnTime = spawnTime;
    }

    // Update is called once per frame
    void Update()
    {
        float ie = intensity;

        if (view)
        {
            if (!view.IsMine)
            {
                ColorUtility.TryParseHtmlString(netColor, out color);
                dissolve = netDissolve;
                colorLerp = .1f;
                dissolveLerp = 4f;
                ie = 1.027f;
            }
            else if(view.Owner.IsMasterClient)
            {
                if(picked)
                    dissolve = 1;
                else
                    dissolve = 0;

                switch (gm.gamemode)
                {
                    case (SCR_Network_GameManager.GameMode.bikes):

                        if (curSpawnTime < 1)
                        {
                            if (picked)
                                picked = false;
                        }

                        curSpawnTime -= Time.deltaTime;

                        break;

                    case (SCR_Network_GameManager.GameMode.virus):

                        if(home)
                        {
                            if (curSpawnTime < 1)
                            {
                                if (picked)
                                    picked = false;
                            }
                        }
                        else
                        {
                            if (curSpawnTime < 1)
                            {
                                PhotonNetwork.Destroy(view);
                            }
                        }
                        curSpawnTime -= Time.deltaTime;


                        break;

                    case (SCR_Network_GameManager.GameMode.mech):

                        if (home)
                        {
                            if (curSpawnTime < 1)
                            {
                                if (picked)
                                    picked = false;
                            }
                        }
                        else
                        {
                            if (curSpawnTime < 1)
                            {
                                PhotonNetwork.Destroy(view);
                            }
                        }
                        curSpawnTime -= Time.deltaTime;


                        break;

                }

                netColor = "#" + ColorUtility.ToHtmlStringRGBA(color);
                netDissolve = dissolve;
            }
        }

        for (int i = 0; i < materials.Length; i++)
        {
            foreach (Vector2 item in intensityEffectors)
            {
                if (item.x == i)
                    ie = item.y;
            }

            materials[i].SetColor("_EmissionColor", Color.Lerp(materials[i].GetColor("_EmissionColor"), color, colorLerp) * ie);
            materials[i].SetFloat("_DissolveAmount", Mathf.Lerp(materials[i].GetFloat("_DissolveAmount"), dissolve * maxDissolve, dissolveLerp * Time.deltaTime));
        }
        foreach (Renderer item in multiRenderers)
        {
            foreach (Material material in item.materials)
            {
                material.SetColor("_EmissionColor", Color.Lerp(material.GetColor("_EmissionColor"), color, colorLerp) * ie);
                material.SetFloat("_DissolveAmount", Mathf.Lerp(material.GetFloat("_DissolveAmount"), dissolve * maxDissolve, dissolveLerp * Time.deltaTime));
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 27)
        {
            Debug.Log("Contact");

            switch (gm.gamemode)
            {
                case (SCR_Network_GameManager.GameMode.bikes):
                    view.RPC("Pickup", RpcTarget.MasterClient, gm.gameRig.GetComponent<SCR_BIKES_Rig>().photonView.ViewID,0,0);
                    break;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == 27)
        {
            Debug.Log("Contact");

            switch (gm.gamemode)
            {
                case (SCR_Network_GameManager.GameMode.virus):

                    if (GrippedLeft())
                    {
                        if(home)
                            view.RPC("Pickup", RpcTarget.MasterClient, gm.gameRig.GetComponent<SCR_VIRUS_Rig>().photonView.ViewID, 0, -1);
                        else
                            view.RPC("Pickup", RpcTarget.MasterClient, gm.gameRig.GetComponent<SCR_VIRUS_Rig>().photonView.ViewID, 0, modifier);
                    }
                    else if (GrippedRight())
                    {
                        if (home)
                            view.RPC("Pickup", RpcTarget.MasterClient, gm.gameRig.GetComponent<SCR_VIRUS_Rig>().photonView.ViewID, 1, -1);
                        else
                            view.RPC("Pickup", RpcTarget.MasterClient, gm.gameRig.GetComponent<SCR_VIRUS_Rig>().photonView.ViewID, 1, modifier);
                    }
                    break;

                case (SCR_Network_GameManager.GameMode.mech):

                    if (GrippedLeft())
                    {
                        if (home)
                            view.RPC("Pickup", RpcTarget.MasterClient, gm.gameRig.GetComponent<SCR_MECHS_Rig>().photonView.ViewID, 0, -1);
                        else
                            view.RPC("Pickup", RpcTarget.MasterClient, gm.gameRig.GetComponent<SCR_MECHS_Rig>().photonView.ViewID, 0, modifier);
                    }
                    else if (GrippedRight())
                    {
                        if (home)
                            view.RPC("Pickup", RpcTarget.MasterClient, gm.gameRig.GetComponent<SCR_MECHS_Rig>().photonView.ViewID, 1, -1);
                        else
                            view.RPC("Pickup", RpcTarget.MasterClient, gm.gameRig.GetComponent<SCR_MECHS_Rig>().photonView.ViewID, 1, modifier);
                    }
                    break;
            }
        }
    }

    [PunRPC]
    void Pickup(int id, int hand, int modifier)
    {
        if(!picked)
        {
            picked = true;
            curSpawnTime = spawnTime;

            int[] content = new int[] { pickupType, pickupIndex, id ,hand, modifier}; // Array contains the target position and the IDs of the selected units
            RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
            SendOptions sendOptions = new SendOptions { Reliability = true };
            PhotonNetwork.RaiseEvent(5, content, raiseEventOptions, sendOptions);
            Debug.Log("Picked");
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (view)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(netColor);
                stream.SendNext(netDissolve);
            }
            else
            {
                netColor = (string)stream.ReceiveNext();
                netDissolve = (float)stream.ReceiveNext();
            }
        }
    }

    private bool GrippedLeft()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
        {

            return (Input.GetButtonDown("LGripButton"));//(Input.GetAxis("LGrip") == 1 && Input.GetButton("LTouchButtons"));

        }
        else if (XRSettings.loadedDeviceName == "OpenVR")
            return (SteamVR_Input.GetStateDown("GrabGrip", SteamVR_Input_Sources.LeftHand, true));

        else
            return false;
        //else if (XRSettings.loadedDeviceName == "OpenVR")
        //{

        //    return ();

        //}
    }

    private bool GrippedRight()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
        {

            return (Input.GetButtonDown("RGripButton"));//(Input.GetAxis("LGrip") == 1 && Input.GetButton("LTouchButtons"));

        }
        else if (XRSettings.loadedDeviceName == "OpenVR")
            return (SteamVR_Input.GetStateDown("GrabGrip", SteamVR_Input_Sources.RightHand, true));

        else
            return false;
        //else if (XRSettings.loadedDeviceName == "OpenVR")
        //{

        //    return ();

        //}
    }

}