﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_BIKES_NeedleTracker : MonoBehaviourPunCallbacks
{

    public LayerMask mask;
    private RaycastHit hit;
    private Transform target;
    public GameObject collider;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity,mask))
        {
            target = hit.transform;
        }
        GameObject b = PhotonNetwork.Instantiate("PRE_Prefabs/PRE_BIKES_Needle", transform.position, transform.rotation);
        if(target)
        b.GetComponent<SCR_BIKES_Needle>().target = target;
        Destroy(gameObject);
    }
}
