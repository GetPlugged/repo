﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SCR_Network_Transform : MonoBehaviour, IPunObservable
{
    [Range(1,50)]
    public float speed = 2.5f;
    private Vector3 velocity;
    private Vector3 netPos;
    private Quaternion netRot;
    private Vector3 oldPos;
    private Vector3 vel;
    private PhotonView view;

    public void Start()
    {
        view = GetComponent<PhotonView>();
    }
    public void Update()
    {
        oldPos = transform.position;
    }
    public void FixedUpdate()
    {
        if (!view.IsMine)
        {
            transform.position = Vector3.MoveTowards(transform.position, netPos, Time.deltaTime * speed);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, netRot, Time.deltaTime * 100);
            return;
        }
    }
    public void LateUpdate()
    {
        velocity = transform.position - oldPos;
    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {

            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
            stream.SendNext(velocity);

        }
        else
        {
            netPos = (Vector3)stream.ReceiveNext();
            netRot = (Quaternion)stream.ReceiveNext();

            float lag = Mathf.Abs((float)(PhotonNetwork.Time - info.SentServerTime));
            netPos += (velocity * lag);

        }
    }
}
