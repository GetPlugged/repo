﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_GLOBAL_DeathRig : MonoBehaviourPunCallbacks, IPunObservable
{
    private string netColor;
    private float netDissolve;
    private float dissolve;
    private PhotonView view;
    public SCR_GLOBAL_PlayerModel[] models;

    // Start is called before the first frame update
    void Start()
    {
        view = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (SCR_GLOBAL_PlayerModel item in models)
        {
            item.dissolveLerp = 4f;
            item.colorLerp = .1f;
            item.intensity = 1.02f;
            item.color = Color.white;
        }

        if (!view.IsMine)
        {
            dissolve = netDissolve;

            foreach (SCR_GLOBAL_PlayerModel item in models)
            {
                item.dissolve = dissolve;
            }
        }
        else
        {
            netDissolve = models[0].dissolve;
        }



    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(netDissolve);
        }
        else
        {
            netDissolve = (float)stream.ReceiveNext();
        }
    }

}
