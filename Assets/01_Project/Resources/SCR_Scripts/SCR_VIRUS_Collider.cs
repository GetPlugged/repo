﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_VIRUS_Collider : MonoBehaviour
{
    public PhotonView view;
    private SCR_GLOBAL_Gun gun;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerStay(Collider other)
    {
        if (view.IsMine && other.gameObject.layer == 29)
        {
            gun.Fire();
        }
    }
}
