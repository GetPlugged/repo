﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class SCR_GLOBAL_GameRigInit : MonoBehaviour
{
    public SCR_GLOBAL_Rig player;
    public SCR_GLOBAL_RenderPlayerModel[] playerModels;
    public PhotonView view;

    // Start is called before the first frame update
    void Start()
    {
        view = this.GetComponent<PhotonView>();
        player = SCR_Network_GameManager.instance.playerRig;
    }

    // Update is called once per frame
    void Update()
    {
        if(player == null)
        {
            // Temporary to fix auto joining into scenes. wont keep on final build > will spawn into home room with no secondary rig
            player = SCR_Network_GameManager.instance.playerRig;
        }
        if (view.IsMine)
            foreach (var item in playerModels)
            {
                item.color = player.GetComponent<SCR_GLOBAL_Rig>().color;
            }
    }
}