﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_BIKES_Bolt : MonoBehaviourPunCallbacks, IPunObservable
{
    private PhotonView view;
    public LineRenderer line;
    public GameObject target;
    private int fint;
    public Transform[] points;
    public float amount;
    private RaycastHit hit;
    public LayerMask mask;
    private Vector3[] netP;
    private Vector3[] netR;

    // Start is called before the first frame update
    void Start()
    {
        view = GetComponent<PhotonView>();
        line.positionCount = 10;
        fint = 3;
        netP = new Vector3[line.positionCount];
        SCR_Network_GameManager.instance.gameRig.GetComponent<SCR_BIKES_Rig>().ammo[5]--;

    }

    // Update is called once per frame
    void Update()
    {
        if(view.IsMine)
        {
            line.SetPosition(0, transform.position);

            if (target)
            {
                for (int i = 1; i < 10; i++)
                {
                    points[0].position = Vector3.Lerp(transform.position, target.transform.position, .1f * i);
                    points[1].localPosition = new Vector3(points[1].localPosition.x + (Random.Range(amount * -1, amount) * i), points[1].localPosition.y + (Random.Range(amount * -1, amount) * i), points[1].localPosition.z);
                    line.SetPosition(i, points[1].position);
                    netP[i] = points[1].position;
                }
            }
            else
            {
                for (int i = 1; i < 10; i++)
                {
                    points[0].position = Vector3.Lerp(transform.position, points[2].transform.position, .1f * i);
                    points[1].localPosition = new Vector3(points[1].localPosition.x + (Random.Range(amount * -1, amount) * i), points[1].localPosition.y + (Random.Range(amount * -1, amount) * i), points[1].localPosition.z);
                    line.SetPosition(i, points[1].position);
                    netP[i] = points[1].position;

                }
            }



        }
        else
        {
            for (int i = 0; i < 10; i++)
            {
                line.SetPosition(i, netP[i]);
            }


        }

        fint--;
        if (fint < 0)
            Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(view.IsMine)
        {
            if (!target && other.gameObject.layer == 22 && !Physics.Linecast(transform.position, other.transform.position, out hit, mask))
                target = other.gameObject;

        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(netP);
        }
        else
        {
            netP = (Vector3[])stream.ReceiveNext();
        }
    }
}
