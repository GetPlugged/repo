﻿using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class SCR_SWORD_Trigger : MonoBehaviour, IPunObservable, IOnEventCallback
{
    public SCR_SWORD_RIG parent;
    public SCR_GLOBAL_RenderPlayerModel renderBlade;
    public SCR_GLOBAL_RenderPlayerModel renderHilt;
    public SCR_GLOBAL_RenderPlayerModel renderHand;
    //public TrailRenderer trail; // Placed on blade model prefab, updated there
    public GameObject linePrefab;
    public SCR_SWORD_Line line;
    public float dissolve;
    public GameObject hilt;
    //public LineRenderer line; //render slash
    public bool isExtended;
    private bool didHit;
    public PhotonView view;
    public Player owner;

    public float startAmt;
    public float endAmt;
    public float minDistance = .5f;
    public bool isSwing;
    private bool isSwing2;
    public Vector3 velocity;
    public float velMag;
    private Vector3[] pos = new Vector3[2];
    private Vector3 lastPos;
    public const byte DrawLineEventCode = 30;
    public const byte AddHitTimerEventCode = 31;
    public const byte RemoveHitTimerEventCode = 32;
    private bool isInitalized = false;

    public void Initalize()
    {
        view = this.GetComponent<PhotonView>();

        transform.parent.TryGetComponent<SCR_SWORD_RIG>(out parent);
        linePrefab = Instantiate(linePrefab);
        line = linePrefab.GetComponent<SCR_SWORD_Line>();
        renderBlade.dissovleLerp = .05f;
        line.gameRig = parent;
        line.owner = parent.view.Owner;
        line.view = view;
        line.renderModel = parent.gPlayerRenderModel;
        renderBlade.color = parent.color;
        DisableBlade();

        //renderBlade.color = parent.playerRig.color;

        //line.color = parent.playerRig.color;




        //lTrigger.line = lBlade.GetComponent<LineRenderer>();   //render slash

        //line.color = parent.playerRig.color;
        //line.color = renderBlade.color;
        //line.lineRenderer.material = new Material(parent.material);
        //line.lineRenderer.material.color = parent.playerRig.color;

        //line.lineRenderer.material.SetColor("_Emission", parent.playerRig.color);


        gameObject.layer = 12; // Player Layer
        //linePrefab = Instantiate(linePrefab);
        //line = linePrefab.GetComponent<SCR_Swords_Line>();
        //line.owner = view.Owner;
        isInitalized = true;
    }
    private void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    private void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public void Update()
    {
        if (SCR_Network_GameManager.instance.worldManager.state != 4) return;
        if (!parent.isAlive) return;
        if (!isInitalized) return;
        if (view.IsMine)
        {
            if (lastPos != renderBlade.transform.position)
            {
                velocity = renderBlade.transform.position - lastPos;
                velocity /= Time.deltaTime;
                lastPos = renderBlade.transform.position;
            }
            Swing();
        }
        line.lineRenderer.material.SetColor("_EmissionColor", parent.gPlayerRenderModel.color);
    }

    public void LateUpdate()
    {
        //lastPos = renderBlade.transform.position;
    }

    public void DisableBlade()
    {
        // dont need to network model change, just observe changes in view on render prefab
        if (renderBlade.gameObject != null)
        {
            //renderBlade.model.GetComponent<MeshRenderer>().enabled = false;
            renderBlade.dissolve = 1;
        }
        isExtended = false;
    }

    [PunRPC]
    public void EnableBlade()
    {
        // This has to be an rpc, to network the button press on both instances
        if (renderBlade.gameObject != null)
        {
            //renderBlade.model.GetComponent<MeshRenderer>().enabled = true;
            renderBlade.dissolve = 0;
        }
        isExtended = true;
    }

    public void OnTriggerEnter(Collider other)
    {
        int target;
        if (isExtended && isSwing)
        {
            SCR_SWORD_Line enemyLine;
            if(other.gameObject.TryGetComponent<SCR_SWORD_Line>(out enemyLine))
            {
                if (enemyLine.view.Owner == view.Owner) return;
                Debug.Log(enemyLine.view);
                Debug.Log(enemyLine.view.ViewID);
                target = enemyLine.view.ViewID;
                RemoveHitTimer(target);
                Debug.Log("Hit Line");
                didHit = true;
            }

            SCR_SWORD_RIG enemyRig;
            if(other.gameObject.TryGetComponent<SCR_SWORD_RIG>(out enemyRig))
            {
                if (enemyRig.view.Owner == view.Owner) return;
                AddHitTimer(enemyRig.view.ViewID, enemyRig.playerRigID);
                didHit = true;
                Debug.Log("Hit Rig");
            }
            Debug.Log("Swing Complete " + gameObject.name);
        }
    }
    private void AddHitTimer(int lineID, int targetID)
    {
        // Send raise event to lines on local instances
        object[] content = new object[] { lineID, targetID }; // Array contains the target position and the IDs of the selected units
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
        PhotonNetwork.RaiseEvent(AddHitTimerEventCode, content, raiseEventOptions, SendOptions.SendReliable);
    }
    private void RemoveHitTimer(int ID)
    {
        object[] content = new object[] { ID }; // Array contains the target position and the IDs of the selected units
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
        PhotonNetwork.RaiseEvent(RemoveHitTimerEventCode, content, raiseEventOptions, SendOptions.SendReliable);
    }

    private void Swing()
    {
        if (!isExtended) return;
        if(velocity.magnitude >= startAmt)
        {
            if (!isSwing)
            {
                isSwing = true;
                pos[0] = renderBlade.transform.position;
                Debug.Log("Swinging");
            }
        }
        else
        {
            if(velocity.magnitude <= endAmt)
            {
                if (isSwing)
                {
                    isSwing = false;

                    pos[1] = renderBlade.transform.position;

                    float distance = Vector3.Distance(pos[0], pos[1]);
                    if (distance > minDistance)
                    {
                        Debug.Log("Distance " + distance);
                        Debug.DrawLine(pos[0], pos[1], Color.red, 15, false);
                        //line.SetLine(pos[0], pos[1]);
                        DrawLine(pos[0], pos[1]);
                    }
                    if (didHit)
                    {
                        didHit = false;
                        DisableBlade();
                    }

                    Debug.Log("Swing Complete");

                }
            }
        }
    }

    private void DrawLine(Vector3 startPos, Vector3 endPos)
    {
        object[] content = new object[] {startPos,endPos,view.ViewID}; // Array contains the target position and the IDs of the selected units
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
        PhotonNetwork.RaiseEvent(DrawLineEventCode, content, raiseEventOptions, SendOptions.SendReliable);
    }

    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;

        switch (eventCode)
        {
            case 30:
                object[] data = (object[])photonEvent.CustomData;
                //Debug.Log(data[2].ToString());
                int ID = (int)data[2];
                if (ID != view.ViewID) return;
                Vector3 _startPos = (Vector3)data[0];
                Vector3 _endPos = (Vector3)data[1];
                line.SetLine(_startPos, _endPos);
                break;
            default:
                break;
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new System.NotImplementedException();
    }

    // Make sure to observe in photon view!!! ** 
    // the way this project is setup, really not able to use rpc's
    // use .ismine, then on sword, view component that needs to change (script)

}
