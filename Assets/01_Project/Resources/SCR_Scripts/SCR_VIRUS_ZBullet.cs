﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_VIRUS_ZBullet : MonoBehaviour
{
    private RaycastHit hit;
    public LayerMask mask;
    public LayerMask mask2;
    public int damage;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Physics.SphereCast(transform.position, 1f, Vector3.up, out hit, mask))
        {
            if (hit.transform.gameObject.GetComponent<SCR_GLOBAL_NetId>())
            {
                int[] content = new int[] { hit.transform.gameObject.GetComponent<SCR_GLOBAL_NetId>().id, damage, SCR_Network_GameManager.instance.playerRig.photonView.ViewID }; // Array contains the target position and the IDs of the selected units
                RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                SendOptions sendOptions = new SendOptions { Reliability = true };
                PhotonNetwork.RaiseEvent(3, content, raiseEventOptions, sendOptions);
                Debug.Log("Damaged players " + 25);
            }
        }
        if (Physics.SphereCast(transform.position, 1f, Vector3.up, out hit, mask))
        {
            if (hit.transform.gameObject.GetComponent<SCR_GLOBAL_NetId>())
            {
                int[] content = new int[] { hit.transform.gameObject.GetComponent<SCR_GLOBAL_NetId>().id, 0, SCR_Network_GameManager.instance.playerRig.photonView.ViewID }; // Array contains the target position and the IDs of the selected units
                RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                SendOptions sendOptions = new SendOptions { Reliability = true };
                PhotonNetwork.RaiseEvent(3, content, raiseEventOptions, sendOptions);
                Debug.Log("Damaged players " + 25);
            }
        }

    }
}
