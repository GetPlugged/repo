﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
/// <summary>
/// Class used for common game rig logic
/// </summary>
public class SCR_BASE_RIG : MonoBehaviour, IPunObservable, IOnEventCallback
{
    [Header("Base Rig Properties")]
    public SCR_Network_GameManager gameManager;
    public PhotonView view;

    public Transform lHandModel;
    public Transform rHandModel;

    [Header("Global Objects")]
    public SCR_GLOBAL_Rig playerRig;
    public int playerRigID;
    public GameObject gPlayer;
    public GameObject gHead;
    public GameObject gBody;
    public GameObject gBodyModel;
    public GameObject gLHand;
    public GameObject gRHand;

    [Header("Child hands")]
    public GameObject lChild;
    public GameObject rChild;

    public Rigidbody r;
    public CapsuleCollider collider;

    [Header("Net Transforms")]
    public Transform[] netTransforms;
    public float lerpPosition;
    public float lerpRotation;

    private Vector3[] netPos;
    private Vector3[] netRot;
    private float lag;

    public SCR_GLOBAL_RenderPlayerModel leftRenderModel;
    public SCR_GLOBAL_RenderPlayerModel rightRenderModel;
    public SCR_GLOBAL_RenderPlayerModel gPlayerRenderModel;
    public Color color;


    [Header("Networking")]
    public int id;
    public float hurtTime;


    public int health;
    private Color hurtColor;
    private Color initColor;
    private int score;
    private float cHurtTime;
    private Player[] playerList;
    private ExitGames.Client.Photon.Hashtable playerInfo = new ExitGames.Client.Photon.Hashtable();

    // Start is called before the first frame update
    protected void Start()
    {
        gameManager = SCR_Network_GameManager.instance;
        view = this.gameObject.GetPhotonView();

        // Rigidbody Check
        if(GetComponent<Rigidbody>() == null)
        {
            r = gameObject.AddComponent<Rigidbody>();
        }
        else
        {
            r = GetComponent<Rigidbody>();
        }
        r.isKinematic = true;

        // Capsule Collider check
        if (GetComponent<CapsuleCollider>() == null)
        {
            collider = gameObject.AddComponent<CapsuleCollider>();
        }
        else
        {
            collider = GetComponent<CapsuleCollider>();
        }
        collider.isTrigger = false;
        collider.radius = .15f;
        //c.center = gBody.transform.position;//c.center = new Vector3(0, -.35f, 0);

        gameObject.layer = 12; // LayerMask.NameToLayer("Player Physics");

        int rigID = System.Convert.ToInt32(view.Owner.CustomProperties["playerRig"].ToString());
        leftRenderModel = PhotonNetwork.GetPhotonView(rigID).gameObject.GetComponent<SCR_GLOBAL_Rig>().renderModels[3];
        rightRenderModel = PhotonNetwork.GetPhotonView(rigID).gameObject.GetComponent<SCR_GLOBAL_Rig>().renderModels[4];
        //rInitRot = rSword.transform.rotation;
        if (view.IsMine)
        {
            playerRig = SCR_Network_GameManager.instance.playerRig;
            playerRigID = playerRig.GetComponent<PhotonView>().ViewID;
            gPlayerRenderModel = playerRig.renderModels[0];
            lHandModel = playerRig.leftHandModel;
            rHandModel = playerRig.rightHandModel;

            gPlayer = playerRig.gameObject;
            gBody = playerRig.body.gameObject;
            gBodyModel = playerRig.bodyModel.gameObject;
        }
        else
        {
            //int rigID = System.Convert.ToInt32(view.Owner.CustomProperties["playerRig"].ToString());
            //leftRenderModel = PhotonNetwork.GetPhotonView(rigID).gameObject.GetComponent<SCR_GLOBAL_Rig>().renderModels[3];
            //rightRenderModel = PhotonNetwork.GetPhotonView(rigID).gameObject.GetComponent<SCR_GLOBAL_Rig>().renderModels[4];

            playerRig = PhotonNetwork.GetPhotonView(rigID).gameObject.GetComponent<SCR_GLOBAL_Rig>();
            gPlayerRenderModel = playerRig.renderModels[0];
            SCR_GLOBAL_RenderPlayerModel bodyRenderModel = playerRig.renderModels[2];

            gPlayer = playerRig.gameObject;
            gBodyModel = playerRig.bodyModel.gameObject;

            lHandModel = PhotonNetwork.GetPhotonView(leftRenderModel.modelViewID).gameObject.transform;
            leftRenderModel = lHandModel.GetComponent<SCR_GLOBAL_RenderPlayerModel>();
            rHandModel = PhotonNetwork.GetPhotonView(rightRenderModel.modelViewID).gameObject.transform;
            rightRenderModel = rHandModel.GetComponent<SCR_GLOBAL_RenderPlayerModel>();
        }
        color = playerRig.color;

        netPos = new Vector3[netTransforms.Length];
        netRot = new Vector3[netTransforms.Length];


        hurtColor.r = .5f - playerRig.color.r;
        hurtColor.g = .5f - playerRig.color.g;
        hurtColor.b = .5f - playerRig.color.b;
        initColor = playerRig.color;

        playerList = PhotonNetwork.PlayerListOthers;

        //Init properties
        if (playerInfo.ContainsKey("playerHealth"))
        {
            playerInfo.Remove("playerHealth");
        }
        playerInfo.Add("playerHealth", 100);

        PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

        if (playerInfo.ContainsKey("playerScore"))
        {
            playerInfo.Remove("playerScore");
        }
        playerInfo.Add("playerScore", 0);

        PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

        health = 100;

    }
    protected void Update()
    {
        if (cHurtTime > 0f)
        {
            cHurtTime -= Time.deltaTime;
            playerRig.color = hurtColor;
        }
        else
            playerRig.color = initColor;
    }
    protected void FixedUpdate()
    {
        
    }

    // Update is called once per frame
    protected void LateUpdate()
    {
        lChild.transform.position = lHandModel.transform.position;
        lChild.transform.rotation = lHandModel.transform.rotation;
        rChild.transform.position = rHandModel.transform.position;
        rChild.transform.rotation = rHandModel.transform.rotation;
    }

    private void NetTransform()
    {
        if (view.IsMine)
        {
            rChild.transform.position = rHandModel.transform.position;
            rChild.transform.rotation = rHandModel.transform.rotation;

            lChild.transform.position = lHandModel.transform.position;
            lChild.transform.rotation = lHandModel.transform.rotation;

            Debug.Log("SEND");
            //Net send
            for (int i = 0; i < netTransforms.Length; i++)
            {
                netPos[i] = netTransforms[i].position;
                netRot[i] = netTransforms[i].eulerAngles;
            }
        }
        else
        {
            Debug.Log("RECIEVE");
            //Net receive
            for (int i = 0; i < netTransforms.Length; i++)
            {
                netTransforms[i].position = Vector3.Lerp(netTransforms[i].position, netPos[i], lerpPosition * Time.deltaTime);
                netTransforms[i].eulerAngles = new Vector3(Mathf.LerpAngle(netTransforms[i].eulerAngles.x, netRot[i].x, lerpRotation * Time.deltaTime), Mathf.LerpAngle(netTransforms[i].eulerAngles.y, netRot[i].y, lerpRotation * Time.deltaTime), Mathf.LerpAngle(netTransforms[i].eulerAngles.z, netRot[i].z, lerpRotation * Time.deltaTime)); ;
            }
        }
    }

    //public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    //{
    //    // Network child and collider positions
    //    if (stream.IsWriting)
    //    {
    //        stream.SendNext(transform.position);
    //        stream.SendNext(transform.rotation);

    //        stream.SendNext(lChild.transform.position);
    //        stream.SendNext(lChild.transform.rotation);

    //        stream.SendNext(rChild.transform.position);
    //        stream.SendNext(rChild.transform.rotation);

    //    }
    //    else
    //    {
    //        transform.position = (Vector3)stream.ReceiveNext();
    //        transform.rotation = (Quaternion)stream.ReceiveNext();

    //        lChild.transform.position = (Vector3)stream.ReceiveNext();
    //        lChild.transform.rotation = (Quaternion)stream.ReceiveNext();

    //        rChild.transform.position = (Vector3)stream.ReceiveNext();
    //        rChild.transform.rotation = (Quaternion)stream.ReceiveNext();
    //    }
    //}

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(netPos);
            stream.SendNext(netRot);
        }
        else
        {
            netPos = (Vector3[])stream.ReceiveNext();
            netRot = (Vector3[])stream.ReceiveNext();
        }
        lag = Mathf.Abs((float)(PhotonNetwork.Time - info.SentServerTimestamp));

        // Inverse lerping distance
        //if (Vector3.Distance(ship.transform.position, _netPos) > 20.0f) // more or less a replacement for CheckExitScreen function on remote clients
        //{
        //    ship.transform.position = _netPos;
        //}
    }

    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;

        if (view.IsMine)
        {
            switch (eventCode)
            {

                case 1:

                    //string data = (string)photonEvent.CustomData;

                    string s = (string)photonEvent.CustomData;
                    Color c;
                    ColorUtility.TryParseHtmlString(s, out c);
                    if (c != Color.white)
                        gameManager.worldColor = c;
                    Debug.Log("Received " + c);
                    break;

                case 2:

                    string[] ss = (string[])photonEvent.CustomData;
                    Color[] colors = new Color[2];

                    ColorUtility.TryParseHtmlString(ss[0], out colors[0]);
                    ColorUtility.TryParseHtmlString(ss[1], out colors[1]);


                    //worldManager.Pulse(colors[0],colors[1]);


                    break;


                case 3:


                    int[] d = (int[])photonEvent.CustomData;

                    if ( playerRig.state != 1) //d[0] == view.ViewID &&
                    {
                        d[1] = health - d[1];
                        health = d[1];
                        if (d[1] <= 0)
                        {
                            playerRig.state = 1;
                            string colorString = "#" + ColorUtility.ToHtmlStringRGBA(playerRig.color); // Array contains the target position and the IDs of the selected units
                            object[] content = new object[] { d[2], colorString }; // Array contains the target position and the IDs of the selected units
                            RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                            SendOptions sendOptions = new SendOptions { Reliability = true };
                            PhotonNetwork.RaiseEvent(4, content, raiseEventOptions, sendOptions);
                            Debug.Log("Killed by  " + d[2]);
                        }
                        else
                        {
                            cHurtTime = hurtTime;
                            //hurtClip.Play();
                        }

                        if (playerInfo.ContainsKey("playerHealth"))
                        {
                            playerInfo.Remove("playerHealth");
                        }
                        playerInfo.Add("playerHealth", health);

                        PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

                        Debug.Log("Damaged " + d);

                    }
                    break;

                case 4:

                    object[] v = (object[])photonEvent.CustomData;

                    if (PhotonNetwork.LocalPlayer == PhotonNetwork.GetPhotonView((int)v[0]).Owner)
                    {
                        score++;
                        if (playerInfo.ContainsKey("playerScore"))
                        {
                            playerInfo.Remove("playerScore");
                        }
                        playerInfo.Add("playerScore", score);
                        PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

                        bool t = false;
                        foreach (Player item in playerList)
                        {
                            if (score > System.Convert.ToInt32(item.CustomProperties["playerScore"].ToString()))
                                t = true;
                        }

                        if (t)
                        {
                            string content = "#" + ColorUtility.ToHtmlStringRGBA(initColor); // Array contains the target position and the IDs of the selected units
                            RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                            SendOptions sendOptions = new SendOptions { Reliability = true };
                            PhotonNetwork.RaiseEvent(1, content, raiseEventOptions, sendOptions);
                            Debug.Log("Sent " + playerRig.color);

                        }
                        else
                        {
                            string[] content = new string[] { "#" + ColorUtility.ToHtmlStringRGBA(initColor), (string)v[1] }; // Array contains the target position and the IDs of the selected units
                            RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                            SendOptions sendOptions = new SendOptions { Reliability = true };
                            PhotonNetwork.RaiseEvent(2, content, raiseEventOptions, sendOptions);
                        }
                    }

                    break;
            }


        }

    }

}
