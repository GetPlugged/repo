﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_GLOBAL_RenderPlayerModel : MonoBehaviourPunCallbacks, IPunObservable
{
    public int type = 0;
    public int obj = 0;
    public Color color;
    [Range(1f,1.2f)]
    public float intensity;
    [Range(0f, 1)]
    public float dissolve;
    public float colorLerp;
    public float dissovleLerp;
    public bool photonInstantiate;
    public bool late;
    [HideInInspector]
    public GameObject model;
    private Transform modelTransform;
    private SCR_GLOBAL_PlayerModel playerModel;
    private Transform tCache;
    private string netColor;
    private float netDissolve;
    private PhotonView view;
    private int netPref = -1;
    public int modelViewID;


    private ExitGames.Client.Photon.Hashtable playerInfo = new ExitGames.Client.Photon.Hashtable();

    private void Awake()
    {
        view = GetComponent<PhotonView>();

        if(!photonInstantiate)
        {
            if (view.IsMine)
            {
                
                //SCR_GLOBAL_DBAsset.initFlag = false;
                //if (playerInfo.ContainsKey(SCR_GLOBAL_DBAsset.GetPlayerAssetString(type, obj)))
                //{
                //    playerInfo.Remove(SCR_GLOBAL_DBAsset.GetPlayerAssetString(type, obj));
                //}
                //playerInfo.Add(SCR_GLOBAL_DBAsset.GetPlayerAssetString(type, obj), SCR_GLOBAL_DBAsset.GetAssetPref(type, obj));

                //PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

                model = SCR_GLOBAL_DBAsset.GetPlayerAsset(type, obj);

                playerModel = model.GetComponent<SCR_GLOBAL_PlayerModel>();
                playerModel.colorLerp = colorLerp;
                playerModel.dissolveLerp = dissovleLerp;
                modelTransform = model.transform;
            }
            else
            {
                Debug.Log(view.Owner.CustomProperties[SCR_GLOBAL_DBAsset.GetPlayerAssetString(type, obj)].ToString());
                netPref = System.Convert.ToInt32(view.Owner.CustomProperties[SCR_GLOBAL_DBAsset.GetPlayerAssetString(type, obj)].ToString());
                model = SCR_GLOBAL_DBAsset.GetAsset(type, obj, netPref);
                playerModel = model.GetComponent<SCR_GLOBAL_PlayerModel>();
                playerModel.colorLerp = colorLerp;
                playerModel.dissolveLerp = dissovleLerp;
                modelTransform = model.transform;
            }
            tCache = transform;
        }
        else if(photonInstantiate && view.IsMine)
        {

            model = SCR_GLOBAL_DBAsset.GetPhotonAsset(type, obj);
            modelViewID = model.GetComponent<PhotonView>().ViewID;
            view.RPC("RetreiveID", RpcTarget.OthersBuffered, modelViewID);
            modelTransform = model.transform;
            playerModel = model.GetComponent<SCR_GLOBAL_PlayerModel>();
            playerModel.colorLerp = colorLerp;
            playerModel.dissolveLerp = dissovleLerp;
            tCache = transform;
        }
    }

    private void Update()
    {
        if (!late)
        {


            if (!photonInstantiate)
            {
                if (!view.IsMine)
                {
                    ColorUtility.TryParseHtmlString(netColor, out color);
                    dissolve = netDissolve;
                }
                else
                {
                    netColor = "#" + ColorUtility.ToHtmlStringRGBA(color);
                    netDissolve = dissolve;
                }
                playerModel.color = color;
                playerModel.dissolve = dissolve;
                playerModel.intensity = intensity;
                modelTransform.position = tCache.position;
                modelTransform.rotation = tCache.rotation;
            }
            else if (view.IsMine)
            {
                playerModel.color = color;
                playerModel.dissolve = dissolve;
                playerModel.intensity = intensity;
                modelTransform.position = tCache.position;
                modelTransform.rotation = tCache.rotation;
            }
        }
    }

    private void LateUpdate()
    {
        if (late)
        {


            if (!photonInstantiate)
            {
                if (!view.IsMine)
                {
                    ColorUtility.TryParseHtmlString(netColor, out color);
                    dissolve = netDissolve;
                }
                else
                {
                    netColor = "#" + ColorUtility.ToHtmlStringRGBA(color);
                    netDissolve = dissolve;
                }
                playerModel.color = color;
                playerModel.dissolve = dissolve;
                playerModel.intensity = intensity;
                modelTransform.position = tCache.position;
                modelTransform.rotation = tCache.rotation;
            }
            else if (view.IsMine)
            {
                playerModel.color = color;
                playerModel.dissolve = dissolve;
                playerModel.intensity = intensity;
                modelTransform.position = tCache.position;
                modelTransform.rotation = tCache.rotation;
            }
        }
    }


    private void OnDestroy()
    {
        if(!photonInstantiate)
            Destroy(model);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (!photonInstantiate)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(netColor);
                stream.SendNext(netDissolve);
            }
            else
            {
                netColor = (string)stream.ReceiveNext();
                netDissolve = (float)stream.ReceiveNext();
            }
        }
    }

    [PunRPC]
    void RetreiveID(int id)
    {
        if(!view.IsMine)
            modelViewID = id;
    }
}