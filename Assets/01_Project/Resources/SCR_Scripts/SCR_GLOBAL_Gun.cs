﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class SCR_GLOBAL_Gun : MonoBehaviour
{
    public bool triggered;
    public PhotonView view;

    [Space(10)]

    public float fireRate;
    public int shots;
    public int bullets;
    public float accuracy;
    public float flashFade;

    [Space(10)]

    public AudioSource sound;

    [Space(10)]

    public Transform[] kickbackFabs;
    public Vector3[] recoil;
    public float[] kick;
    public float[] recovery;
    public GameObject[] barrels;
    public SCR_GLOBAL_Flash[] flashes;

    [Space(10)]

    public GameObject[] localFabs;
    public string[] netFabs;

    private int curShots;
    private int curBarrel;
    private Vector3[] recoilVelo;
    private Vector3[] recoilVelo2;
    private Vector3[] kRef;
    private float coolDown;

    // Start is called before the first frame update
    void Start()
    {
        if (!view)
            enabled = false;
        kRef = new Vector3[kickbackFabs.Length];
        recoilVelo = new Vector3[kickbackFabs.Length];
        recoilVelo2 = new Vector3[kickbackFabs.Length];

        for (int i = 0; i < kickbackFabs.Length; i++)
        {
            kRef[i] = kickbackFabs[i].localPosition;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!view.IsMine)
            enabled = false;

        if (triggered)
            Fire();

        foreach (SCR_GLOBAL_Flash item in flashes)
        {
            item.material.SetFloat("_DissolveAmount", 1);

        }


        if (curShots > 0 && coolDown < 0)
        {
            if (curBarrel < 1)
                curBarrel = barrels.Length;

            for (int i = 0; i < bullets; i++)
            {
                sound.PlayOneShot(sound.clip);

                flashes[curBarrel - 1].material.SetFloat("_DissolveAmount", 0);

                foreach (var item in localFabs)
                {
                    GameObject.Instantiate(item, barrels[curBarrel - 1].transform.position, new Quaternion(barrels[curBarrel - 1].transform.rotation.x + Random.Range(accuracy,-accuracy), barrels[curBarrel - 1].transform.rotation.y + Random.Range(accuracy, -accuracy), barrels[curBarrel - 1].transform.rotation.z + Random.Range(accuracy, -accuracy), barrels[curBarrel - 1].transform.rotation.w));
                }
                foreach (var item in netFabs)
                {
                    PhotonNetwork.Instantiate("PRE_Prefabs/"+item, barrels[curBarrel - 1].transform.position, barrels[curBarrel - 1].transform.rotation);
                }
            }

            for (int i = 0; i < kickbackFabs.Length; i++)
            {
                kickbackFabs[i].localPosition = new Vector3(Mathf.SmoothDamp(kickbackFabs[i].localPosition.x, kickbackFabs[i].localPosition.x + recoil[i].x,ref recoilVelo2[i].x, kick[i]), Mathf.SmoothDamp(kickbackFabs[i].localPosition.y, kickbackFabs[i].localPosition.y + recoil[i].y, ref recoilVelo2[i].y, kick[i]), Mathf.SmoothDamp(kickbackFabs[i].localPosition.z, kickbackFabs[i].localPosition.z + recoil[i].z, ref recoilVelo2[i].z, kick[i]));
            }

            coolDown = fireRate;
            curShots--;
            curBarrel--;
        }

        coolDown -= Time.deltaTime;

        for (int i = 0; i < kickbackFabs.Length; i++)
        {
            kickbackFabs[i].localPosition = new Vector3(Mathf.SmoothDamp(kickbackFabs[i].localPosition.x,kRef[i].x, ref recoilVelo[i].x, recovery[i]), Mathf.SmoothDamp(kickbackFabs[i].localPosition.y, kRef[i].y, ref recoilVelo[i].y, recovery[i]), Mathf.SmoothDamp(kickbackFabs[i].localPosition.z, kRef[i].z, ref recoilVelo[i].z, recovery[i]));
        }
    }

    public void Fire()
    {        
        if (curShots < 1)
        {
            curShots = shots;
        }
    }
}