﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_GLOBAL_NetBullet : MonoBehaviour
{
    private RaycastHit hit;
    public LayerMask mask;
    public int damage;
    public int weaponIndex;
    public bool left;
    public string bullet;
    private LineRenderer line;
    // Start is called before the first frame update
    void Start()
    {
        SCR_Network_GameManager gm = SCR_Network_GameManager.instance;
        switch (gm.gamemode)
        {
            case (SCR_Network_GameManager.GameMode.bikes):
                Bikes(gm);
                break;

            case (SCR_Network_GameManager.GameMode.virus):
                Virus(gm);
                break;

        }

    }
    private void Update()
    {
        Destroy(this.gameObject);
    }

    private void Bikes(SCR_Network_GameManager gm)
    {
        Shoot();
        gm.gameRig.GetComponent<SCR_BIKES_Rig>().ammo[weaponIndex]--;
    }

    private void Virus(SCR_Network_GameManager gm)
    {
        if (left)
        {
            if(gm.gameRig.GetComponent<SCR_VIRUS_Rig>().ammoL>0)
            {
                Shoot();
                gm.gameRig.GetComponent<SCR_VIRUS_Rig>().ammoL--;
                PhotonNetwork.Instantiate("PRE_Prefabs/" + bullet, transform.position, transform.rotation);
            }
            else
            {
                //Click
            }
        }
        else
        {
            if (gm.gameRig.GetComponent<SCR_VIRUS_Rig>().ammoR > 0)
            {
                Shoot();
                gm.gameRig.GetComponent<SCR_VIRUS_Rig>().ammoR--;
                PhotonNetwork.Instantiate("PRE_Prefabs/" + bullet, transform.position, transform.rotation);
            }
            else
            {
                //Click
            }
        }
    }

    private void Shoot()
    {
        if (Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity, mask))
        {
            if (hit.transform.gameObject.GetComponent<SCR_GLOBAL_NetId>())
            {
                int[] content = new int[] { hit.transform.gameObject.GetComponent<SCR_GLOBAL_NetId>().id, damage, SCR_Network_GameManager.instance.playerRig.photonView.ViewID }; // Array contains the target position and the IDs of the selected units
                RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                SendOptions sendOptions = new SendOptions { Reliability = true };
                PhotonNetwork.RaiseEvent(3, content, raiseEventOptions, sendOptions);
                Debug.Log("Damaged players " + 25);
            }
        }
    }
}
