﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_SWORD_Trail : MonoBehaviour
{
    public TrailsFX.TrailEffect trail;
    public SCR_GLOBAL_PlayerModel model;
    private float duration;
    private float rate;
    // Start is called before the first frame update
    void Start()
    {
        trail.color = model.color;
        duration = trail.duration;
        rate = duration / model.maxDissolve;
    }
    private void Update()
    {
        trail.color = model.color;
        //trail.colorOverTime
        trail.duration = duration - (rate * model.dissolve);
        if(trail.duration <= .005f) trail.duration = 0;
    }
}
