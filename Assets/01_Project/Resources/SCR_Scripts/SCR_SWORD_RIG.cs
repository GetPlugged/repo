﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Photon.Realtime;
using Photon.Pun;
using ExitGames.Client.Photon;

public class SCR_SWORD_RIG : SCR_BASE_RIG, IPunObservable
{
    public SCR_SWORD_Trigger lTrigger;
    public SCR_SWORD_Trigger rTrigger;

    [Space(10)]
    public GameObject lSword;
    public GameObject lHilt;
    public GameObject lBlade;


    public GameObject rSword;
    public GameObject rHilt;
    public GameObject rBlade;

    private Vector3 rVel;
    private Vector3[] rightPos = new Vector3[2];
    private Vector3 rLastPos;

    

    private float nextHit = 0;
    private float cooldown = 2.5f; // need to change this into sheathe. > when strike, need to unsheathe next sword. > unsheate can be shader the goes upwards revealing blade
    public float threshold = 1.25f;

  
    private bool isSwing = false;
    private Quaternion rInitRot;

    [Space(10)]
    public Material material;
    public GameObject linePrefab;

    private int hitAmt;
    private float hitTime;
    public bool isAlive;



    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        lTrigger.Initalize();
        rTrigger.Initalize();
        //// Left Sword
        //lTrigger = lChild.GetComponent<SCR_SWORD_Trigger>();
        ////lTrigger.view = lSword.GetComponent<PhotonView>();
        //lTrigger.parent = this;
        //lTrigger.blade = lBlade;
        //lTrigger.renderBlade = lBlade.GetComponentInChildren<SCR_GLOBAL_RenderPlayerModel>();
        //lTrigger.renderHilt = lHilt.GetComponentInChildren<SCR_GLOBAL_RenderPlayerModel>();
        //lTrigger.hilt = lHilt;
        //lTrigger.isExtended = false;
        //lTrigger.renderBlade.color = playerRig.color;
        ////lTrigger.line = lBlade.GetComponent<LineRenderer>();   //render slash
        //lTrigger.linePrefab = Instantiate(linePrefab);
        //lTrigger.line = linePrefab.GetComponent<SCR_Swords_Line>();
        //lTrigger.line.color = playerRig.color;
        //lTrigger.line.owner = view.Owner;
        //lTrigger.line.lineRenderer.sharedMaterial = new Material(material);
        //lTrigger.EnableBlade();

        //// Right Sword
        //rTrigger = rChild.GetComponent<SCR_SWORD_Trigger>();
        ////rTrigger.view = rSword.GetComponent<PhotonView>();
        //rTrigger.parent = this;
        //rTrigger.blade = rBlade;
        //rTrigger.renderBlade = rBlade.GetComponentInChildren<SCR_GLOBAL_RenderPlayerModel>();
        //rTrigger.renderHilt = rHilt.GetComponentInChildren<SCR_GLOBAL_RenderPlayerModel>();
        //rTrigger.hilt = rHilt;
        //rTrigger.isExtended = false;
        //rTrigger.renderBlade.color = playerRig.color;
        ////rTrigger.line = rBlade.GetComponent<LineRenderer>();  //render slash
        //rTrigger.linePrefab = Instantiate(linePrefab);
        //rTrigger.line = linePrefab.GetComponent<SCR_Swords_Line>();
        //rTrigger.line.color = playerRig.color;
        //rTrigger.line.owner = view.Owner;
        //rTrigger.line.lineRenderer.sharedMaterial = new Material(material);
        //rTrigger.EnableBlade();
    }

    // Update is called once per frame
    new void Update()
    {
        if (SCR_Network_GameManager.instance.worldManager.state != 4) return;
        GetState();
        if (!isAlive) return;
        base.Update();

        if (view.IsMine)
        {
            if (SteamVR_Input.GetState("GrabGrip", SteamVR_Input_Sources.LeftHand) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetButton("LTouchButtons"))
            {
                lTrigger.view.RPC("EnableBlade", RpcTarget.All, null);
                //lTrigger.EnableBlade();
            }
            if (SteamVR_Input.GetState("GrabGrip", SteamVR_Input_Sources.RightHand) || Input.GetKeyDown(KeyCode.RightArrow) || Input.GetButton("RTouchButtons"))
            {
                rTrigger.view.RPC("EnableBlade", RpcTarget.All, null);
                //rTrigger.EnableBlade();
            }
        }
        if (hitAmt > 0)
        {
            CheckHitTime();
        }
    }
    private void GetState()
    {
        int rigState = playerRig.state;
        switch (rigState)
        {
            case 0:
                isAlive = true;
                if (Input.GetKeyDown(KeyCode.K))
                {
                    int[] content = new int[] { id, 25, view.ViewID }; // Array contains the target position and the IDs of the selected units
                    RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                    SendOptions sendOptions = new SendOptions { Reliability = true };
                    PhotonNetwork.RaiseEvent(3, content, raiseEventOptions, sendOptions);
                    Debug.Log("Damaged players " + 25);
                    //Alive();
                }


                break;

            case 3:
                isAlive = false;
                if (Input.GetKeyDown(KeyCode.K))
                {
                    object[] content = new object[] { view.ViewID }; // Array contains the target position and the IDs of the selected units
                    RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                    SendOptions sendOptions = new SendOptions { Reliability = true };
                    PhotonNetwork.RaiseEvent(4, content, raiseEventOptions, sendOptions);
                    // Dead();
                }


                    break;
        }
    }

    new void FixedUpdate()
    {
        base.FixedUpdate();
        transform.position = gBodyModel.transform.position;
        //collider.center = gBodyModel.transform.position;
    }   
    new void LateUpdate()
    {
        base.LateUpdate();
    }

    public void CheckHitTime()
    {
        if(Time.time >= hitTime)
        {
            health--;
            hitAmt--;
        }
        if(hitAmt < 0) hitAmt = 0;
    }

    
}
