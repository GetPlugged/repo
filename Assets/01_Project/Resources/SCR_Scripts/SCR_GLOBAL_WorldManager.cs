﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_GLOBAL_WorldManager : MonoBehaviour
{
    public Material clouds;
    public Material skyBox;
    public float smooth;
    [Range(.1f,1f)]
    public float[] timers;
    public AudioClip[] timeClips;
    public AudioSource[] spawnClips;
    public AudioSource ambience;

    public Renderer[] renderers;
    public float[] maxDissolve;
    public float preIntesity;

    public float pulseTime;

    private Material[] materials;
    public Color color;
    private Color modelColor;

    private SCR_Network_GameManager gameManager;
    public int state;
    private float[] cTimes;
    private float pretime;
    private float preCtime;
    private float dissolve;
    private float dissolveLerp;
    private float cInts;
    private int nLevel = 0;
    private AudioSource audio;
    private bool preref;
    private PhotonView view;
    private float cPulseTime;
    private Color[] pulseColor;
    private readonly byte MoveUnitsToTargetPositionEvent = 1;

    void Start()
    {
        gameManager = SCR_Network_GameManager.instance;
        view = gameManager.photonView;
        gameManager.worldManager = GetComponent<SCR_GLOBAL_WorldManager>();
        materials = new Material[renderers.Length];
        audio = GetComponent<AudioSource>();
        audio.clip = timeClips[0];
        for (int i = 0; i < renderers.Length; i++)
        {
            materials[i] = renderers[i].material;
        }
        gameManager.worldColor = Color.black;
        color = Color.black;
        modelColor = Color.white;
        if (skyBox)
        skyBox.SetColor("_Color2", Color.black);
        if(clouds)
        clouds.SetColor("_EmissionColor", Color.black);
        state = 0;
        cTimes = new float[timers.Length];
        for (int i = 0; i < cTimes.Length; i++)
        {
            cTimes[i] = 1f;
        }
        dissolve = 1f;
        dissolveLerp = 4f;
        foreach (Material item in materials)
        {
            item.SetFloat("_DissolveAmount", 1f);
        }

        pretime = cTimes[3]/4;
        preCtime = pretime;

        if (gameManager.playerRig)
        gameManager.playerRig.color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f);


    }


    void Update()
    {

        //Coroutine not necessary for time step
        switch (state)
        {
            
            //Init
            case 0:

                if (cTimes[state] > 0)
                    cTimes[state] -= timers[state] * Time.deltaTime;
                else
                {
                    spawnClips[0].Play();
                    if(gameManager.gamemode == SCR_Network_GameManager.GameMode.virus)
                        modelColor = Color.green;

                    state++;
                }
                break;
            
            //Dissolve
            case 1:

                if (cTimes[state] > 0)
                {
                    cTimes[state] -= timers[state] * Time.deltaTime;

                    dissolve = 0f;
                }
                else
                {
                    if (clouds || skyBox)
                    {
                        spawnClips[1].Play();
                        ambience.Play();
                    }

                    state++;
                }

                break;

            //Light
            case 2:

                if (cTimes[state] > 0)
                {
                    if (clouds || skyBox)
                    {
                        cTimes[state] -= timers[state] * Time.deltaTime;
                        if(gameManager.gamemode != SCR_Network_GameManager.GameMode.virus)
                        gameManager.worldColor = Color.white;
                        else
                            gameManager.worldColor = Color.green;

                        cInts += preIntesity * Time.deltaTime;
                        color = gameManager.worldColor * cInts;
                    }
                    else
                        state++;
                }
                else
                    state++;

                break;
            
            //Timer
            case 3:

                if (cTimes[state] > 0)
                {
                    cTimes[state] -= timers[state] * Time.deltaTime;
                    color = gameManager.worldColor;

                    if(preCtime > 0)
                        preCtime -= timers[state] * Time.deltaTime;
                    else if (gameManager.gamemode != SCR_Network_GameManager.GameMode.home)
                    {
                        preCtime = pretime;
                        audio.Play();
                    }

                    //Timer play
                }
                else
                {
                    audio.clip = timeClips[1];

                    if (gameManager.gamemode != SCR_Network_GameManager.GameMode.home)
                        audio.Play();

                    //gameManager.worldColor = gameManager.playerRig.color;

                    cInts = 0;

                    state++;
                }

                break;

            //Game
            case 4:

                if(cPulseTime<=0f)
                {

                    color = gameManager.worldColor;
                    if (color == Color.white)
                        modelColor = color;
                    else if (gameManager.gamemode != SCR_Network_GameManager.GameMode.virus)
                    {
                        modelColor.r = 1f - color.r;
                        modelColor.g = 1f - color.g;
                        modelColor.b = 1f - color.b;
                    }
                }
                else
                {
                    //cPulseTime -= Time.deltaTime;
                    //if (cPulseTime > pulseTime / 2)
                    //    modelColor = pulseColor[0];
                    //else
                    //    modelColor = pulseColor[1];

                }

                // for testing gamemode switching
                if (Input.GetKeyDown(KeyCode.Alpha1))
                {
                    nLevel = 1;
                    state++;
                }
                if (Input.GetKeyDown(KeyCode.Alpha2))
                {
                    nLevel = 2;
                    state++;
                }
                if (Input.GetKeyDown(KeyCode.Alpha3))
                {
                    nLevel = 3;
                    state++;
                }
                if (Input.GetKeyDown(KeyCode.Alpha4))
                {
                    nLevel = 4;
                    state++;
                }
                if (Input.GetKeyDown(KeyCode.Alpha5))
                {
                    nLevel = 5;
                    state++;
                }
                if (Input.GetKeyDown(KeyCode.Alpha6))
                {
                    nLevel = 6;
                    state++;
                }
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    SCR_Network_GameManager.instance.LeaveRoom();
                }
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    SCR_Network_GameManager.instance.LeaveRoom();
                }
                break;
            
            //Quit
            case 5:

                if (cTimes[state] > 0)
                {
                    cTimes[state] -= timers[state] * Time.deltaTime;
                    cInts += preIntesity * Time.deltaTime;

                    color = gameManager.worldColor * cInts;
                    modelColor = color;
                    //Misc
                }
                else
                    state++;
                break;

            //Light/Dissolve
            case 6:

                if (cTimes[state] > 0)
                {
                    cTimes[state] -= timers[state] * Time.deltaTime;

                    color = Color.black;
                    dissolve = 1;
                }
                else
                    state++;
                break;
            
            //Home
            case 7:
                if(skyBox)
                skyBox.SetColor("_Color2", Color.black);
                gameManager.LoadGamemodeInt(nLevel);
                if(nLevel == 6 )
                {
                    PhotonNetwork.SendRate = 50;
                    PhotonNetwork.SerializationRate = 25;
                    Debug.Log("Set net rate for bikes");
                }
                else
                {
                    PhotonNetwork.SendRate = 20;
                    PhotonNetwork.SerializationRate = 10;
                    Debug.Log("Set net rate for all");

                }
                state++;
                break;
        }

        if (gameManager.gamemode == SCR_Network_GameManager.GameMode.home && gameManager.playerRig)
        {
            gameManager.playerRig.gameObject.transform.position = Vector3.zero;
            gameManager.playerRig.gameObject.transform.eulerAngles = Vector3.zero;
            color = gameManager.playerRig.color;
            modelColor = color;
        }

        if (skyBox)
            skyBox.SetColor("_Color2", Color.Lerp(skyBox.GetColor("_Color2"), color, smooth * Time.deltaTime));

        if (clouds)
            clouds.SetColor("_EmissionColor", Color.Lerp(clouds.GetColor("_EmissionColor"), color, smooth * Time.deltaTime));

        for (int i = 0; i < materials.Length; i++)
        {
            materials[i].SetColor("_EmissionColor", Color.Lerp(materials[i].GetColor("_EmissionColor"), modelColor * 2f, smooth * Time.deltaTime));
            materials[i].SetFloat("_DissolveAmount", Mathf.Lerp(materials[i].GetFloat("_DissolveAmount"), dissolve * maxDissolve[i], dissolveLerp * Time.deltaTime));
        }


    }

    public void Pulse(Color color1, Color color2)
    {
        //cPulseTime = pulseTime;
        //pulseColor[0] = color1;
        //pulseColor[1] = color2;
    }


}
