﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Valve.VR;
using RootMotion;
using ExitGames.Client.Photon;
using UnityEngine.SpatialTracking;

public class SCR_MECHS_Rig : MonoBehaviourPunCallbacks, IPunObservable, IOnEventCallback
{
    public int id;
    public float deadTime;


    public float lerpPosition;
    public float lerpRotation;


    public Transform[] netTransforms;
    public Vector3[] netPos;
    public Vector3[] netRot;


    public int lIndex;
    public int RIndex;
    private Transform[] lArsenel;
    private Transform[] rArsenel;
    public SCR_GLOBAL_RenderPlayerModel[] lArsenelModels;
    public SCR_GLOBAL_RenderPlayerModel[] rArsenelModels;
    public Transform[] lArsenelSecondGrip;
    public Transform[] rArsenelSecondGrip;
    public GameObject[] pickups;

    public SCR_GLOBAL_RenderPlayerModel[] models;


    public Transform prop2;
    public float propOffset;


    public Transform player;
    public SCR_GLOBAL_Rig playerRig;
    public Transform playerSnap;
    public Transform cam;

    public CharacterController controller;
    public RootMotion.FinalIK.FullBodyBipedIK ik;
    public RootMotion.Dynamics.PuppetMaster puppetMaster;

    public float boost;
    public float boostd;

    public float fall;

    public float drop;
    public float rise;
    public float climb;

    public float booster;

    public float grav;

    public float playerSmooth;


    public float armSmooth;

    public Transform mechRoot;

    public Transform mechRootBase;

    public Transform rigSnap;
    public Transform mechSnap;


    public Transform lHand;
    public Transform rHand;

    public Transform lTarget;
    public Transform rTarget;

    public float speed;


    public Vector3 lOffset;
    public Vector3 rOffset;


    public Vector3 multiplier;

    public TrackedPoseDriver headPose;





    private Vector3 playerSmoothV;


    private Vector3 lHSmoothV;
    private Quaternion lRSmoothV;
    private Quaternion rRSmoothV;

    private Vector3 rHSmoothV;



    private Vector2 axisL;

    private RaycastHit hit;

    private bool click;

    private bool grippedL;
    private bool grippedR;
    private PhotonView view;
    private SCR_Network_GameManager gameManager;
    private SCR_GLOBAL_WorldManager worldManager;
    private int health;
    public float hurtTime;
    private float cHurtTime;
    public AudioSource hurtClip;
    private ExitGames.Client.Photon.Hashtable playerInfo = new ExitGames.Client.Photon.Hashtable();
    private int score;
    private Color initColor;
    private Player[] playerList;
    private Color hurtColor;
    public int netState;
    private float cDeadTime;
    private float deadStateTime;
    private float CdeadStateTime;
    public int deadState;
    public AudioSource spawnClip;
    private bool spawnIndex;
    private bool lFree;
    private bool rFree;

    private float limbMult;


    // Start is called before the first frame update
    void Start()
    {
        netPos = new Vector3[netTransforms.Length];
        netRot = new Vector3[netTransforms.Length];
        worldManager = SCR_Network_GameManager.instance.worldManager;
        view = GetComponent<PhotonView>();

        if (view.IsMine)
        {
            gameManager = SCR_Network_GameManager.instance;

            //Init vars
            player = gameManager.playerRig.gameObject.transform;
            playerRig = gameManager.playerRig;
            playerRig.reflectionPlane.gameObject.SetActive(false);
            cam = playerRig.cam;
            //Init properties
            if (playerInfo.ContainsKey("playerHealth"))
            {
                playerInfo.Remove("playerHealth");
            }
            playerInfo.Add("playerHealth", 100);

            PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

            if (playerInfo.ContainsKey("playerScore"))
            {
                playerInfo.Remove("playerScore");
            }
            playerInfo.Add("playerScore", 0);

            PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

            health = 100;
            initColor = playerRig.color;
            hurtColor.r = .5f - playerRig.color.r;
            hurtColor.g = .5f - playerRig.color.g;
            hurtColor.b = .5f - playerRig.color.b;

            foreach (SCR_GLOBAL_RenderPlayerModel item in models)
            {
                item.dissolve = 1;
            }


            lArsenel = new Transform[lArsenelModels.Length];
            rArsenel = new Transform[rArsenelModels.Length];
            for (int i = 0; i < lArsenel.Length; i++)
            {
                lArsenel[i] = lArsenelModels[i].gameObject.transform;
            }
            for (int i = 0; i < rArsenel.Length; i++)
            {
                rArsenel[i] = rArsenelModels[i].gameObject.transform;
            }
        }
        else
            headPose.enabled = false;
        ik.solver.effectors[5].target = lTarget.transform;
        ik.solver.effectors[6].target = rTarget.transform;

    }

    void Update()
    {
        if (view.IsMine)
        {
            foreach (SCR_GLOBAL_RenderPlayerModel item in models)
            {
                item.color = playerRig.color;
            }
        }



        int worldState = worldManager.state;
        switch(worldState)
        {
            case 1:

                if (view.IsMine)
                {
                    foreach (SCR_GLOBAL_RenderPlayerModel item in models)
                    {
                        item.dissolve = 0;
                    }

                    if (!spawnIndex)
                    {
                        spawnClip.Play();
                        spawnIndex = true;
                    }
                }
                break;

            case 4:
                        StateFour();

                break;
            case 6:
                foreach (SCR_GLOBAL_RenderPlayerModel item in models)
                {
                    item.dissolve = 1;
                }

                break;



        }

        if (view.IsMine)
        {
            //Snapping
            player.position = Vector3.SmoothDamp(player.position, playerSnap.position, ref playerSmoothV, playerSmooth * Time.deltaTime);
            rigSnap.position = playerRig.body.position;
            rigSnap.rotation = playerRig.body.rotation;
            mechRootBase.position = mechSnap.position;
            mechRoot.eulerAngles = new Vector3(0, playerRig.body.localEulerAngles.y, 0);
            lHand.position = playerRig.leftHandPose.position;
            rHand.position = playerRig.rightHandPose.position;
            prop2.position = lHand.position;
            prop2.forward = playerRig.leftHandPose.up;
            limbMult = 1;
            lTarget.rotation = SmoothDamp(lTarget.rotation, playerRig.leftHandPose.rotation, ref lRSmoothV, armSmooth * Time.deltaTime);
            rTarget.rotation = SmoothDamp(rTarget.rotation, playerRig.rightHandPose.rotation, ref rRSmoothV, armSmooth * Time.deltaTime);
            lTarget.localPosition = Vector3.SmoothDamp(lTarget.localPosition, Vector3.Scale(lHand.localPosition + lOffset, multiplier * limbMult), ref lHSmoothV, armSmooth * Time.deltaTime);
            rTarget.localPosition = Vector3.SmoothDamp(rTarget.localPosition, Vector3.Scale(rHand.localPosition + rOffset, multiplier * limbMult), ref rHSmoothV, armSmooth * Time.deltaTime);
        }

    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {

        if (view && !view.IsMine && targetPlayer == view.Owner)
        {

            //ColorUtility.TryParseHtmlString((string)targetPlayer.CustomProperties["color"], out sColor);

            int h = (int)targetPlayer.CustomProperties["playerHealth"];
            Debug.Log(health);
            if (h <= 0)
            {
                netState = 1;
            }
            Debug.Log(health);
        }
    }

    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;

        if (view.IsMine)
        {


            switch (eventCode)
            {

                case 1:

                    //string data = (string)photonEvent.CustomData;

                    string s = (string)photonEvent.CustomData;
                    Color c;
                    ColorUtility.TryParseHtmlString(s, out c);
                    if (c != Color.white)
                        gameManager.worldColor = c;
                    Debug.Log("Received " + c);
                    break;

                case 2:

                    string[] ss = (string[])photonEvent.CustomData;
                    Color[] colors = new Color[2];

                    ColorUtility.TryParseHtmlString(ss[0], out colors[0]);
                    ColorUtility.TryParseHtmlString(ss[1], out colors[1]);


                    worldManager.Pulse(colors[0], colors[1]);


                    break;


                case 3:


                    int[] d = (int[])photonEvent.CustomData;

                    if (d[0] == photonView.ViewID && playerRig.state != 1)
                    {
                        d[1] = health - d[1];
                        health = d[1];
                        if (d[1] <= 0)
                        {
                            playerRig.state = 1;
                            string colorString = "#" + ColorUtility.ToHtmlStringRGBA(playerRig.color); // Array contains the target position and the IDs of the selected units
                            object[] content = new object[] { d[2], colorString }; // Array contains the target position and the IDs of the selected units
                            RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                            SendOptions sendOptions = new SendOptions { Reliability = true };
                            PhotonNetwork.RaiseEvent(4, content, raiseEventOptions, sendOptions);
                            Debug.Log("Killed by  " + d[2]);
                        }
                        else
                        {
                            cHurtTime = hurtTime;
                            hurtClip.Play();
                        }

                        if (playerInfo.ContainsKey("playerHealth"))
                        {
                            playerInfo.Remove("playerHealth");
                        }
                        playerInfo.Add("playerHealth", health);

                        PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

                        Debug.Log("Damaged " + d);

                    }
                    break;

                case 4:

                    object[] v = (object[])photonEvent.CustomData;

                    if (PhotonNetwork.LocalPlayer == PhotonNetwork.GetPhotonView((int)v[0]).Owner)
                    {
                        score++;
                        if (playerInfo.ContainsKey("playerScore"))
                        {
                            playerInfo.Remove("playerScore");
                        }
                        playerInfo.Add("playerScore", score);
                        PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

                        bool t = false;
                        foreach (Player item in playerList)
                        {
                            if (score > System.Convert.ToInt32(item.CustomProperties["playerScore"].ToString()))
                                t = true;
                        }

                        if (t)
                        {
                            string content = "#" + ColorUtility.ToHtmlStringRGBA(initColor); // Array contains the target position and the IDs of the selected units
                            RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                            SendOptions sendOptions = new SendOptions { Reliability = true };
                            PhotonNetwork.RaiseEvent(1, content, raiseEventOptions, sendOptions);
                            Debug.Log("Sent " + playerRig.color);

                        }
                        else
                        {
                            string[] content = new string[] { "#" + ColorUtility.ToHtmlStringRGBA(initColor), (string)v[1] }; // Array contains the target position and the IDs of the selected units
                            RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                            SendOptions sendOptions = new SendOptions { Reliability = true };
                            PhotonNetwork.RaiseEvent(2, content, raiseEventOptions, sendOptions);
                        }
                    }

                    break;

                case 5:

                    int[] p = (int[])photonEvent.CustomData;
                    Debug.Log("Received");

                    if (view.ViewID == p[2])
                    {
                        Debug.Log("Match");

                        if (p[0] == 0)
                        {

                            if (p[3] == 0)
                            {
                                if (lFree)
                                    lIndex = p[1];
                                //if (p[4] < 0)
                                //    ammoL = ammo[p[1]];
                                //else
                                //    ammoL = p[4];
                            }
                            else if (p[3] == 1)
                            {
                                if (rFree)
                                    RIndex = p[1];
                                //if (p[4] < 0)
                                //    ammoR = ammo[p[1]];
                                //else
                                //    ammoR = p[4];
                            }
                        }
                        else if (p[0] == 1)
                        {

                            if (p[3] == 0)
                            {
                                if (lFree && rFree)
                                    lIndex = p[1];
                                //if (p[4] < 0)
                                //    ammoL = ammo[p[1]];
                                //else
                                //    ammoL = p[4];
                            }
                            else if (p[3] == 1)
                            {
                                if (rFree && lFree)
                                    RIndex = p[1];
                                //if (p[4] < 0)
                                //    ammoR = ammo[p[1]];
                                //else
                                //    ammoR = p[4];
                            }
                        }

                    }



                    break;

            }


        }

    }


    private void StateFour()
    {


        if (view.IsMine)
        {
            int rigState = playerRig.state;
            switch (rigState)
            {

                case 0:

                    if (puppetMaster.pinWeight != .5f)
                        puppetMaster.pinWeight = .5f;

                    foreach (SCR_GLOBAL_RenderPlayerModel item in models)
                    {
                        if (item.dissolve != 0)
                            item.dissolve = 0;
                    }
                    //Input
                    axisL = AxisL();
                    grippedL = GripL();
                    grippedR = GripR();
                    click = Click();



                    if (cHurtTime > 0f)
                    {
                        cHurtTime -= Time.deltaTime;
                        playerRig.color = hurtColor;
                    }
                    else
                        playerRig.color = initColor;

                    foreach (SCR_GLOBAL_RenderPlayerModel item in models)
                    {
                        if (item.dissolve != 0)
                            item.dissolve = 0;
                    }



                    if (Input.GetKeyDown(KeyCode.C))
                    {
                        string content = "#" + ColorUtility.ToHtmlStringRGBA(initColor); // Array contains the target position and the IDs of the selected units
                        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                        SendOptions sendOptions = new SendOptions { Reliability = true };
                        PhotonNetwork.RaiseEvent(1, content, raiseEventOptions, sendOptions);
                        Debug.Log("Sent " + playerRig.color);
                    }

                    if (Input.GetKeyDown(KeyCode.K))
                    {
                        int[] content = new int[] { id, 25, view.ViewID }; // Array contains the target position and the IDs of the selected units
                        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                        SendOptions sendOptions = new SendOptions { Reliability = true };
                        PhotonNetwork.RaiseEvent(3, content, raiseEventOptions, sendOptions);
                        Debug.Log("Damaged players " + 25);
                    }




                    //Updates
                    Move();
                    Grab();


                    //Props
                    foreach (Transform item in lArsenel)
                    {
                        item.position = lTarget.position;
                        item.localPosition = new Vector3(item.localPosition.x, item.localPosition.y, propOffset);
                        item.forward = prop2.forward;
                    }
                    foreach (Transform item in rArsenel)
                    {
                        item.position = rTarget.position;
                        item.localPosition = new Vector3(item.localPosition.x, item.localPosition.y, propOffset);
                        item.forward = prop2.forward;
                    }


                    //Net Send
                    for (int i = 0; i < netTransforms.Length; i++)
                    {
                        netPos[i] = netTransforms[i].position;
                        netRot[i] = netTransforms[i].eulerAngles;
                    }

                    break;

                case 1:
                    if(playerRig.deadState == 3)
                    {
                        foreach (SCR_GLOBAL_RenderPlayerModel item in models)
                        {
                            if (item.dissolve != 1)
                                item.dissolve = 1;
                        }

                    }
                    if (puppetMaster.pinWeight != 0f)
                        puppetMaster.pinWeight = 0f;

                    break;
            }


        }
        else
        {


            switch (netState)
            {
                case 0:
                    //Net Receive
                    for (int i = 0; i < netTransforms.Length; i++)
                    {
                        netTransforms[i].position = Vector3.Lerp(netTransforms[i].position, netPos[i], lerpPosition);
                        netTransforms[i].eulerAngles = new Vector3(Mathf.LerpAngle(netTransforms[i].eulerAngles.x, netRot[i].x, lerpRotation), Mathf.LerpAngle(netTransforms[i].eulerAngles.y, netRot[i].y, lerpRotation), Mathf.LerpAngle(netTransforms[i].eulerAngles.z, netRot[i].z, lerpRotation)); ;
                    }

                    if (cDeadTime <= 0)
                        cDeadTime = deadTime;

                    if (puppetMaster.pinWeight != .5f)
                        puppetMaster.pinWeight = .5f;
                    break;

                case 1:

                    if (cDeadTime > 0)
                    {
                        cDeadTime -= Time.deltaTime;
                        if (puppetMaster.pinWeight != 0f)
                            puppetMaster.pinWeight = 0f;
                    }
                    else
                        netState = 0;
                    break;
            }

        }
    }

    void FixedUpdate()
    {
        if (view.IsMine)
        {
            //Booster
            if (controller.isGrounded)
                fall = 0;
            else
                fall += drop;

            if (click)
            {
                boost = booster;
                rise = climb;
            }

            if (boost > 0)
                boost = boost - grav;

            boostd = boostd - .1f;
        }
    }

    private void Move()
    {
        Vector3 moveDirectionForward = mechRoot.forward * axisL.y * -1;
        Vector3 moveDirectionSide = mechRoot.right * axisL.x;

        //find the direction
        Vector3 direction = (moveDirectionForward + moveDirectionSide);

        direction.y = (direction.y + boost * rise) - (grav * fall);

        //find the distance
        Vector3 distance = direction * speed * Time.deltaTime;

        // Apply Movement to Player
        controller.Move(distance);
    }

    private void Grab()
    {
        lFree = true;
        rFree = true;
        limbMult = 1;

        //if (!grippedR)
        //{
        //    ik.solver.effectors[5].target = lTarget.transform;
        //    ik.solver.effectors[6].target = rTarget.transform;

        //    ik.solver.effectors[5].rotationWeight = .3f;
        //    ik.solver.effectors[6].rotationWeight = .3f;

        //    limbMult = 1;

        //    if (rArsenelModels[RIndex].dissolve == 0f)
        //        rArsenelModels[RIndex].dissolve = .2f;
        //}
        //else
        //{
        //    if (rArsenelSecondGrip[RIndex])
        //    {
        //        if (!grippedL)
        //        {
        //            if (rArsenelModels[RIndex].dissolve != 0f)
        //                rArsenelModels[RIndex].dissolve = 0f;

        //            ik.solver.effectors[5].target = rArsenelSecondGrip[RIndex].transform;

        //            ik.solver.effectors[5].rotationWeight = 0f;
        //            ik.solver.effectors[6].rotationWeight = 0f;

        //            limbMult = .2f;

        //            ik.solver.effectors[6].target = rArsenel[RIndex].transform;
        //        }
        //    }
        //    else
        //    {
        //        if (rArsenelModels[RIndex].dissolve != 0f)
        //            rArsenelModels[RIndex].dissolve = 0f;

        //        ik.solver.effectors[6].target = rArsenel[RIndex].transform;
        //    }
        //}

        if (grippedL)
        {
            if (lIndex > -1 && lFree)
            {
                lFree = false;
                if (lArsenelSecondGrip[lIndex])
                {
                    rFree = false;

                    ik.solver.effectors[6].target = lArsenelSecondGrip[lIndex].transform;

                    ik.solver.effectors[6].rotationWeight = 0f;
                    ik.solver.effectors[5].rotationWeight = 0f;

                    limbMult = .2f;

                    ik.solver.effectors[5].target = lArsenel[lIndex].transform;
                }
                else
                {
                    ik.solver.effectors[5].target = lArsenel[lIndex].transform;
                }
            }
        }
        else
        {
            if(lIndex > 0)
            {
                ik.solver.effectors[5].target = lTarget.transform;
                ik.solver.effectors[5].rotationWeight = 1f;

                view.RPC("SpawnPickup", RpcTarget.MasterClient, lIndex, lHand.position, 0);

                lIndex = -1;
            }
        }

        if (grippedR)
        {
            if (RIndex > -1 && rFree)
            {
                rFree = false;
                if (rArsenelSecondGrip[RIndex])
                {
                    lFree = false;

                    ik.solver.effectors[5].target = rArsenelSecondGrip[RIndex].transform;

                    ik.solver.effectors[5].rotationWeight = 0f;
                    ik.solver.effectors[6].rotationWeight = 0f;

                    limbMult = .2f;

                    ik.solver.effectors[6].target = rArsenel[RIndex].transform;
                }
                else
                {
                    ik.solver.effectors[6].target = rArsenel[RIndex].transform;
                }
            }
        }
        else
        {
            if(RIndex > 0)
            {
                ik.solver.effectors[6].target = rTarget.transform;
                ik.solver.effectors[6].rotationWeight = 1f;

                view.RPC("SpawnPickup", RpcTarget.MasterClient, RIndex, rHand.position, 0);

                RIndex = -1;
            }
        }
    }

    public static Quaternion SmoothDamp(Quaternion rot, Quaternion target, ref Quaternion deriv, float time)
    {
        // account for double-cover
        var Dot = Quaternion.Dot(rot, target);
        var Multi = Dot > 0f ? 1f : -1f;
        target.x *= Multi;
        target.y *= Multi;
        target.z *= Multi;
        target.w *= Multi;
        // smooth damp (nlerp approx)
        var Result = new Vector4(
            Mathf.SmoothDamp(rot.x, target.x, ref deriv.x, time),
            Mathf.SmoothDamp(rot.y, target.y, ref deriv.y, time),
            Mathf.SmoothDamp(rot.z, target.z, ref deriv.z, time),
            Mathf.SmoothDamp(rot.w, target.w, ref deriv.w, time)
        ).normalized;
        // compute deriv
        var dtInv = 1f / Time.deltaTime;
        deriv.x = (Result.x - rot.x) * dtInv;
        deriv.y = (Result.y - rot.y) * dtInv;
        deriv.z = (Result.z - rot.z) * dtInv;
        deriv.w = (Result.w - rot.w) * dtInv;
        return new Quaternion(Result.x, Result.y, Result.z, Result.w);
    }

    private Vector2 AxisL()
    {
        if (XRSettings.loadedDeviceName == "Oculus")

            return new Vector2(Input.GetAxis("WeaponSelectX"), Input.GetAxis("WeaponSelectY"));

        else if (XRSettings.loadedDeviceName == "OpenVR")
            return new Vector2(SteamVR_Input.GetVector2("Stick", SteamVR_Input_Sources.Any).x, SteamVR_Input.GetVector2("Stick", SteamVR_Input_Sources.Any).y * -1);

        else
            return new Vector2(0, 0);
    }

    private bool Click()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
        {

            return (Input.GetButtonDown("WeaponSelect"));//(Input.GetAxis("LGrip") == 1 && Input.GetButton("LTouchButtons"));

        }
        else if (XRSettings.loadedDeviceName == "OpenVR")
            return SteamVR_Input.GetState("Click", SteamVR_Input_Sources.Any, true);

        else
            return false;
        //else if (XRSettings.loadedDeviceName == "OpenVR")
        //{

        //    return ();

        //}
    }
    private bool ClickHold()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
        {

            return (Input.GetButton("WeaponSelect"));//(Input.GetAxis("LGrip") == 1 && Input.GetButton("LTouchButtons"));

        }
        else if (XRSettings.loadedDeviceName == "OpenVR")
            return SteamVR_Input.GetState("Click", SteamVR_Input_Sources.Any, true);

        else
            return false;
        //else if (XRSettings.loadedDeviceName == "OpenVR")
        //{

        //    return ();

        //}
    }

    private bool GripL()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
        {

            return (Input.GetAxis("LGrip") >= .5f);//(Input.GetAxis("LGrip") == 1 && Input.GetButton("LTouchButtons"));

        }
        else if (XRSettings.loadedDeviceName == "OpenVR")
            return (SteamVR_Input.GetFloat("SqueezeGrip", SteamVR_Input_Sources.LeftHand, true) > .5f || SteamVR_Input.GetState("GrabGrip", SteamVR_Input_Sources.LeftHand, true));

        else
            return false;
        //else if (XRSettings.loadedDeviceName == "OpenVR")
        //{

        //    return ();

        //}
    }

    private bool GripR()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
        {

            return (Input.GetAxis("RGrip") >= .5f);//(Input.GetAxis("LGrip") == 1 && Input.GetButton("LTouchButtons"));

        }
        else if (XRSettings.loadedDeviceName == "OpenVR")
            return (SteamVR_Input.GetFloat("SqueezeGrip", SteamVR_Input_Sources.RightHand, true) > .5f || SteamVR_Input.GetState("GrabGrip", SteamVR_Input_Sources.RightHand, true));

        else
            return false;
        //else if (XRSettings.loadedDeviceName == "OpenVR")
        //{

        //    return ();

        //}
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(netPos);
            stream.SendNext(netRot);
        }
        else
        {
            netPos = (Vector3[])stream.ReceiveNext();
            netRot = (Vector3[])stream.ReceiveNext();
        }
    }

    [PunRPC]
    private void SpawnPickup(int pickup, Vector3 point, int ammo)
    {
        GameObject p = PhotonNetwork.Instantiate("PRE_Prefabs/" + pickups[pickup].name, point - new Vector3(0, 1, 0), Quaternion.identity);
        p.GetComponent<SCR_GLOBAL_Pickup>().modifier = ammo;
        p.GetComponent<SCR_GLOBAL_Pickup>().home = false;
        p.GetComponent<SCR_GLOBAL_Pickup>().spawnTime = 4;
    }
}



