﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

[AddComponentMenu("Plugged/Player Info")]
public class SCR_Network_PlayerInfo : MonoBehaviourPunCallbacks
{
    /// <summary>
    /// The local player's color
    /// </summary>
    [SerializeField]
    public Color color;

    /// <summary>
    /// The local player's face texture
    /// </summary>
    [SerializeField]
    public Texture texture;

    /// <summary>
    /// The local player's name
    /// </summary>
    private string nickname;

    /// <summary>
    /// Hashtable for networking variables
    /// </summary>
    private ExitGames.Client.Photon.Hashtable playerInfo = new ExitGames.Client.Photon.Hashtable();

    public PhotonView myView;

    void Start()
    {
        SetColor(color);
        // Setup hashtables

        //myView.RPC("NetworkVanity", RpcTarget.AllBuffered, color);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SCR_Network_GameManager.instance.LeaveRoom();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            SetColor(Random.ColorHSV());
        }
        
    }

    public void SetColor(Color newColor)
    {
        if (playerInfo.ContainsKey("color"))
        {
            playerInfo.Remove("color");
        }
        playerInfo.Add("color", newColor);
        PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);
        Debug.Log(PhotonNetwork.LocalPlayer.CustomProperties["color"]);
    }

    public void SetTexture(Texture newTexture)
    {
        playerInfo.Add("texture", newTexture);
        PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);
    }

    public void SetName(string newName)
    {
        if (photonView.IsMine)
        {
            nickname = newName;          
        }
        PhotonNetwork.NickName = nickname;
    }

    private void UpdateHash(string key, Object value)
    {
        if (playerInfo.ContainsKey(key))
        {
            playerInfo.Remove(key);
        }
        playerInfo.Add(key, value);        
        PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);
    }

    [PunRPC]
    public void NetworkVanity(Color newColor)
    {
        //if (changedProps.ContainsKey("color"))
        //{
        //    Color newColor = (Color)changedProps["color"];
        //    this.GetComponent<MeshRenderer>().material.color = newColor;
        //}
        if (photonView.IsMine)
        {
            this.GetComponent<MeshRenderer>().material.color = newColor;
            Debug.Log("DEBUG METHOD CALLED");
        }

    }
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        foreach (Player newPlayer in PhotonNetwork.PlayerList)
        {
            if(photonView.Owner == targetPlayer)
            { 
                Color newColor = (Color)changedProps["color"];
                GetComponent<MeshRenderer>().material.color = newColor;
            }
        }
        //base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
    }
}
