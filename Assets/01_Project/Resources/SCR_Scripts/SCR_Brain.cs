﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_Brain : MonoBehaviour
{
    float amplitudeX = 10.0f;
    float amplitudeY = 5.0f;
    public float amp = .05f;
    float omegaX = 1.0f;
    float omegaY = 5.0f;
    public float mag = 1;
    float elapsedTime;
    private void Start()
    {
        amp = (transform.localScale.x + transform.localScale.y + transform.localScale.z);
    }
    private void Update()
    {
        elapsedTime += Time.deltaTime;
        float x = amp * Mathf.Cos(mag * elapsedTime);
        float y = amp * Mathf.Sin(mag * elapsedTime);
        transform.localPosition = new Vector3(0, y, 0) / 5;
        transform.localRotation = Quaternion.Euler(x * 15, -x * 10, y * 25);
    }
}
