﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_DEBUG_Bullet : MonoBehaviour
{
    public float time;
    private AudioSource audio;
    private PhotonView view;
    public LayerMask mask;
    private RaycastHit hit;
    public LineRenderer line;
    // Start is called before the first frame update
    void Start()
    {
        audio = gameObject.GetComponent<AudioSource>();
        if (!gameObject.GetComponent<PhotonView>().IsMine)
            audio.Play();

        if (Physics.Linecast(line.GetPosition(0), line.GetPosition(1), out hit, mask))
            line.SetPosition(1,hit.point);
    }

    // Update is called once per frame
    void Update()
    {
            Destroy(gameObject.GetComponent<LineRenderer>());
        if(!audio.isPlaying)
            Destroy(gameObject);

    }
}
