﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public class SCR_GLOBAL_DBAsset : MonoBehaviour
{
    public static bool initFlag = false;
    public static string[][] assets = new string[][]
    {
        new string[] {  "0-GLOBAL_0-LHANDS",
                        "0-GLOBAL_1-RHANDS",
                        "0-GLOBAL_2-BODY",
                        "0-GLOBAL_3-HEAD",
                        "0-GLOBAL_4-FACE" },

        new string[] {  "1-HOME_0-HOME"},

        new string[] {  "2-SWORDS_0-HILT",
                        "2-SWORDS_1-BLADE" },

        new string[] {  "3-VIRUS_0-FACE",
                        "3-VIRUS_1-PISTOL" },

        new string[] { "4-MECHS_0-BASE",
                        "4-MECHS_1-RIBS",
                        "4-MECHS_2-HEAD",
                        "4-MECHS_3-FACE",
                        "4-MECHS_4-UPPERARMLEFT",
                        "4-MECHS_5-UPPERARMRIGHT",
                        "4-MECHS_6-LOWERARMLEFT",
                        "4-MECHS_7-LOWERARMRIGHT",
                        "4-MECHS_8-HANDLEFT",
                        "4-MECHS_9-HANDRIGHT" },

        new string[] {  "5-BIKES_0-BIKE",
                        "5-BIKES_1-STEER",
                        "5-BIKES_2-NODE",
                        "5-BIKES_3-POINTER",
                        "5-BIKES_4-ASSAULTLEFT",
                        "5-BIKES_5-ASSAULTRIGHT",
                        "5-BIKES_6-SPREADLEFT",
                        "5-BIKES_7-SPREADRIGHT",
                        "5-BIKES_8-CHAINLEFT",
                        "5-BIKES_9-CHAINRIGHT",
                        "5-BIKES_10-RAIL",
                        "5-BIKES_11-NEEDLELEFT",
                        "5-BIKES_12-NEEDLERIGHT",
                        "5-BIKES_13-BOLT",
                        "5-BIKES_14-TORPEDOLEFT",
                        "5-BIKES_15-TORPEDORIGHT",
                        "5-BIKES_16-MISSILESLEFT",
                        "5-BIKES_17-MISSILESRIGHT" },
    };
    public static string[][] playerAssets = new string[assets.Length][];
    public static string[][][] items = new string[assets.Length][][];

    public static void Init()
    {
        ExitGames.Client.Photon.Hashtable playerInfo = new ExitGames.Client.Photon.Hashtable();

        for (int type = 0; type < assets.Length; type++)
        {
            items[type] = new string[assets[type].Length][];
            playerAssets[type] = new string[assets[type].Length];

            for (int obj = 0; obj < assets[type].Length; obj++)
            {
                Object[] o = Resources.LoadAll(assets[type][obj].Replace('_', '/'));
                items[type][obj] = new string[o.Length];

                for (int i = 0; i < items[type][obj].Length; i++)
                {
                    items[type][obj][i] = i.ToString();
                    //Debug.Log(items[type][obj][i]);
                }

                //Debug.Log(assets[type][obj].Replace('_', '/') + "/" + assets[type][obj] + "_" + items[type][obj][PlayerPrefs.GetInt(assets[type][obj], 0)]);
                playerAssets[type][obj] = assets[type][obj].Replace('_', '/') + "/" + assets[type][obj] + "_" + items[type][obj][PlayerPrefs.GetInt(assets[type][obj], 0)];

                if (playerInfo.ContainsKey(assets[type][obj]))
                {
                    playerInfo.Remove(assets[type][obj]);
                }
                playerInfo.Add(assets[type][obj], PlayerPrefs.GetInt(assets[type][obj], 0));

                PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

                Debug.Log("Set property: " + assets[type][obj] + " " + PlayerPrefs.GetInt(assets[type][obj], 0));

            }
        }

        initFlag = true;
    }

    public static string GetAssetString(int type, int obj, int item)
    {
        if (!initFlag)
            Init();

        return assets[type][obj] + "_" + items[type][obj][item];
    }

    public static string GetPlayerAssetString(int type, int obj)
    {
        if (!initFlag)
            Init();

        int item = PlayerPrefs.GetInt(assets[type][obj]);
        return assets[type][obj];
    }

    public static int GetAssetPref(int type, int obj)
    {
        if (!initFlag)
            Init();

        return PlayerPrefs.GetInt(assets[type][obj]);
    }

    public static GameObject GetAsset(int type, int obj, int item)
    {
        if (!initFlag)
            Init();

        Debug.Log("Instantiated player model: " + playerAssets[type][obj]);
        return Instantiate(Resources.Load(assets[type][obj].Replace('_', '/') + "/" + assets[type][obj] + "_" + items[type][obj][item]) as GameObject);
    }

    public static GameObject GetPlayerAsset(int type, int obj)
    {
        if (!initFlag)
            Init();

        Debug.Log("Instantiated player model: " + playerAssets[type][obj]);
        return Instantiate(Resources.Load(playerAssets[type][obj]) as GameObject);
    }

    public static GameObject GetPhotonAsset(int type, int obj)
    {
        if (!initFlag)
            Init();

        Debug.Log("Instantiated player model: " + playerAssets[type][obj]);
        return PhotonNetwork.Instantiate(playerAssets[type][obj], Vector3.zero, Quaternion.identity);
    }
}
