﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_GLOBAL_PickupSpawner : MonoBehaviour
{
    private PhotonView view;
    public string pickup;
    // Start is called before the first frame update
    void Start()
    {
        view = SCR_Network_GameManager.instance.playerRig.photonView;
        if (view.IsMine && view.Owner.IsMasterClient)
        {
            PhotonNetwork.Instantiate("PRE_Prefabs/"+pickup, transform.position, transform.rotation);
        }
        Destroy(gameObject);
    }
}
