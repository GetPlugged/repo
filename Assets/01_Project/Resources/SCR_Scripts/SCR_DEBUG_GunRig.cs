﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SCR_DEBUG_GunRig : MonoBehaviour
{

    public GameObject gun;
    public SCR_GLOBAL_GameRigInit rigInit;
    public GameObject grip;
    public PhotonView view;
    // Start is called before the first frame update
    void Start()
    {
        if (view.IsMine)
        {
            //grip = SCR_Network_GameManager.instance.localPlayerInstance.GetComponent<SCR_GLOBAL_Rig>().leftHand.GetComponent<SCR_GLOBAL_AnimationInput>().model.gameObject;
            //grip = rigInit.player.GetComponent<SCR_GLOBAL_Rig>().leftHand.GetComponent<SCR_GLOBAL_AnimationInput>().model;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(grip == null && view.IsMine)
        {
            // Temporary Fix, wont keep in final, wont spawn into games until primary rig is initalized
            //grip = SCR_Network_GameManager.instance.localPlayerInstance.GetComponent<SCR_GLOBAL_Rig>().leftHand.GetComponent<SCR_GLOBAL_AnimationInput>().model.gameObject;
        }
    }

    private void FixedUpdate()
    {
        if (view.IsMine)
        {
            gun.transform.position = grip.transform.position;
            gun.transform.rotation = grip.transform.rotation;
        }

    }
}
