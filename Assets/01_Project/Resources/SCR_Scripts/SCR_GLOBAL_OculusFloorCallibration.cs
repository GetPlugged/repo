﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Valve.VR;

public class SCR_GLOBAL_OculusFloorCallibration : MonoBehaviour
{
    public float calRef;

    public float smooth;
    public float test;

    public GameObject lH;
    public GameObject rH;

    public GameObject head;
    public GameObject body;

    public GameObject reff;

    public bool cross;
    // Start is called before the first frame update
    void Start()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
        {
            transform.position = new Vector3(transform.position.x,calRef, transform.position.y);
        }
    }

    private void FixedUpdate()
    {
        Vector3 dot = Vector3.Cross(lH.transform.position, rH.transform.position).normalized;
        Vector3 pos = (head.transform.InverseTransformDirection(lH.transform.position) - head.transform.InverseTransformDirection(rH.transform.position));
        if (Vector3.Dot(transform.right, pos) > 0)
            cross = true;
        else
            cross = false;




        //cross = head.transform.InverseTransformPoint(lH.transform.position).x > head.transform.InverseTransformPoint(rH.transform.position).x;
        //if (head.transform.InverseTransformPoint(lH.transform.position).x > head.transform.InverseTransformPoint(rH.transform.position).x)
        //{
        //    cross = true;
        //}
        //else
        //{
        //    cross = false;
        //}

        reff.transform.position = (lH.transform.position + rH.transform.position) / 2.0f;
        if(!cross)
            reff.transform.rotation = Quaternion.LookRotation(lH.transform.position - rH.transform.position, Vector3.forward);
        reff.transform.eulerAngles = new Vector3(0, reff.transform.eulerAngles.y, 0);

        body.transform.rotation = Quaternion.Lerp(body.transform.rotation, Quaternion.Lerp(head.transform.rotation, reff.transform.rotation, .5f), smooth);
        body.transform.eulerAngles = new Vector3(0, body.transform.eulerAngles.y,0);
    }

    

}
