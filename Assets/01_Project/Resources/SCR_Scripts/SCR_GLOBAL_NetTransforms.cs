﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_GLOBAL_NetTransforms : MonoBehaviourPunCallbacks, IPunObservable
{
    public Transform[] netTransforms;
    public float lerpPosition;
    public float lerpRotation;

    private Vector3[] netPos;
    private Vector3[] netRot;
    private PhotonView view;

    void Start()
    {
        netPos = new Vector3[netTransforms.Length];
        netRot = new Vector3[netTransforms.Length];
    }

    void Update()
    {
        //Net send
        if(view.IsMine)
        for (int i = 0; i < netTransforms.Length; i++)
        {
            netPos[i] = netTransforms[i].position;
            netRot[i] = netTransforms[i].eulerAngles;
        }
            else
            {
            //Net receive
            for (int i = 0; i < netTransforms.Length; i++)
            {
                netTransforms[i].position = Vector3.Lerp(netTransforms[i].position, netPos[i], lerpPosition);
                netTransforms[i].eulerAngles = new Vector3(Mathf.LerpAngle(netTransforms[i].eulerAngles.x, netRot[i].x, lerpRotation), Mathf.LerpAngle(netTransforms[i].eulerAngles.y, netRot[i].y, lerpRotation), Mathf.LerpAngle(netTransforms[i].eulerAngles.z, netRot[i].z, lerpRotation)); ;
            }
        }

    }


    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(netPos);
            stream.SendNext(netRot);
        }
        else
        {
            netPos = (Vector3[])stream.ReceiveNext();
            netRot = (Vector3[])stream.ReceiveNext();
        }
    }
}
