﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_DEBUG_WorldGeometry : MonoBehaviour
{
    public Renderer[] renderers;
    public Material[] materials;
    public float dissolveSpeed;

    // Start is called before the first frame update


    void Awake()
    {
        materials = new Material[renderers.Length];

        for (int i = 0; i < renderers.Length; i++)
        {
            materials[i] = renderers[i].material;
            materials[i].SetFloat("_DissolveAmount", 1f);
        }
        //        clouds.SetColor("_EmissionColor", Color.black);
        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < renderers.Length; i++)
        {
            if (materials[i].GetFloat("_DissolveAmount") > 0)
                materials[i].SetFloat("_DissolveAmount", materials[i].GetFloat("_DissolveAmount") - dissolveSpeed * Time.deltaTime);
        }

    }

    private void OnDisable()
    {
        for (int i = 0; i < renderers.Length; i++)
        {
            materials[i] = renderers[i].material;
            materials[i].SetFloat("_DissolveAmount", .5f);
        }

    }
}
