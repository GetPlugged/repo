﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Valve.VR;
using TMPro;

public class SCR_VIRUS_Rig : MonoBehaviourPunCallbacks, IPunObservable, IOnEventCallback
{
    public int id;
    //
    public Transform snap;
    public Transform tracker;
    public float speed;
    public float gSpeed;
    public float zSpeed;
    public float offset;
    public float smoothRun;
    public float ySmooth;
    public float threshold;
    public float clipThresh;
    public float hurtTime;
    private float gTime;
    public float zTime;

    public Transform colRef;
    public Transform ray;
    public Transform colRay;
    public LayerMask mask;

    public SCR_GLOBAL_RenderPlayerModel[] models;
    public SCR_GLOBAL_RenderPlayerModel[] zFaces;
    public SCR_GLOBAL_RenderPlayerModel[] lArsenalModels;
    public SCR_GLOBAL_RenderPlayerModel[] rArsenalModels;
    public int[] ammo;
    public int lSlot;
    public int rSlot;
    public GameObject[] rBullets;
    public string[] pickups;
    private bool grippedLeft;
    private bool grippedRight;
    private float triggerLeft;
    private float triggerRight;
    public Transform lCollider;
    public Transform rCollider;
    public Transform[] gameCollider;

    private SCR_GLOBAL_Gun[] lArsenal;
    private SCR_GLOBAL_Gun[] rArsenal;
    public int ammoL;
    public int ammoR;

    private Transform[] lArsenalTransforms;
    private Transform[] rArsenalTransforms;
    private Vector3 charCollider;
    public Transform collider;

    private SCR_Network_GameManager gameManager;
    private SCR_GLOBAL_Rig playerRig;
    private SCR_GLOBAL_WorldManager worldManager;

    private PhotonView view;
    public AudioSource hurtClip;
    private RaycastHit hit;

    private Transform player;
    private Transform body;
    private Transform lHand;
    private Transform rHand;
    private Transform lHandModel;
    private Transform rHandModel;
    private Transform face;
    private float cHurtTime;

    private CharacterController controller;

    private float cSpeed;
    private float lastP;
    private float lastPL;
    private float lastPR;
    private float cZspeed;
    private float thresh;
    private float cGTime;
    private float cZTime;

    private int health;
    private int score;

    private bool running;
    private bool grabbed;
    private bool zDead;

    private Color initColor;
    private Color hurtColor;

    private ExitGames.Client.Photon.Hashtable playerInfo = new ExitGames.Client.Photon.Hashtable();

    // Start is called before the first frame update
    void Start()
    {
        view = GetComponent<PhotonView>();
        worldManager = SCR_Network_GameManager.instance.worldManager;


        if (view.IsMine)
        {
            gameManager = SCR_Network_GameManager.instance;
            playerRig = gameManager.playerRig;

            player = playerRig.gameObject.transform;
            body = playerRig.body;
            face = playerRig.renderModels[0].model.transform;
            lHand = playerRig.renderModels[3].model.transform;
            rHand = playerRig.renderModels[4].model.transform;
            controller = snap.gameObject.GetComponent<CharacterController>();
            foreach (SCR_GLOBAL_RenderPlayerModel item in zFaces)
            {
                item.model.layer = 17;
            }
            health = 100;
            score = 0;
            initColor = InitColor();
            hurtColor.r = .5f - initColor.r;
            hurtColor.g = .5f - initColor.g;
            hurtColor.b = .5f - initColor.b;

            //Init properties
            if (playerInfo.ContainsKey("playerHealth"))
            {
                playerInfo.Remove("playerHealth");
            }
            playerInfo.Add("playerHealth", health);

            PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

            if (playerInfo.ContainsKey("playerScore"))
            {
                playerInfo.Remove("playerScore");
            }
            playerInfo.Add("playerScore", 0);

            PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);
        }
        else
        {
            int rigID = System.Convert.ToInt32(view.Owner.CustomProperties["playerRig"].ToString());
            lHand = PhotonNetwork.GetPhotonView(rigID).gameObject.GetComponent<SCR_GLOBAL_Rig>().renderModels[3].gameObject.transform;
            rHand = PhotonNetwork.GetPhotonView(rigID).gameObject.GetComponent<SCR_GLOBAL_Rig>().renderModels[4].gameObject.transform;
            body = PhotonNetwork.GetPhotonView(rigID).gameObject.GetComponent<SCR_GLOBAL_Rig>().body;
            face = PhotonNetwork.GetPhotonView(rigID).gameObject.GetComponent<SCR_GLOBAL_Rig>().renderModels[0].gameObject.transform;

            lHand = PhotonNetwork.GetPhotonView(lHand.GetComponent<SCR_GLOBAL_RenderPlayerModel>().modelViewID).gameObject.transform;
            rHand = PhotonNetwork.GetPhotonView(rHand.GetComponent<SCR_GLOBAL_RenderPlayerModel>().modelViewID).gameObject.transform;
        }


        lArsenal = new SCR_GLOBAL_Gun[lArsenalModels.Length];
        lArsenalTransforms = new Transform[lArsenalModels.Length];

        for (int i = 0; i < lArsenalModels.Length; i++)
        {
            lArsenal[i] = lArsenalModels[i].model.gameObject.GetComponent<SCR_GLOBAL_Gun>();
            lArsenal[i].view = view;
            lArsenalTransforms[i] = lArsenalModels[i].gameObject.transform.parent;
        }

        rArsenal = new SCR_GLOBAL_Gun[rArsenalModels.Length];
        rArsenalTransforms = new Transform[rArsenalModels.Length];

        for (int i = 0; i < rArsenalModels.Length; i++)
        {
            rArsenal[i] = rArsenalModels[i].model.gameObject.GetComponent<SCR_GLOBAL_Gun>();
            rArsenal[i].view = view;
            rArsenal[i].localFabs[0] = rBullets[i];
            rArsenalTransforms[i] = rArsenalModels[i].gameObject.transform.parent;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (view.IsMine)
        {
        }

        int worldState = worldManager.state;
        switch (worldState)
        {
            case 1:
                //dissolve in
                break;

            case 4:
                StateFour();
                break;

            case 6:
                //dissolve out
                break;
        }
    }

    void LateUpdate()
    {
        if (view.IsMine)
        {
            lCollider.position = lHand.position;
            rCollider.position = rHand.position;

            if (lSlot >= 0)
            {
                lArsenalTransforms[lSlot].position = lHand.position;
                lArsenalTransforms[lSlot].rotation = lHand.rotation;
            }
            if (rSlot >= 0)
            {
                rArsenalTransforms[rSlot].position = rHand.position;
                rArsenalTransforms[rSlot].rotation = rHand.rotation;
            }
        }
        else
        {
            foreach (Transform item in lArsenalTransforms)
            {
                item.position = lHand.position;
                item.rotation = lHand.rotation;
            }
            foreach (Transform item in rArsenalTransforms)
            {
                item.position = rHand.position;
                item.rotation = rHand.rotation;
            }
        }

        foreach (SCR_GLOBAL_RenderPlayerModel item in zFaces)
        {
            item.transform.position = face.position;
            item.transform.rotation = face.rotation;
        }
        gameCollider[0].transform.position = body.transform.position;
    }

    void StateFour()
    {
        if (view.IsMine)
        {
            Move();

            int rigState = playerRig.state;
            switch (rigState)
            {
                case 0:
                    GetInput();
                    Human();

                    if (Input.GetKeyDown(KeyCode.K))
                    {
                        int[] content = new int[] { id, 25, view.ViewID }; // Array contains the target position and the IDs of the selected units
                        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                        SendOptions sendOptions = new SendOptions { Reliability = true };
                        PhotonNetwork.RaiseEvent(3, content, raiseEventOptions, sendOptions);
                        Debug.Log("Damaged players " + 25);
                    }
                    break;

                case 3:
                    Zombie();

                    if (Input.GetKeyDown(KeyCode.K))
                    {
                        int[] content = new int[] { id, 25, view.ViewID }; // Array contains the target position and the IDs of the selected units
                        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                        SendOptions sendOptions = new SendOptions { Reliability = true };
                        PhotonNetwork.RaiseEvent(3, content, raiseEventOptions, sendOptions);
                        Debug.Log("Damaged players " + 25);

                        //object[] content = new object[] { view.ViewID }; // Array contains the target position and the IDs of the selected units
                        //RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                        //SendOptions sendOptions = new SendOptions { Reliability = true };
                        //PhotonNetwork.RaiseEvent(4, content, raiseEventOptions, sendOptions);
                    }
                    break;
            }
        }
        else
        {

        }
    }

    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;

        if (view.IsMine)
        {


            switch (eventCode)
            {
                case 3:

                    int[] d = (int[])photonEvent.CustomData;

                    if (d[0] == photonView.ViewID && playerRig.state != 1 && cHurtTime <= 0)
                    {
                        d[1] = health - d[1];
                        health = d[1];
                        if (d[1] <= 0)
                        {
                            if (playerRig.state ==0)
                            {
                                playerRig.state = 3;
                                int f = Random.Range(0, zFaces.Length - 1);
                                zFaces[f].dissolve = 0;

                                health = 100;
                                view.RPC("Change", RpcTarget.AllBuffered, true);
                                object[] content = new object[] { d[2] }; // Array contains the target position and the IDs of the selected units
                                RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                                SendOptions sendOptions = new SendOptions { Reliability = true };
                                PhotonNetwork.RaiseEvent(4, content, raiseEventOptions, sendOptions);
                                Debug.Log("Killed by  " + d[2]);
                            }
                            else if(playerRig.state == 3)
                            {
                                zDead = true;
                                cZTime = zTime;
                                health = 100;
                                //object[] content = new object[] { d[2] }; // Array contains the target position and the IDs of the selected units
                                //RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                                //SendOptions sendOptions = new SendOptions { Reliability = true };
                                //PhotonNetwork.RaiseEvent(4, content, raiseEventOptions, sendOptions);
                                Debug.Log("Killed by  " + d[2]);
                            }

                        }
                        else
                        {
                            cHurtTime = hurtTime;
                            hurtClip.Play();
                        }

                        if (playerInfo.ContainsKey("playerHealth"))
                        {
                            playerInfo.Remove("playerHealth");
                        }
                        playerInfo.Add("playerHealth", health);

                        PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

                        Debug.Log("Damaged " + d);
                    }
                    break;

                case 4:

                    object[] v = (object[])photonEvent.CustomData;

                    if (PhotonNetwork.LocalPlayer == PhotonNetwork.GetPhotonView((int)v[0]).Owner)
                    {
                        score++;

                        if (score > 4)
                        {
                            playerRig.state = 0;
                            score = 0;
                            initColor = InitColor();
                            hurtColor.r = .5f - initColor.r;
                            hurtColor.g = .5f - initColor.g;
                            hurtColor.b = .5f - initColor.b;
                            foreach (SCR_GLOBAL_RenderPlayerModel item in zFaces)
                            {
                                item.dissolve = 1;
                            }
                            view.RPC("Change", RpcTarget.AllBuffered, false);
                        }

                        if (playerInfo.ContainsKey("playerScore"))
                        {
                            playerInfo.Remove("playerScore");
                        }
                        playerInfo.Add("playerScore", score);
                        PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);
                    }

                    break;

                case 5:

                    int[] p = (int[])photonEvent.CustomData;
                    Debug.Log("Received");

                    if (view.ViewID == p[2])
                    {
                        Debug.Log("Match");

                        if (p[0] == 0)
                        {

                            if (p[3] == 0)
                            {
                                lSlot = p[1];
                                if (p[4] < 0)
                                    ammoL = ammo[p[1]];
                                else
                                    ammoL = p[4];
                            }
                            else if (p[3] == 1)
                            {
                                rSlot = p[1];
                                if (p[4] < 0)
                                    ammoR = ammo[p[1]];
                                else
                                    ammoR = p[4];
                            }
                        }
                    }

                    break;

            }


        }

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }

    void Move()
    {
        float shake;
        shake = (Mathf.Abs((Mathf.Abs(lHand.position.y - rHand.position.y) - lastP)) / Time.deltaTime);
        thresh = ((Mathf.Abs(lHand.position.y - lastPL) / Time.deltaTime) + (Mathf.Abs(rHand.position.y - lastPR) / Time.deltaTime)) / 2;

        cSpeed = speed * shake * cZspeed;

        snap.eulerAngles = new Vector3(snap.eulerAngles.x, body.eulerAngles.y + 45, snap.eulerAngles.z);

        Vector3 m = tracker.position - collider.position;

        if(m.magnitude > .005)
            collider.GetComponent<CharacterController>().Move(m * 10 * Time.deltaTime);

        colRef.position = collider.position;
        controller.center = colRef.localPosition;
        //controller.center = new Vector3(Mathf.Clamp(colRef.localPosition.x, -1, 1), colRef.localPosition.y, Mathf.Clamp(colRef.localPosition.z, -1, 1));
        running = (thresh > threshold);

        if (grabbed)
            cSpeed = cSpeed * gSpeed;
        if (running)
            controller.Move(snap.forward * cSpeed * Time.deltaTime);

        float h = snap.position.y;
        ray.position = new Vector3(body.position.x, collider.position.y, body.position.z);
        if (Physics.Raycast(ray.position, ray.up * -1, out hit, 6, mask))// && Vector3.Distance(ray.position, collider.position) < clipThresh)
            h = hit.point.y;

        snap.position = new Vector3(snap.transform.position.x, h, snap.transform.position.z) + (offset * Vector3.up);

        Debug.Log(Vector3.Distance(ray.position, collider.position));

        tracker.parent.position = snap.position;

        Vector3 l = Vector3.Lerp(player.position, tracker.transform.position, smoothRun * Time.deltaTime);
        player.position = new Vector3(l.x, Mathf.Lerp(player.position.y, snap.position.y, ySmooth), l.z);

        // if (Vector3.Distance(ray.position, collider.position) < clipThresh)

        lastP = Mathf.Abs(lHand.position.y - rHand.position.y);
        lastPL = lHand.position.y;
        lastPR = rHand.position.y;
    }

    void Human()
    {
        if (cHurtTime > 0f)
        {
            cHurtTime -= Time.deltaTime;
            playerRig.color = hurtColor;
            grabbed = true;
        }
        else
        {
            playerRig.color = initColor;
            grabbed = false;
        }

        if (cZspeed != 1)
            cZspeed = 1;

        if (ammoL < 0)
            ammoL = 0;
        if (ammoR < 0)
            ammoR = 0;

        for (int i = 0; i < lArsenalModels.Length; i++)
        {
            if (i != lSlot)
            {
                if (lArsenalModels[i].dissolve != 1)
                    lArsenalModels[i].dissolve = 1;
                lArsenalModels[i].model.GetComponentInChildren<TextMeshPro>().text = "";
            }
            else
            {
                if (grippedLeft)
                {
                    if(lSlot > -1)
                    {
                        if (lArsenalModels[i].dissolve != 0)
                            lArsenalModels[i].dissolve = 0;
                        lArsenalModels[i].model.GetComponentInChildren<TextMeshPro>().text = ammoL.ToString() + "/" + ammo[i].ToString();
                    }
                }
                else
                {
                    if (lSlot > -1)
                    {
                        if (lArsenalModels[i].dissolve != 1)
                            lArsenalModels[i].dissolve = 1;
                        lArsenalModels[i].model.GetComponentInChildren<TextMeshPro>().text = "";

                        view.RPC("SpawnPickup", RpcTarget.MasterClient, i, collider.transform.position, ammoL);

                        lSlot = -1;
                    }
                }
            }
        }

        if (triggerLeft > .9f && lSlot > -1)
        {
            if (lArsenalModels[lSlot].dissolve == 0 && ammoL > 0)
                lArsenal[lSlot].Fire();
        }

        for (int i = 0; i < rArsenalModels.Length; i++)
        {
            if (i != rSlot)
            {
                if (rArsenalModels[i].dissolve != 1)
                    rArsenalModels[i].dissolve = 1;
                rArsenalModels[i].model.GetComponentInChildren<TextMeshPro>().text = "";
            }
            else
            {
                if (grippedRight)
                {
                    if (rSlot > -1)
                    {
                        if (rArsenalModels[i].dissolve != 0)
                            rArsenalModels[i].dissolve = 0;
                        rArsenalModels[i].model.GetComponentInChildren<TextMeshPro>().text = ammoR.ToString() + "/" + ammo[i].ToString();
                    }
                }
                else
                {
                    if (rSlot > -1)
                    {
                        if (rArsenalModels[i].dissolve != 1)
                            rArsenalModels[i].dissolve = 1;
                        rArsenalModels[i].model.GetComponentInChildren<TextMeshPro>().text = "";

                        view.RPC("SpawnPickup", RpcTarget.MasterClient, i, collider.transform.position, ammoR);

                        rSlot = -1;
                    }
                }
            }
        }

        if (triggerRight > .9f && rSlot > -1)
        {
            if (rArsenalModels[rSlot].dissolve == 0 && ammoR > 0)
                rArsenal[rSlot].Fire();
        }
    }

    void Zombie()
    {
        if (cZspeed != zSpeed)
            cZspeed = zSpeed;
        if (zDead)
        {
            cZspeed = 0;
            playerRig.zColor = Color.black;
            foreach (SCR_GLOBAL_RenderPlayerModel item in zFaces)
            {
                item.color = Color.black;
            }
        }
        else
        {
            playerRig.zColor = Color.green;
            foreach (SCR_GLOBAL_RenderPlayerModel item in zFaces)
            {
                item.color = Color.green;

            }
        }

        if (cZTime < 0)
            zDead = false;

        cZTime-=Time.deltaTime;

        if (cHurtTime > 0f)
        {
            cHurtTime -= Time.deltaTime;
            grabbed = true;
        }
        else
        {
            grabbed = false;
        }
    }

    public void Collide()
    {
        
    }

    void GetInput()
    {
        grippedLeft = GrippedLeft();
        grippedRight = GrippedRight();
        triggerLeft = TriggerLeft();
        triggerRight = TriggerRight();
    }

    private bool GrippedLeft()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
        {

            return (Input.GetAxis("LGrip") >= .5f);//(Input.GetAxis("LGrip") == 1 && Input.GetButton("LTouchButtons"));

        }
        else if (XRSettings.loadedDeviceName == "OpenVR")
            return (SteamVR_Input.GetFloat("SqueezeGrip", SteamVR_Input_Sources.LeftHand, true) > .5f || SteamVR_Input.GetState("GrabGrip", SteamVR_Input_Sources.LeftHand, true));

        else
            return false;
        //else if (XRSettings.loadedDeviceName == "OpenVR")
        //{

        //    return ();

        //}
    }

    private bool GrippedRight()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
        {

            return (Input.GetAxis("RGrip") >= .5f);//(Input.GetAxis("LGrip") == 1 && Input.GetButton("LTouchButtons"));

        }
        else if (XRSettings.loadedDeviceName == "OpenVR")
            return (SteamVR_Input.GetFloat("SqueezeGrip", SteamVR_Input_Sources.RightHand, true) > .5f || SteamVR_Input.GetState("GrabGrip", SteamVR_Input_Sources.RightHand, true));

        else
            return false;
        //else if (XRSettings.loadedDeviceName == "OpenVR")
        //{

        //    return ();

        //}
    }

    private float TriggerLeft()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
            return Input.GetAxis("LTrigger");

        else if (XRSettings.loadedDeviceName == "OpenVR")
            return SteamVR_Input.GetFloat("Squeeze", SteamVR_Input_Sources.LeftHand);

        else
            return 0f;
    }

    private float TriggerRight()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
            return Input.GetAxis("RTrigger");

        else if (XRSettings.loadedDeviceName == "OpenVR")
            return SteamVR_Input.GetFloat("Squeeze", SteamVR_Input_Sources.RightHand);

        else
            return 0f;
    }

    private Color InitColor()
    {
        int i = Random.Range(0, 8);
        Color c = Color.blue;
        switch (i)
        {
            case 1:
                c = Color.blue;
                break;

            case 2:
                c = Color.cyan;
                break;

            case 3:
                c = Color.red;
                break;

            case 4:
                c = Color.magenta;
                break;

            case 5:
                c = Color.yellow;
                break;

            case 6:
                c = Color.blue;
                c.r = 1 - c.r;
                c.g = 1 - c.g;
                c.b = 1 - c.b;
                break;

            case 7:
                c = Color.cyan;
                c.r = 1 - c.r;
                c.g = 1 - c.g;
                c.b = 1 - c.b;
                break;

            case 8:
                c = Color.yellow;
                c.r = 1 - c.r;
                c.g = 1 - c.g;
                c.b = 1 - c.b;
                break;
        }
        return c;
    }

    [PunRPC]
    private void SpawnPickup(int pickup, Vector3 point, int ammo)
    {
        GameObject p = PhotonNetwork.Instantiate("PRE_Prefabs/"+pickups[pickup], point - new Vector3(0,1,0), Quaternion.identity);
        p.GetComponent<SCR_GLOBAL_Pickup>().modifier = ammo;
        p.GetComponent<SCR_GLOBAL_Pickup>().home = false;
        p.GetComponent<SCR_GLOBAL_Pickup>().spawnTime = 4;
    }

    [PunRPC]
    private void Change(bool z)
    {
        if (z)
        {
            gameCollider[2].gameObject.layer = 28;
            gameCollider[1].gameObject.SetActive(true);
        }
        else
        {
            gameCollider[2].gameObject.layer = 29;
            gameCollider[1].gameObject.SetActive(false);
        }
    }
}
