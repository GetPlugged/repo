﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Valve.VR;
using TMPro;
public class SCR_BIKES_Rig : MonoBehaviourPunCallbacks, IPunObservable, IOnEventCallback
{
    public AudioSource tempMusic;
    public int id;

    [Space(10)]

    public float speed;
    public float steerAmount;
    public float tiltAmount;
    public float positionDamping;
    public float steeringDamping;
    public float tiltDamping;

    [Space(10)]

    public float modelDamping;

    [Space(10)]

    public float steeringDeadzone;
    public float steeringSensitivity;
    public Transform steerModelLeft;
    public Transform steerModelRight;

    [Space(10)]

    public float floorOffset;
    public LayerMask mask;

    [Space(10)]

    public SCR_GLOBAL_RenderPlayerModel[] models;

    [Space(10)]

    public int weaponCount;
    public bool[] equiped;
    public int[] ammo;
    public int[] capacity;
    public int[] replenishment;
    public float accuracy;
    public TextMeshPro indicator;
    public SCR_GLOBAL_RenderPlayerModel[] leftGunModel;
    public SCR_GLOBAL_RenderPlayerModel[] rightGunModel;
    public SCR_GLOBAL_RenderPlayerModel[] nodesLeft;
    public Transform nodePointerLeft;
    public SCR_GLOBAL_RenderPlayerModel[] nodesRight;
    public Transform nodePointerRight;
    public AudioSource gunAudio;
    public AudioClip[] gunClips;
    public Transform weaponRay;

    public LayerMask netMask;
    public SCR_GLOBAL_NetId[] netColiders;
    public Collider rigCollider;
    public float deadTime;
    public float hurtTime;
    public Vector2 xExplodeForce;
    public Vector2 yExplodeForce;
    public Vector2 zExplodeForce;

    [Space(10)]

    public new AudioSource audio;
    public AudioSource spawnClip;
    public AudioSource hurtClip;

    [Space(10)]

    public Transform rig;
    public Transform ray;
    public Transform steerPivot;
    public Transform rollPivot;
    public Transform model;
    public Transform playerSnap;
    public Transform weaponWheel;
    public Transform bikeModel;

    public Transform steerLeft;
    public Transform steerLeftRef;
    public Transform steerLeftRot;
    public Transform steerLeftRot2;
    public Transform leftNode1;
    public Transform leftNode2;

    public Transform steerRight;
    public Transform steerRightRef;
    public Transform steerRightRot;
    public Transform steerRightRot2;
    public Transform rightNode1;
    public Transform rightNode2;

    [Space(10)]

    public Transform[] netTransforms;
    public float lerpPosition;
    public float lerpRotation;

    //Private vars
    private PhotonView view;
    private int state;

    private SCR_Network_GameManager gameManager;
    private SCR_GLOBAL_Rig playerRig;
    private SCR_GLOBAL_WorldManager worldManager;

    private Transform player;
    private SCR_GLOBAL_AnimationInput pLHand;
    private SCR_GLOBAL_AnimationInput pRHand;
    private Transform pLHandTransform;
    private Transform pRHandTransform;
    private Transform pLHandModel;
    private Transform pRHandModel;

    private RaycastHit hit;

    private float curSpeed;

    private float smoothSpeed;
    private float smoothRot;
    private float smoothTilt;

    private float smoothSpeedV;
    private float smoothRotV;
    private float smoothTiltV;

    private Quaternion smoothModelV;

    private Vector2 axis;
    private float trigger;
    private bool click;
    private bool touchLeft;
    private bool touchRight;

    private int slot;
    private SCR_GLOBAL_Gun[] leftGun;
    private SCR_GLOBAL_Gun[] rightGun;

    private float xRef;
    private float wheelSteer;
    private bool hasGripped;
    private bool wheelGrippedLeft;
    private bool wheelGrippedRight;

    private float sIndex;
    private bool wInit;
    private bool bIndex = false;
    private bool spawnIndex;

    private Vector3[] netPos;
    private Vector3[] netRot;
    private Vector3[] netPosV;
    private Vector3[] netRotV;

    private bool[] tracers;

    private int health;
    private Color hurtColor;
    private Color initColor;
    private int dead;
    private Transform[] deadModels;
    private Vector3[] deadPos;
    private Vector3[] deadRot;
    private Vector3[] explodeForce;
    private float cHurtTime;
    private Player[] playerList;
    private int[] playerScores;
    private int score;
    private RaycastHit gunHit;
    public int netState;
    private float cDeadTime;
    private float deadStateTime;
    private float CdeadStateTime;
    public int deadState;

    private ExitGames.Client.Photon.Hashtable playerInfo = new ExitGames.Client.Photon.Hashtable();

    //Input
    #region Input
    private Vector2 Axis()
    {
        if (XRSettings.loadedDeviceName == "Oculus")

            return new Vector2(Input.GetAxis("WeaponSelectX"), Input.GetAxis("WeaponSelectY"));

        else if (XRSettings.loadedDeviceName == "OpenVR")
            return new Vector2(SteamVR_Input.GetVector2("Stick", SteamVR_Input_Sources.Any).x, SteamVR_Input.GetVector2("Stick", SteamVR_Input_Sources.Any).y * -1);

        else
            return new Vector2(0, 0);
    }

    private float Trigger()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
            return Input.GetAxis("Fire");

        else if (XRSettings.loadedDeviceName == "OpenVR")
            return SteamVR_Input.GetFloat("Squeeze", SteamVR_Input_Sources.Any);

        else
            return 0f;
    }

    private bool WheelGrippedLeft()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
        {

            return (Input.GetAxis("LGrip") >= .5f);//(Input.GetAxis("LGrip") == 1 && Input.GetButton("LTouchButtons"));

        }
        else if (XRSettings.loadedDeviceName == "OpenVR")
            return (SteamVR_Input.GetFloat("SqueezeGrip", SteamVR_Input_Sources.LeftHand, true) > .5f || SteamVR_Input.GetState("GrabGrip", SteamVR_Input_Sources.LeftHand, true));

        else
            return false;
        //else if (XRSettings.loadedDeviceName == "OpenVR")
        //{

        //    return ();

        //}
    }

    private bool WheelGrippedRight()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
        {

            return (Input.GetAxis("RGrip") >= .5f);//(Input.GetAxis("LGrip") == 1 && Input.GetButton("LTouchButtons"));

        }
        else if (XRSettings.loadedDeviceName == "OpenVR")
            return (SteamVR_Input.GetFloat("SqueezeGrip", SteamVR_Input_Sources.RightHand, true) > .5f || SteamVR_Input.GetState("GrabGrip", SteamVR_Input_Sources.RightHand, true));

        else
            return false;
        //else if (XRSettings.loadedDeviceName == "OpenVR")
        //{

        //    return ();

        //}
    }

    private bool Click()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
        {

            return (Input.GetButtonDown("WeaponSelect"));//(Input.GetAxis("LGrip") == 1 && Input.GetButton("LTouchButtons"));

        }
        else if (XRSettings.loadedDeviceName == "OpenVR")
            return SteamVR_Input.GetState("Click", SteamVR_Input_Sources.Any, true);

        else
            return false;
        //else if (XRSettings.loadedDeviceName == "OpenVR")
        //{

        //    return ();

        //}
    }

    private bool TouchLeft()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
        {

            return (Input.GetButton("LTouchTop"));//(Input.GetAxis("LGrip") == 1 && Input.GetButton("LTouchButtons"));

        }
        else if (XRSettings.loadedDeviceName == "OpenVR")
            return SteamVR_Input.GetState("Touchpad", SteamVR_Input_Sources.LeftHand);

        else
            return false;
        //else if (XRSettings.loadedDeviceName == "OpenVR")
        //{

        //    return ();

        //}
    }

    private bool TouchRight()
    {
        if (XRSettings.loadedDeviceName == "Oculus")
        {

            return (Input.GetButton("RTouchTop"));//(Input.GetAxis("LGrip") == 1 && Input.GetButton("LTouchButtons"));

        }
        else if (XRSettings.loadedDeviceName == "OpenVR")
            return SteamVR_Input.GetState("Touchpad", SteamVR_Input_Sources.RightHand);

        else
            return false;
        //else if (XRSettings.loadedDeviceName == "OpenVR")
        //{

        //    return ();

        //}
    }


    #endregion

    void Start()
    {
        worldManager = SCR_Network_GameManager.instance.worldManager;
        view = GetComponent<PhotonView>();

        netPos = new Vector3[netTransforms.Length];
        netRot = new Vector3[netTransforms.Length];
        netPosV = new Vector3[netTransforms.Length];
        netRotV = new Vector3[netTransforms.Length];
        deadStateTime = deadTime / 6;

        foreach (SCR_GLOBAL_NetId item in netColiders)
        {
            item.id = view.ViewID;
        }

        if (view.IsMine)
        {
            //init vars
            gameManager = SCR_Network_GameManager.instance;
            playerRig = gameManager.playerRig;
            player = playerRig.gameObject.transform;
            slot = 1;
            pLHand = playerRig.leftHandModel.GetComponent<SCR_GLOBAL_RenderPlayerModel>().model.GetComponent<SCR_GLOBAL_AnimationInput>();
            pRHand = playerRig.rightHandModel.GetComponent<SCR_GLOBAL_RenderPlayerModel>().model.GetComponent<SCR_GLOBAL_AnimationInput>();
            pLHandTransform = pLHand.gameObject.transform;
            pRHandTransform = pRHand.gameObject.transform;
            pLHandModel = pLHand.gameObject.transform;
            pRHandModel = pRHand.gameObject.transform;
            leftGun = new SCR_GLOBAL_Gun[leftGunModel.Length];
            rightGun = new SCR_GLOBAL_Gun[rightGunModel.Length];
            sIndex = 1 / speed;
            wInit = false;
            leftGun = new SCR_GLOBAL_Gun[leftGunModel.Length];
            rightGun = new SCR_GLOBAL_Gun[rightGunModel.Length];
            hurtColor.r = .5f - playerRig.color.r;
            hurtColor.g = .5f - playerRig.color.g;
            hurtColor.b = .5f - playerRig.color.b;
            initColor = playerRig.color;

            playerList = PhotonNetwork.PlayerListOthers;
            steerModelLeft.GetComponent<SCR_GLOBAL_RenderPlayerModel>().dissolve = 1;
            steerModelRight.GetComponent<SCR_GLOBAL_RenderPlayerModel>().dissolve = 1;
            steerModelLeft.GetComponent<SCR_GLOBAL_RenderPlayerModel>().color = initColor;
            steerModelRight.GetComponent<SCR_GLOBAL_RenderPlayerModel>().color = initColor;

            foreach (SCR_GLOBAL_NetId item in netColiders)
            {
                item.gameObject.SetActive(false);
            }

            //Init properties
            if (playerInfo.ContainsKey("playerHealth"))
            {
                playerInfo.Remove("playerHealth");
            }
            playerInfo.Add("playerHealth", 100);

            PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

            if (playerInfo.ContainsKey("playerScore"))
            {
                playerInfo.Remove("playerScore");
            }
            playerInfo.Add("playerScore", 0);

            PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

            health = 100;



            for (int i = 0; i < leftGun.Length; i++)
            {
                leftGun[i] = leftGunModel[i].model.gameObject.GetComponent<SCR_GLOBAL_Gun>();
                if(rightGunModel[i])
                rightGun[i] = rightGunModel[i].model.gameObject.GetComponent<SCR_GLOBAL_Gun>();

                leftGunModel[i].dissolve = 1;
                if(rightGunModel[i])
                rightGunModel[i].dissolve = 1;
            }

            foreach (SCR_GLOBAL_Gun item in leftGun)
            {
                item.view = view;
            }
            foreach (SCR_GLOBAL_Gun item in rightGun)
            {
                if(item)
                item.view = view;
            }

            for (int i = 0; i < leftGunModel.Length; i++)
            {
                leftGunModel[i].color = nodesLeft[i].color;
            }
            for (int i = 0; i < leftGunModel.Length; i++)
            {
                if(rightGunModel[i])
                rightGunModel[i].color = nodesLeft[i].color;
            }

            //Init models
            foreach (SCR_GLOBAL_RenderPlayerModel item in models)
            {
                item.dissolve = 1;
            }

            //Oculus cal
            if (XRSettings.loadedDeviceName == "Oculus")
                playerSnap.localPosition = new Vector3(playerSnap.localPosition.x, playerSnap.localPosition.y + playerRig.oculusFloorOffset, playerSnap.localPosition.z);

            curSpeed = 0;
        }
        else
        {

            rigCollider.enabled = false;
            //foreach (SCR_GLOBAL_Gun item in leftGun)
            //{
            //    if(item)
            //        item.enabled = false;
            //}
            //foreach (SCR_GLOBAL_Gun item in rightGun)
            //{
            //    if (item)
            //        item.enabled = false;
            //}
        }

        //init death models
        int ll = 0;
        int rr = 0;
        int bb = 0;
        int bm = 0;
        int[] l = new int[leftGunModel.Length];
        int[] r = new int[rightGunModel.Length];
        int[] b = new int[models.Length];
        int[] bbm = new int[models.Length];

        for (int i = 0; i < leftGunModel.Length; i++)
        {
            l[i] = leftGunModel[i].model.GetComponent<SCR_GLOBAL_PlayerModel>().renderers.Length;
            ll += l[i];
        }
        for (int i = 0; i < rightGunModel.Length; i++)
        {
            if(rightGunModel[i])
            r[i] = rightGunModel[i].model.GetComponent<SCR_GLOBAL_PlayerModel>().renderers.Length;
            rr += r[i];
        }
        for (int i = 0; i < models.Length; i++)
        {
            b[i] = models[i].model.GetComponent<SCR_GLOBAL_PlayerModel>().renderers.Length;
            bb += b[i];
        }
        for (int i = 0; i < models.Length; i++)
        {
            bbm[i] = models[i].model.GetComponent<SCR_GLOBAL_PlayerModel>().multiRenderers.Length;
            bm += bbm[i];
        }


        deadModels = new Transform[ll + rr + bb + bm];
        deadPos = new Vector3[deadModels.Length];
        deadRot = new Vector3[deadModels.Length];
        explodeForce = new Vector3[deadModels.Length];

        int m = 0;
        for (int i = 0; i < leftGunModel.Length; i++)
        {
            for (int ii = 0; ii < l[i]; ii++)
            {
                deadModels[m] = leftGunModel[i].model.GetComponent<SCR_GLOBAL_PlayerModel>().renderers[ii].transform;
                m++;
            }
        }

        for (int i = 0; i < rightGunModel.Length; i++)
        {
            for (int ii = 0; ii < r[i]; ii++)
            {
                if(rightGunModel[i])
                deadModels[m] = rightGunModel[i].model.GetComponent<SCR_GLOBAL_PlayerModel>().renderers[ii].transform;
                m++;
            }
        }

        for (int i = 0; i < models.Length; i++)
        {
            for (int ii = 0; ii < b[i]; ii++)
            {
                deadModels[m] = models[i].model.GetComponent<SCR_GLOBAL_PlayerModel>().renderers[ii].transform;
                m++;
            }
        }

        for (int i = 0; i < models.Length; i++)
        {
            for (int ii = 0; ii < bbm[i]; ii++)
            {
                deadModels[m] = models[i].model.GetComponent<SCR_GLOBAL_PlayerModel>().multiRenderers[ii].transform;
                m++;
            }
        }


        for (int i = 0; i < deadModels.Length; i++)
        {
            deadPos[i] = deadModels[i].localPosition;
            deadRot[i] = deadModels[i].localEulerAngles;
            explodeForce[i] = new Vector3(Random.Range(xExplodeForce.x, xExplodeForce.y), Random.Range(yExplodeForce.x, yExplodeForce.y), Random.Range(zExplodeForce.x, zExplodeForce.y));
        }

    }

    private void LateUpdate()
    {
        if(view.IsMine)
        {
            steerModelLeft.position = pLHandTransform.position;
            steerModelRight.position = pRHandTransform.position;
            steerModelLeft.rotation = pLHandTransform.rotation;
            steerModelRight.rotation = pRHandTransform.rotation;
        }
    }

    private void Update()
    {
        state = worldManager.state;
        switch (state)
        {
            case 1:
                if(view.IsMine)
                {
                    foreach (SCR_GLOBAL_RenderPlayerModel item in models)
                    {
                        item.dissolve = 0;
                    }

                    if (!spawnIndex)
                    {
                        spawnClip.Play();
                        spawnIndex = true;
                    }
                }
                break;


            case 2:
                if (view.IsMine)
                {
                    //Activate default slot 
                    if (leftGunModel[slot - 1].dissolve != 0f)
                        leftGunModel[slot - 1].dissolve = 0f;
                    if(rightGunModel[slot-1])
                    if (rightGunModel[slot - 1].dissolve != 0f)
                        rightGunModel[slot - 1].dissolve = 0f;

                    if (!wInit)
                    {
                        gunAudio.PlayOneShot(gunClips[slot - 1]);
                        wInit = true;
                    }

                }
                break;

            case 4:

                    StateFour();
                break;

            case 5:
                curSpeed = 0f;
                smoothSpeed = 0f;
                break;

            case 6:
                foreach (SCR_GLOBAL_RenderPlayerModel item in models)
                {
                    item.dissolve = 1;
                }

                leftGunModel[slot - 1].dissolve = 1f;
                if(rightGunModel[slot-1])
                rightGunModel[slot - 1].dissolve = 1f;
                break;
        }

        if(view.IsMine)
        {
            player.position = playerSnap.position;
            player.rotation = playerSnap.rotation;

            //Mod audio
            audio.pitch = (smoothSpeed * (sIndex)) + (Mathf.Abs(smoothTilt) * .001f);

            foreach (SCR_GLOBAL_RenderPlayerModel item in models)
            {
                item.color = playerRig.color;
            }
        }
    }

    void FixedUpdate()
    {
        if (view.IsMine && state == 4 && state != 1)
        {
            //Smoothing
            smoothSpeed = Mathf.SmoothDamp(smoothSpeed, curSpeed, ref smoothSpeedV, positionDamping * Time.deltaTime);
            smoothRot = Mathf.SmoothDamp(smoothRot, wheelSteer * steerAmount, ref smoothRotV, steeringDamping * Time.deltaTime);
            smoothTilt = Mathf.SmoothDamp(smoothTilt, wheelSteer * tiltAmount, ref smoothTiltV, tiltDamping * Time.deltaTime);

            //Ray
            if (Physics.Raycast(ray.position, ray.forward, out hit, Mathf.Infinity, mask))
            {
                //Set rig transform
                rig.position = hit.point;
                rig.rotation = Quaternion.FromToRotation(rig.up, hit.normal) * rig.rotation;

                //Set steerPivot
                steerPivot.localEulerAngles = new Vector3(steerPivot.localEulerAngles.x, steerPivot.localEulerAngles.y + smoothRot, steerPivot.localEulerAngles.z);
                steerPivot.localPosition = new Vector3(0, 0 + floorOffset, 0);

                //Drive with smoothSpeed
                rig.position += steerPivot.forward * smoothSpeed * Time.deltaTime;
            }
            else
            {
                Debug.Log("Did not Hit");
            }

            //rollAmount
            rollPivot.localEulerAngles = new Vector3(0, 0, smoothTilt / 2);
            if (slot < 4)
            {
                if (Physics.Raycast(weaponRay.position, weaponRay.forward, out gunHit, Mathf.Infinity))
                {
                    foreach (SCR_GLOBAL_Gun item in leftGun)
                    {
                        Vector3 a = new Vector3(Random.Range(-item.accuracy, item.accuracy), Random.Range(-item.accuracy, item.accuracy), Random.Range(-item.accuracy, item.accuracy));
                        foreach (GameObject barrel in item.barrels)
                        {
                            barrel.transform.LookAt(gunHit.point + (a * accuracy));
                        }
                    }
                    foreach (SCR_GLOBAL_Gun item in rightGun)
                    {
                        if (item)
                        {
                            Vector3 a = new Vector3(Random.Range(-item.accuracy, item.accuracy), Random.Range(-item.accuracy, item.accuracy), Random.Range(-item.accuracy, item.accuracy));
                            foreach (GameObject barrel in item.barrels)
                            {
                                barrel.transform.LookAt(gunHit.point + (a * accuracy));
                            }
                        }
                    }
                }
                else
                {
                    foreach (SCR_GLOBAL_Gun item in leftGun)
                    {
                        foreach (GameObject barrel in item.barrels)
                        {
                            if (barrel.transform.forward != weaponRay.forward)
                                barrel.transform.forward = weaponRay.forward;
                        }
                    }
                    foreach (SCR_GLOBAL_Gun item in rightGun)
                    {
                        if (item)
                            foreach (GameObject barrel in item.barrels)
                            {
                                if (barrel.transform.forward != weaponRay.forward)
                                    barrel.transform.forward = weaponRay.forward;
                            }
                    }
                }
            }
        }
    }

    public void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    public void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(netPos);
            stream.SendNext(netRot);
        }
        else
        {
            netPos = (Vector3[])stream.ReceiveNext();
            netRot = (Vector3[])stream.ReceiveNext();
        }
    }



    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;

        if (view.IsMine)
        {


            switch (eventCode)
            {

                case 1:

                    //string data = (string)photonEvent.CustomData;

                    string s = (string)photonEvent.CustomData;
                    Color c;
                    ColorUtility.TryParseHtmlString(s, out c);
                    if(c!=Color.white)
                        gameManager.worldColor = c;
                    Debug.Log("Received " + c);
                    break;

                case 2:

                    string[] ss = (string[])photonEvent.CustomData;
                    Color[] colors = new Color[2];

                    ColorUtility.TryParseHtmlString(ss[0], out colors[0]);
                    ColorUtility.TryParseHtmlString(ss[1], out colors[1]);


                    worldManager.Pulse(colors[0],colors[1]);


                    break;


                case 3:


                    int[] d = (int[])photonEvent.CustomData;

                    if (d[0] == photonView.ViewID && playerRig.state != 1)
                    {
                        d[1] = health - d[1];
                        health = d[1];
                        if (d[1] <= 0)
                        {
                            playerRig.state = 1;
                            string colorString = "#" + ColorUtility.ToHtmlStringRGBA(playerRig.color); // Array contains the target position and the IDs of the selected units
                            object[] content = new object[] { d[2] , colorString }; // Array contains the target position and the IDs of the selected units
                            RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                            SendOptions sendOptions = new SendOptions { Reliability = true };
                            PhotonNetwork.RaiseEvent(4, content, raiseEventOptions, sendOptions);
                            Debug.Log("Killed by  " + d[2]);
                        }
                        else
                        {
                            cHurtTime = hurtTime;
                            hurtClip.Play();
                        }

                        if (playerInfo.ContainsKey("playerHealth"))
                        {
                            playerInfo.Remove("playerHealth");
                        }
                        playerInfo.Add("playerHealth", health);

                        PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

                        Debug.Log("Damaged " + d);

                    }
                    break;

                case 4:

                    object[] v = (object[])photonEvent.CustomData;

                    if (PhotonNetwork.LocalPlayer == PhotonNetwork.GetPhotonView((int)v[0]).Owner)
                    {
                        score++;
                        if (playerInfo.ContainsKey("playerScore"))
                        {
                            playerInfo.Remove("playerScore");
                        }
                        playerInfo.Add("playerScore", score);
                        PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

                        bool t = false;
                        foreach (Player item in playerList)
                        {
                            if (score > System.Convert.ToInt32(item.CustomProperties["playerScore"].ToString()))
                                t = true;
                        }

                        if (t)
                        {
                            string content = "#" + ColorUtility.ToHtmlStringRGBA(initColor); // Array contains the target position and the IDs of the selected units
                            RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                            SendOptions sendOptions = new SendOptions { Reliability = true };
                            PhotonNetwork.RaiseEvent(1, content, raiseEventOptions, sendOptions);
                            Debug.Log("Sent " + playerRig.color);

                        }
                        else
                        {
                            string[] content = new string[] { "#" + ColorUtility.ToHtmlStringRGBA(initColor), (string)v[1] }; // Array contains the target position and the IDs of the selected units
                            RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                            SendOptions sendOptions = new SendOptions { Reliability = true };
                            PhotonNetwork.RaiseEvent(2, content, raiseEventOptions, sendOptions);
                        }
                    }

                    break;

                case 5:

                    int[] p = (int[])photonEvent.CustomData;
                    Debug.Log("Received");

                    if (view.ViewID == p[2])
                    {
                        Debug.Log("Match");

                        if (p[0] == 0)
                        {
                            Debug.Log("Equiped");

                            if(!equiped[p[1]])
                            {
                                equiped[p[1]] = true;


                                leftGunModel[slot - 1].dissolve = 1f;
                                if (rightGunModel[slot - 1])
                                    rightGunModel[slot - 1].dissolve = 1f;

                                slot = p[1] + 1;

                                leftGunModel[slot - 1].dissolve = 0f;
                                if (rightGunModel[slot - 1])
                                    rightGunModel[slot - 1].dissolve = 0f;
                                if (gunClips[slot - 1])
                                    gunAudio.PlayOneShot(gunClips[slot - 1]);


                                ammo[p[1]] = capacity[p[1]];

                            }
                            else
                            {
                                if (gunClips[slot - 1])
                                    gunAudio.PlayOneShot(gunClips[p[1]]);
                                ammo[p[1]] += replenishment[p[1]];

                            }

                        }
                    }

                    break;
            }


        }

    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {

        if(view && !view.IsMine && targetPlayer == view.Owner)
        {

            //ColorUtility.TryParseHtmlString((string)targetPlayer.CustomProperties["color"], out sColor);

            int h = (int)targetPlayer.CustomProperties["playerHealth"];
            Debug.Log(health);
            if (h<=0)
            {
                netState = 1;
            }
            Debug.Log(health);
        }
    }

    void StateFour()
    {
        if (view.IsMine)
        {
            dead = playerRig.state;

            if (dead != 1)
            {
                if (!bIndex)
                {
                    tempMusic.Play();
                    bIndex = true;
                }

                if (cHurtTime > 0f)
                {
                    cHurtTime -= Time.deltaTime;
                    playerRig.color = hurtColor;
                }
                else
                    playerRig.color = initColor;


                if (Input.GetKeyDown(KeyCode.C))
                {
                    string content = "#" + ColorUtility.ToHtmlStringRGBA(initColor); // Array contains the target position and the IDs of the selected units
                    RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                    SendOptions sendOptions = new SendOptions { Reliability = true };
                    PhotonNetwork.RaiseEvent(1, content, raiseEventOptions, sendOptions);
                    Debug.Log("Sent " + playerRig.color);
                }

                if (Input.GetKeyDown(KeyCode.K))
                {
                    int[] content = new int[] {id, 25, view.ViewID} ; // Array contains the target position and the IDs of the selected units
                    RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                    SendOptions sendOptions = new SendOptions { Reliability = true };
                    PhotonNetwork.RaiseEvent(3, content, raiseEventOptions, sendOptions);
                    Debug.Log("Damaged players " + 25);
                }

                curSpeed = speed;

                //Input
                click = Click();
                axis = Axis();
                wheelGrippedLeft = WheelGrippedLeft();
                wheelGrippedRight = WheelGrippedRight();
                trigger = Trigger();
                touchLeft = TouchLeft();
                touchRight = TouchRight();


                foreach (SCR_GLOBAL_Gun item in leftGun)
                {
                    if (!item.enabled)
                        item.enabled = true;
                }

                foreach (SCR_GLOBAL_Gun item in rightGun)
                {
                    if(item)
                    if (!item.enabled)
                        item.enabled = true;
                }

                for (int i = 0; i < deadModels.Length; i++)
                {
                    if (deadModels[i].localPosition != deadPos[i])
                        deadModels[i].localPosition = deadPos[i];

                    if (deadModels[i].localEulerAngles != deadRot[i])
                        deadModels[i].localEulerAngles = deadRot[i];
                }

                if (leftGunModel[slot - 1].dissolve != 0)
                    leftGunModel[slot - 1].dissolve = 0;

                if(rightGunModel[slot-1])
                if (rightGunModel[slot - 1].dissolve != 0)
                    rightGunModel[slot - 1].dissolve = 0;

                foreach (SCR_GLOBAL_RenderPlayerModel item in models)
                {
                    if (item.dissolve != 0)
                        item.dissolve = 0;
                }

                for (int i = 0; i < ammo.Length; i++)
                {
                    if (ammo[i] < 0)
                        ammo[i] = 0;
                    else if (ammo[i] > capacity[i])
                        ammo[i] = capacity[i];
                }

                //draw ammo
                indicator.text = ammo[slot - 1].ToString() + "/" + capacity[slot - 1].ToString();
                indicator.color = new Color(nodesLeft[slot - 1].color.r, nodesLeft[slot - 1].color.g, nodesLeft[slot - 1].color.b,1);

                //Select weapon
                if (click)
                {
                    float a = weaponWheel.localEulerAngles.y;
                    float c = 360 / weaponCount;
                    for (int i = 1; i < weaponCount + 1; i++)
                    {
                        if (a > (c * i) - c && a < (c * i) && slot != i && equiped[i-1])
                        {
                            leftGunModel[slot - 1].dissolve = 1f;
                            if(rightGunModel[slot-1])
                            rightGunModel[slot - 1].dissolve = 1f;

                            slot = i;

                            leftGunModel[slot - 1].dissolve = 0f;
                            if (rightGunModel[slot - 1])
                                rightGunModel[slot - 1].dissolve = 0f;
                            if(gunClips[slot - 1])
                            gunAudio.PlayOneShot(gunClips[slot - 1]);
                        }
                    }
                }

                //weapon wheel
                if(touchLeft && !touchRight)
                {
                    for (int i = 0; i < nodesLeft.Length; i++)
                    {
                        nodesLeft[i].dissolve = 0;

                        if (i == slot - 1)
                            nodesLeft[i].intensity = 1.1f;
                        else
                            nodesLeft[i].intensity = 0.5f;

                        if(!equiped[i])
                            nodesLeft[i].intensity = 0f;


                    }
                    nodePointerLeft.localEulerAngles = new Vector3(nodePointerLeft.localEulerAngles.x, nodePointerLeft.localEulerAngles.y, weaponWheel.localEulerAngles.y * -1 + 22.5f);

                    nodePointerLeft.GetComponent<SCR_GLOBAL_RenderPlayerModel>().dissolve = 0;
                }
                else if (!touchLeft && touchRight)
                {
                    for (int i = 0; i < nodesRight.Length; i++)
                    {
                        nodesRight[i].dissolve = 0;

                        if (i == slot - 1)
                            nodesRight[i].intensity = 1.1f;
                        else
                            nodesRight[i].intensity = 0.5f;

                        if (!equiped[i])
                            nodesRight[i].intensity = 0f;
                    }
                    nodePointerRight.localEulerAngles = new Vector3(nodePointerRight.localEulerAngles.x, nodePointerRight.localEulerAngles.y, weaponWheel.localEulerAngles.y * -1 + 22.5f);

                    nodePointerRight.GetComponent<SCR_GLOBAL_RenderPlayerModel>().dissolve = 0;
                }
                else
                {
                    foreach (SCR_GLOBAL_RenderPlayerModel item in nodesLeft)
                    {
                        item.dissolve = 1;
                    }
                    nodePointerLeft.GetComponent<SCR_GLOBAL_RenderPlayerModel>().dissolve = 1;
                    foreach (SCR_GLOBAL_RenderPlayerModel item in nodesRight)
                    {
                        item.dissolve = 1;
                    }
                    nodePointerRight.GetComponent<SCR_GLOBAL_RenderPlayerModel>().dissolve = 1;
                }

                //Shoot
                if (trigger > .9f && ammo[slot-1] > 0)
                {
                    if (leftGunModel[slot - 1].dissolve == 0f)
                        leftGun[slot - 1].Fire();
                    if (rightGunModel[slot - 1])
                        if (rightGunModel[slot - 1].dissolve == 0f)
                        rightGun[slot - 1].Fire();
                }

                //Model updates
                model.rotation = SmoothDamp(model.rotation, rollPivot.rotation, ref smoothModelV, modelDamping * Time.deltaTime);
                model.position = steerPivot.position;
                bikeModel.localEulerAngles = new Vector3(0, 0, smoothTilt / 2);

                //Net send
                for (int i = 0; i < netTransforms.Length; i++)
                {
                    netPos[i] = netTransforms[i].position;
                    netRot[i] = netTransforms[i].eulerAngles;
                }
            }
            else
            {
                if(health != 100)
                {
                    health = 100;
                    if (playerInfo.ContainsKey("playerHealth"))
                    {
                        playerInfo.Remove("playerHealth");
                    }
                    playerInfo.Add("playerHealth", health);

                    PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);

                }

                if (smoothSpeed != 0f)
                    smoothSpeed = 0;

                foreach (SCR_GLOBAL_Gun item in leftGun)
                {
                    if (item.enabled)
                        item.enabled = false;
                }

                foreach (SCR_GLOBAL_Gun item in rightGun)
                {
                    if(item)
                    if (item.enabled)
                        item.enabled = false;
                }

                slot = 1;

                ammo[0] = capacity[0];

                for (int i = 1; i < equiped.Length; i++)
                {
                    equiped[i] = false;
                    ammo[i] = 0;
                }

                for (int i = 0; i < deadModels.Length; i++)
                {
                    deadModels[i].localPosition += explodeForce[i] * Time.deltaTime;
                    deadModels[i].localEulerAngles += explodeForce[i] * Time.deltaTime * 4f;
                }

                if (playerRig.deadState == 3)
                {
                    foreach (SCR_GLOBAL_RenderPlayerModel item in leftGunModel)
                    {
                        if (item.dissolve != 1)
                            item.dissolve = 1;
                    }

                    foreach (SCR_GLOBAL_RenderPlayerModel item in rightGunModel)
                    {
                        if(item)
                        if (item.dissolve != 1)
                            item.dissolve = 1;
                    }

                    foreach (SCR_GLOBAL_RenderPlayerModel item in models)
                    {
                        if (item.dissolve != 1)
                            item.dissolve = 1;
                    }
                }
            }
            UpdateSteering();

            //Temp weapon wheel model
            weaponWheel.localEulerAngles = new Vector3(0, (Mathf.Atan2(axis.y, axis.x) * -180 / Mathf.PI), 0);

        }
        else
        {
            switch (netState)
            {
                case 0:


                    if (cDeadTime <= 0)
                        cDeadTime = deadTime;

                    if (deadState != 0)
                        deadState = 0;

                    for (int i = 0; i < deadModels.Length; i++)
                    {
                        if (deadModels[i].localPosition != deadPos[i])
                            deadModels[i].localPosition = deadPos[i];

                        if (deadModels[i].localEulerAngles != deadRot[i])
                            deadModels[i].localEulerAngles = deadRot[i];
                    }

                    break;

                case 1:
                    if (cDeadTime > 0)
                    {


                        //if (CdeadStateTime > 0)
                        //{

                        //    CdeadStateTime -= t;
                        //}
                        //else
                        //{
                        //    CdeadStateTime = deadStateTime;
                        //    deadState++;
                        //}

                        cDeadTime -= Time.deltaTime;
                    }
                    else
                    {
                        netState = 0;
                    }
                    break;

                case 2:

                    break;
            }


            //Net receive
            for (int i = 0; i < netTransforms.Length; i++)
            {
                netTransforms[i].position = Vector3.Lerp(netTransforms[i].position, netPos[i], lerpPosition);
                netTransforms[i].eulerAngles = new Vector3(Mathf.LerpAngle(netTransforms[i].eulerAngles.x, netRot[i].x, lerpRotation), Mathf.LerpAngle(netTransforms[i].eulerAngles.y, netRot[i].y, lerpRotation), Mathf.LerpAngle(netTransforms[i].eulerAngles.z, netRot[i].z, lerpRotation)); ;
            }

            if(netState == 1)
            for (int i = 0; i < deadModels.Length; i++)
            {
                deadModels[i].localPosition += explodeForce[i] * Time.deltaTime;
                deadModels[i].localEulerAngles += explodeForce[i] * Time.deltaTime * 4f;
            }
        }
    }

    void UpdateSteering()
    {

        //Sync steerLeft transforms
        steerLeft.position = pLHandModel.position;
        steerLeftRef.rotation = Quaternion.FromToRotation(steerLeftRef.up, pLHandModel.up) * steerLeftRef.rotation;
        steerLeftRot.rotation = pLHandModel.rotation;
        steerRight.position = pRHandModel.position;
        steerRightRef.rotation = Quaternion.FromToRotation(steerRightRef.up, pRHandModel.up) * steerRightRef.rotation;
        steerRightRot.rotation = pRHandModel.rotation;

        //steerLeft
        if (wheelGrippedLeft && !wheelGrippedRight)
        {
            leftNode2.position = leftNode1.position;

            if (!hasGripped)
            {
                hasGripped = true;
            }
            else
            {
                if (leftNode2.localPosition.z > 0)
                {
                    wheelSteer = Mathf.Clamp(leftNode2.localPosition.x, -1, 1);
                    wheelSteer = Mathf.Pow(Mathf.Abs(wheelSteer), steeringSensitivity) * ((1f / wheelSteer));
                    steerModelLeft.GetComponent<SCR_GLOBAL_RenderPlayerModel>().dissolve = 0;
                }
            }
        }
        else if (wheelGrippedRight && !wheelGrippedLeft)
        {
            rightNode2.position = rightNode1.position;

            if (!hasGripped)
            {
                hasGripped = true;
            }
            else
            {
                if (rightNode2.localPosition.z > 0)
                {
                    wheelSteer = Mathf.Clamp(rightNode2.localPosition.x, -1, 1);
                    steerModelRight.GetComponent<SCR_GLOBAL_RenderPlayerModel>().dissolve = 0;
                    wheelSteer = Mathf.Pow(Mathf.Abs(wheelSteer), steeringSensitivity) * ((1f / wheelSteer));
                }
            }
        }
        else
        {
            hasGripped = false;
            wheelSteer = 0;
            steerLeftRot2.rotation = pLHandModel.rotation;
            steerRightRot2.rotation = pRHandModel.rotation;
            steerModelLeft.GetComponent<SCR_GLOBAL_RenderPlayerModel>().dissolve = 1;
            steerModelRight.GetComponent<SCR_GLOBAL_RenderPlayerModel>().dissolve = 1;
        }


    }

    //Smooth quaternion
    public static Quaternion SmoothDamp(Quaternion rot, Quaternion target, ref Quaternion deriv, float time)
    {
        // account for double-cover
        var Dot = Quaternion.Dot(rot, target);
        var Multi = Dot > 0f ? 1f : -1f;
        target.x *= Multi;
        target.y *= Multi;
        target.z *= Multi;
        target.w *= Multi;
        // smooth damp (nlerp approx)
        var Result = new Vector4(
            Mathf.SmoothDamp(rot.x, target.x, ref deriv.x, time),
            Mathf.SmoothDamp(rot.y, target.y, ref deriv.y, time),
            Mathf.SmoothDamp(rot.z, target.z, ref deriv.z, time),
            Mathf.SmoothDamp(rot.w, target.w, ref deriv.w, time)
        ).normalized;
        // compute deriv
        var dtInv = 1f / Time.deltaTime;
        deriv.x = (Result.x - rot.x) * dtInv;
        deriv.y = (Result.y - rot.y) * dtInv;
        deriv.z = (Result.z - rot.z) * dtInv;
        deriv.w = (Result.w - rot.w) * dtInv;
        return new Quaternion(Result.x, Result.y, Result.z, Result.w);
    }
}
