﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SpatialTracking;
using UnityEngine;
using UnityEngine.XR;
using Photon.Pun;
using Photon.Realtime;

public class SCR_GLOBAL_Rig : MonoBehaviourPunCallbacks, IPunObservable
{
    public Transform cam;
    public AudioListener audioListener;
    public SCR_GLOBAL_PlayerModel brain;
    public Transform body;
    public Transform bodyModel; // Need for collider positioning
    public Transform neck;

    [Space(10)]

    public Transform rootPose;
    public Transform leftHandPose;
    public Transform rightHandPose;
    public Transform handRef;
    public Transform leftHandModel;
    public Transform rightHandModel;

    [Space(10)]

    public float oculusFloorOffset;
    public Vector3[] handModelOffsetPositions;
    public Vector3[] handModelOffsetRotations;

    [Space(10)]

    public Color color;
    public Color zColor;

    [Range(0f,1f)]
    public float dissolve;
    public SCR_GLOBAL_RenderPlayerModel[] renderModels;
    public Transform reflectionPlane;

    [Space(10)]

    public TrackedPoseDriver[] networkTPoses;
    public GameObject[] networkObjects;
    public MonoBehaviour[] networkScripts;

    [Space(10)]

    public Transform[] netTransforms;
    public float lerpPosition;
    public float lerpRotation;

    [Space(10)]

    public int defaultState;
    public SCR_GLOBAL_DeathRig deathRig;
    public SCR_GLOBAL_PlayerModel[] deathModels;
    public float deadTime;
    public float deadFloat;
    public float deathFadeSpeed;
    public Renderer deathFader;
    public AudioSource deathClip;

    public int state;
    private Transform tCache;
    private Vector3[] netPos;
    private Vector3[] netRot;
    private float initDissolve;
    private SCR_GLOBAL_WorldManager worldManager;
    private bool spawnIndex;
    //public bool dead;
    private float cDeadTime;
    private float deadStateTime;
    private float CdeadStateTime;
    [HideInInspector]
    public int deadState;
    private RaycastHit hit;
    public LayerMask mask;
    private Material deathMaterial;
    private ExitGames.Client.Photon.Hashtable playerInfo = new ExitGames.Client.Photon.Hashtable();
    private SCR_GLOBAL_AnimationInput hAnim;


    void Awake()
    {

            DontDestroyOnLoad(gameObject);

    }
    void Start()
    {
        netPos = new Vector3[netTransforms.Length];
        netRot = new Vector3[netTransforms.Length];
        //rightHandModel.GetComponent<SCR_GLOBAL_RenderPlayerModel>().model.GetComponent<SCR_GLOBAL_AnimationInput>().view = photonView;
        //leftHandModel.GetComponent<SCR_GLOBAL_RenderPlayerModel>().model.GetComponent<SCR_GLOBAL_AnimationInput>().view = photonView;

        if (photonView.IsMine)
        {
            if (playerInfo.ContainsKey("playerRig"))
            {
                playerInfo.Remove("playerRig");
            }
            playerInfo.Add("playerRig", photonView.ViewID);

            PhotonNetwork.LocalPlayer.SetCustomProperties(playerInfo, null, null);


            //init
            tCache = transform;
            EnableObjects();

            ColorUtility.TryParseHtmlString(PlayerPrefs.GetString("PLAYER_COLOR"), out color);

            if (XRSettings.loadedDeviceName == "Oculus")
                tCache.position = new Vector3(tCache.position.x, oculusFloorOffset, tCache.position.y);


            //init hands
            int o = leftHandModel.gameObject.GetComponent<SCR_GLOBAL_RenderPlayerModel>().model.GetComponent<SCR_GLOBAL_AnimationInput>().deviceIndex;
            leftHandModel.localPosition = handModelOffsetPositions[o];
            leftHandModel.localEulerAngles = handModelOffsetRotations[o];
            rightHandModel.localPosition = Vector3.Scale(handModelOffsetPositions[o],new Vector3(-1,1,1));
            rightHandModel.localEulerAngles = handModelOffsetRotations[o];

            //init visibility layers
            SCR_GLOBAL_PlayerModel r = renderModels[1].GetComponent<SCR_GLOBAL_RenderPlayerModel>().model.GetComponent<SCR_GLOBAL_PlayerModel>();
            r.renderers[0].gameObject.layer = 17;
            r.renderers[1].gameObject.layer = 17;
            renderModels[0].GetComponent<SCR_GLOBAL_RenderPlayerModel>().model.GetComponent<SCR_GLOBAL_PlayerModel>().renderers[0].gameObject.layer = 17;
            brain.gameObject.layer = 17;
            brain.dissolveLerp = 2f;

            foreach (SCR_GLOBAL_PlayerModel item in deathModels)
            {
                item.dissolveLerp = 2f;
                item.colorLerp = 2f;
                item.dissolve = 1;
                item.intensity = 1.1f;
                item.color = Color.white;
            }
            deathModels[2].gameObject.layer = 17;

            dissolve = 1;
            deadStateTime = deadTime / 6;
            deathMaterial = deathFader.material;

        }
        else
        {
            DisableObjects();
            dissolve = 0;
        }
    }

    private void Update()
    {
        if (photonView.IsMine)
        {
            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                state = 1;
            }

            //Init state
            if (!worldManager)
                worldManager = SCR_Network_GameManager.instance.worldManager;
            else if (!spawnIndex)
            {
                if (worldManager.state == 0)
                    dissolve = 1;
                else if (worldManager.state == 1)
                {
                    dissolve = 0;
                    spawnIndex = true;
                }
            }

            if (worldManager)
                if (worldManager.state == 4 && state != 1)
                    dissolve = 0;

            switch(state)
            {
                case 0:
                    //Color sync
                    for (int i = 0; i < renderModels.Length; i++)
                    {
                        if (i != 2)
                        {
                            renderModels[i].color = color;
                            renderModels[i].dissolve = dissolve;
                        }
                        else
                        {
                            renderModels[i].color = color;
                            if (SCR_Network_GameManager.instance.gamemode == SCR_Network_GameManager.GameMode.bikes)
                                renderModels[i].dissolve = 1f;
                            else
                                renderModels[i].dissolve = dissolve;
                        }
                        brain.dissolve = dissolve;
                    }


                    if (cDeadTime <= 0)
                        cDeadTime = deadTime;

                    if (deadState != 0)
                        deadState = 0;

                    Color c = deathMaterial.GetColor("_Color");
                    if (c.a > 0)
                        deathMaterial.SetColor("_Color", new Color(c.r, c.g, c.b, c.a - (Time.deltaTime * deathFadeSpeed)));

                    break;

                case 1:
                    if (cDeadTime > 0)
                    {
                        for (int i = 0; i < renderModels.Length; i++)
                        {
                            renderModels[i].color = Color.white;
                        }
                        renderModels[0].dissolve = 1;

                        float t = Time.deltaTime;
                        rootPose.position += rootPose.up * (deadFloat * Time.deltaTime);

                        if (CdeadStateTime > 0)
                        {

                            switch (deadState)
                            {
                                case 1:
                                    foreach (SCR_GLOBAL_PlayerModel item in deathModels)
                                    {
                                        if (item.dissolve > 0)
                                            item.dissolve = 0;
                                    }
                                    if (renderModels[2].dissolve != 0)
                                        renderModels[2].dissolve = 0;
                                    break;

                                case 4:
                                    for (int i = 0; i < renderModels.Length; i++)
                                    {
                                        renderModels[i].dissolve = 1;
                                    }
                                    foreach (SCR_GLOBAL_PlayerModel item in deathModels)
                                    {
                                        item.dissolve = 1;
                                    }
                                    brain.dissolve = 1;
                                    break;

                                case 5:
                                    Color c1 = deathMaterial.GetColor("_Color");
                                    deathMaterial.SetColor("_Color", new Color(c1.r, c1.g, c1.b, c1.a + (Time.deltaTime * deathFadeSpeed)));
                                    break;
                            }
                            CdeadStateTime -= t;
                        }
                        else
                        {
                            CdeadStateTime = deadStateTime;
                            if (deadState == 0)
                                deathClip.Play();
                            deadState++;
                        }

                        cDeadTime -= t;
                    }
                    else
                    {
                        rootPose.localPosition = Vector3.zero; ;
                        state = defaultState;
                    }
                    break;

                case 3:
                    for (int i = 0; i < renderModels.Length; i++)
                    {
                        renderModels[i].color = zColor;
                    }
                    renderModels[0].dissolve = 1;
                    break;
            }


            if (state != 1)
            {

            }
            else
            {
                
            }


            brain.dissolve = dissolve;

            UpdateBodyTransform();

            ////Ray
            //if (Physics.Raycast(cam.position, transform.up * -1, out hit, Mathf.Infinity, mask))
            //{

            //    reflectionPlane.position = hit.point;

            //}

                //Net send
                for (int i = 0; i < netTransforms.Length; i++)
            {
                netPos[i] = netTransforms[i].position;
                netRot[i] = netTransforms[i].eulerAngles;
            }
        }
        else
        {
            //if(rightHandModel.GetComponent<SCR_GLOBAL_RenderPlayerModel>().model.GetComponent<PhotonView>().IsMine)
            //    rightHandModel.GetComponent<SCR_GLOBAL_RenderPlayerModel>().model.GetComponent<PhotonView>().TransferOwnership(photonView.Owner);
            //if (leftHandModel.GetComponent<SCR_GLOBAL_RenderPlayerModel>().model.GetComponent<PhotonView>().IsMine)
            //    leftHandModel.GetComponent<SCR_GLOBAL_RenderPlayerModel>().model.GetComponent<PhotonView>().TransferOwnership(photonView.Owner);

            switch (state)
            {
                case 0:


                    if (cDeadTime <= 0)
                        cDeadTime = deadTime;

                    if (deadState != 0)
                        deadState = 0;


                    break;

                case 1:
                    if (cDeadTime > 0)
                    {

                        float t = Time.deltaTime;

                        if (CdeadStateTime > 0)
                        {

                            CdeadStateTime -= t;
                        }
                        else
                        {
                            CdeadStateTime = deadStateTime;
                            if (deadState == 0)
                                deathClip.Play();
                            deadState++;
                        }

                        cDeadTime -= t;
                    }
                    else
                    {
                        state = defaultState;
                    }
                    break;

                case 2:

                    break;
            }


            //Net receive
            for (int i = 0; i < netTransforms.Length; i++)
            {
                netTransforms[i].position = Vector3.Lerp(netTransforms[i].position, netPos[i], lerpPosition);
                netTransforms[i].eulerAngles = new Vector3(Mathf.LerpAngle(netTransforms[i].eulerAngles.x, netRot[i].x, lerpRotation), Mathf.LerpAngle(netTransforms[i].eulerAngles.y, netRot[i].y, lerpRotation), Mathf.LerpAngle(netTransforms[i].eulerAngles.z, netRot[i].z, lerpRotation)); ;
            }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(netPos);
            stream.SendNext(netRot);
        }
        else
        {
            netPos = (Vector3[])stream.ReceiveNext();
            netRot = (Vector3[])stream.ReceiveNext();
        }
    }

    private void UpdateBodyTransform()
    {
        //Update body position
        body.position = Vector3.Lerp(body.position, new Vector3(neck.transform.position.x, cam.transform.position.y, neck.transform.position.z), 6f * Time.deltaTime);

        //Update body rotation
        //Vectors r,m, and a for proper cache
        Vector3 r = rightHandPose.position;
        handRef.position = (leftHandPose.position + r) / 2.0f;

        Vector3 m = handRef.position;
        Vector3 handAngle = (m - body.position);
        handAngle = tCache.InverseTransformDirection(handAngle);
        handAngle.y = 0;

        Vector3 headAngle = (cam.forward);
        headAngle = tCache.InverseTransformDirection(headAngle);

        Vector3 pointAngle = (headAngle + handAngle).normalized;
        pointAngle.y = 0;

        Vector3 dirR = (r - m);

        //Handle zero vector
        if(dirR != Vector3.zero)
            handRef.rotation = Quaternion.LookRotation(dirR);

        Vector3 angleF = pointAngle - handRef.right;
        angleF.y = 0;

        Vector3 a = angleF.normalized;
        if (Vector3.Dot(a, headAngle.normalized) >= .25f)
        {
            body.localRotation = Quaternion.LookRotation(a, Vector3.up);
        }
    }

    //Old
    //private void UpdateTransform()
    //{
    //    Vector3 pos = ((rightOrigin.position + leftOrigin.position) / 2);
    //    //Vector3 pos = (head.InverseTransformDirection(leftOrigin.position) - head.InverseTransformDirection(rightOrigin.position));

    //    mid.position = (leftOrigin.position + rightOrigin.position) / 2.0f;

    //    //Debug.DrawLine(mid.position, leftOrigin.position, Color.white, .05f);
    //    //Debug.DrawLine(mid.position, rightOrigin.position, Color.white, .05f);



    //    Vector3 handAngle = (mid.position - body.position);
    //    handAngle = transform.InverseTransformDirection(handAngle);
    //    handAngle.y = 0;

    //    Vector3 headAngle = (head.forward);
    //    headAngle = transform.InverseTransformDirection(headAngle);

    //    Vector3 pointAngle = (headAngle + handAngle).normalized;
    //    pointAngle.y = 0;

    //    Vector3 dirL = (leftOrigin.position - mid.position);
    //    Vector3 dirR = (rightOrigin.position - mid.position);

    //    mid.rotation = Quaternion.LookRotation(dirR);

    //    Vector3 angleF = pointAngle - mid.right;
    //    angleF.y = 0;

    //    cross2 = Vector3.Dot(mid.forward, headAngle.normalized);
    //    cross = Vector3.Dot(angleF.normalized, headAngle.normalized);
    //    if (cross >= .25f) // or 0
    //    {
    //        body.localRotation = Quaternion.LookRotation(angleF.normalized, Vector3.up);
    //    }


    //    //Debug.DrawRay(mid.position, mid.forward, Color.blue);
    //    //Debug.DrawRay(mid.position, -mid.right, Color.red);
    //    //Debug.DrawRay(mid.position, angleF, Color.magenta);
    //    //Debug.DrawRay(body.position, handAngle, Color.blue);
    //    //Debug.DrawRay(body.position, headAngle, Color.red);
    //    //Debug.DrawRay(body.position, pointAngle, Color.magenta);


    //    bool isCross = (Vector3.Dot(transform.right, mid.position) > 0);
    //    if (!isCross && pos != Vector3.zero)
    //    {
    //        //mid.rotation = Quaternion.LookRotation(leftOrigin.position - rightOrigin.position, Vector3.forward);
    //    }
    //    //leftHand.position = transform.position;
    //    //rightHand.position = transform.position;
    //    //leftHand.rotation = transform.rotation;
    //    //rightHand.rotation = transform.rotation;
    //}

    private void EnableObjects()
    {
        for (int i = 0; i < networkTPoses.Length; i++)
        {
            if(i>0)
            {
                if (XRSettings.loadedDeviceName == "Oculus")
                {
                    networkTPoses[i].enabled = true;
                }
            }
            else if (i == 0)
                networkTPoses[i].enabled = true;
        }

        for (int i = 0; i < networkScripts.Length; i++)
        {
            networkScripts[i].enabled=true;
        }

        cam.gameObject.GetComponent<Camera>().enabled = true;
        audioListener.enabled = true;
    }

    private void DisableObjects()
    {
        for (int i = 0; i < networkObjects.Length; i++)
        {
            networkObjects[i].SetActive(false);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(handRef.position, .025f);
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(leftHandPose.position, .025f);
        Gizmos.DrawSphere(rightHandPose.position, .025f);
    }
}
