﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_BIKES_NeedleCollider : MonoBehaviour
{
    public GameObject needle;
    private Rigidbody body;
    public float time;
    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        body.MovePosition(needle.transform.position);
    }

    private void Update()
    {
        body.rotation = needle.transform.rotation;
        if(time < 0)
        {
            Destroy(needle);
            Destroy(gameObject);
        }
        time -= Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {

        if(other.gameObject.layer == 22 || other.gameObject.layer == 23)
        {
            if (other.gameObject.layer == 22)
            {
                //damage
            }
            Destroy(needle);
            Destroy(gameObject);
        }
        Debug.Log("hit: " + other.name);
    }
}
