﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_GLOBAL_SyncLights : MonoBehaviour
{
    public Light[] lights;
    public SCR_GLOBAL_PlayerModel playerModel;
    public SCR_GLOBAL_WorldManager worldManager;
    private Color color;

    // Update is called once per frame
    void Update()
    {
        if (playerModel)
            color = playerModel.color;
        if (worldManager)
            color = worldManager.color;

        foreach (Light item in lights)
        {
            item.color = color;
        }
    }
}
