﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_GLOBAL_PlayerModel : MonoBehaviourPunCallbacks, IPunObservable
{
    public Renderer[] renderers;
    public Renderer[] multiRenderers;
    public float maxDissolve = 1;
    public Vector2[] intensityEffectors;

    [HideInInspector]
    public float colorLerp;
    [HideInInspector]
    public float dissolveLerp;
    [HideInInspector]
    public Color color;
    [HideInInspector]
    public float intensity;
    [HideInInspector]
    public float dissolve;
    private string netColor;
    private float netDissolve;
    private Material[] materials;
    private Color cRef;
    private PhotonView view;

    void Start()
    {
        if (GetComponent<PhotonView>())
            view = GetComponent<PhotonView>();

        materials = new Material[renderers.Length];
        for (int i = 0; i < materials.Length; i++)
        {
            materials[i] = renderers[i].material;
        }
        dissolve = 1;
    }

    void Update()
    {
        float ie = intensity;
        if (view)
        {
            if (!view.IsMine)
            {
                ColorUtility.TryParseHtmlString(netColor, out color);
                dissolve = netDissolve;
                colorLerp = .1f;
                dissolveLerp = 4f;
                ie = 1.027f;
            }
            else
            {
                netColor = "#" + ColorUtility.ToHtmlStringRGBA(color);
                netDissolve = dissolve;
            }
        }

        for (int i = 0; i < materials.Length; i++)
        {
            foreach (Vector2 item in intensityEffectors)
            {
                if (item.x == i)
                    ie = item.y;
            }
            materials[i].SetColor("_EmissionColor", Color.Lerp(materials[i].GetColor("_EmissionColor"), color, colorLerp) * ie);
            materials[i].SetFloat("_DissolveAmount", Mathf.Lerp(materials[i].GetFloat("_DissolveAmount"), dissolve * maxDissolve, dissolveLerp * Time.deltaTime));
        }
        foreach (Renderer item in multiRenderers)
        {
            foreach (Material material in item.materials)
            {
                material.SetColor("_EmissionColor", Color.Lerp(material.GetColor("_EmissionColor"), color, colorLerp) * ie);
                material.SetFloat("_DissolveAmount", Mathf.Lerp(material.GetFloat("_DissolveAmount"), dissolve * maxDissolve, dissolveLerp * Time.deltaTime));
            }
        }
    }



    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(view)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(netColor);
                stream.SendNext(netDissolve);
            }
            else
            {
                netColor = (string)stream.ReceiveNext();
                netDissolve = (float)stream.ReceiveNext();
            }
        }
    }
}