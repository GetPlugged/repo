﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

/// <summary>
/// This is used to connect to the photon network
/// </summary>
[AddComponentMenu("Plugged/Network Launcher")]
public class SCR_Network_Launcher : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private byte maxPlayers = 4;

    /// <summary>
    /// Updates with project settings/player/version
    /// </summary>
    [SerializeField]
    private string gameVersion;

    [SerializeField]
    private PunLogLevel logLevel = PunLogLevel.Informational;

    private static bool isConnecting;
    [SerializeField]
    public Object homeScene;

    /// <summary> Start game in online mode or offline mode? </summary>
    public Mode mode;
    public enum Mode
    {
        online,
        offline
    };
    /// <summary> Joint game or host game on start? </summary>
    public Server server;
    public enum Server
    {
        solo,
        multi
    };

    /// <summary>
    /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
    /// </summary>
    private void Awake()
    {
        Screen.fullScreen = false;
        PhotonNetwork.LogLevel = logLevel;
        PhotonNetwork.AutomaticallySyncScene = true;
        gameVersion = Application.version;

    }

    /// <summary>
    /// MonoBehaviour method called on GameObject by Unity during initialization phase.
    /// </summary>
    void Start()
    {
        switch (mode)
        {
            case Mode.online:
                PhotonNetwork.OfflineMode = false;
                break;
            case Mode.offline:
                PhotonNetwork.OfflineMode = true;
                //PhotonNetwork.JoinRandomRoom();
                //PhotonNetwork.CreateRoom("Offline");
                break;
            default:
                break;
        }
        Connect();  //Connects to the photon Server
    }

    /// <summary>
    /// Start the connection process.
    /// - If already connected, we attempt joining a random room
    /// - if not yet connected, Connect this application instance to Photon Cloud Network
    /// </summary>
    public void Connect()
    {
        isConnecting = true;

        // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
        if (PhotonNetwork.IsConnectedAndReady)
        {
            // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnJoinRandomFailed() and we'll create one.
            PhotonNetwork.JoinRandomRoom();
        }
        else
        {
            // #Critical, we must first and foremost connect to Photon Online Server.
            PhotonNetwork.GameVersion = gameVersion;
            PhotonNetwork.ConnectUsingSettings();
            Debug.Log("Launcher: Connect() connecting to server with given version");
        }
    }

    public static void ButtonConnect()
    {
        isConnecting = true;
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnected()
    {
        //base.OnConnected();
        Debug.Log("Launcher: OnConnectedToPhoton() was called by PUN, Connected to Photon");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        //base.OnDisconnected(cause);
        Debug.LogWarning("Launcher: OnDisconnectedFromPhoton() was called by PUN, Disconnected from Photon");
    }

    public override void OnConnectedToMaster()
    {
        if (isConnecting)
        {
            switch (server)
            {
                case Server.solo:
                    PhotonNetwork.CreateRoom(System.DateTime.Now.ToString(), null, null, null); // Create random string for room name
                    break;
                case Server.multi:
                    PhotonNetwork.JoinRandomRoom();
                    break;
                default:
                    break;
            }
        }
        //base.OnConnectedToMaster();
        Debug.Log("Launcher: OnConnectedToMaster() was called by PUN, connected to Master");
        
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        //base.OnJoinRandomFailed(returnCode, message);
        Debug.Log("Launcher:OnPhotonRandomJoinFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom(null, new RoomOptions() {maxPlayers = 4}, null);");
        //PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = maxPlayers }, null);

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = maxPlayers;
        roomOptions.PlayerTtl = 1;
        PhotonNetwork.CreateRoom("Dev", roomOptions, null, null);
        //PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinedRoom()
    {
        //base.OnJoinedRoom();
        Debug.Log("Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.");
        // #Critical: We only load if we are the first player, else we rely on `PhotonNetwork.AutomaticallySyncScene` to sync our instance scene.
        //Debug.Log(homeScene.name);
        PhotonNetwork.LoadLevel("SCN_MODE_Home");
        
    }
}
